package testp

import (
	"dns"
	"log"
	"randomaddr"
	"runtime/debug"
	"servernode"
	"store"
	"strconv"
	"testing"
	"paxosserver"
	"time"
)

var t *testing.T = &testing.T{}

var ExistingAddres = make([]string, 0)

// bootstrap Chubby server and DNS. returns server address and server store address
func ServerDnsBootstrap(ready chan bool) (string, string) {

	var succ bool

	serverAddr := randomaddr.RandomLocalAddr()
	serverStoreAddr := randomaddr.RandomLocalAddr()
	for serverAddr == dns.DNSAddr {
		serverAddr = randomaddr.RandomLocalAddr()
	}

	for serverStoreAddr == serverAddr || serverStoreAddr == dns.DNSAddr {
		serverStoreAddr = randomaddr.RandomLocalAddr()
	}

	// start a DNS server
	go func() {
		if e := dns.ServeDNSRPC(dns.NewDNS(), &ready); e != nil {
			log.Printf("Starting DNS server failed")
			panic(e)
		}
	}()
	<-ready
	log.Printf("DNS is up and running!")

	// start Chubby server (only 1 node)
	st := store.NewStorage()
	go store.ServeStoreRPC(serverStoreAddr, st, ready)
	<-ready
	log.Printf("Server store started at %s", serverStoreAddr)

	server := servernode.NewServerNode(&servernode.ServerNodeConfig{StoreAddr: serverStoreAddr, ServerAddr: serverAddr})
	go func() {
		if e := servernode.ServeServerRPC(serverAddr, server, ready); e != nil {
			log.Printf("Starting Chubby server failed")
			panic(e)
		}
	}()
	<-ready
	log.Printf("Chubby server started at: %s: ", serverAddr)
	log.Printf("Chubby server sotrage started at: %s: ", serverStoreAddr)

	// server registers itself at DNS
	dns := dns.NewDnsRPCclient(dns.DNSAddr)
	dns.Register(store.KV(servernode.Primary, serverAddr), &succ)
	if !succ {
		log.Fatal("Registering Chubby primary failed")
	}

	return serverAddr, serverStoreAddr
}

func PaxosDnsBootstrap() []*paxosserver.PaxosServer {
	ready := make(chan bool)
	// start a DNS server
	go func() {
		if e := dns.ServeDNSRPC(dns.NewDNS(), &ready); e != nil {
			log.Printf("Starting DNS server failed")
			panic(e)
		}
	}()
	<-ready
	log.Printf("DNS is up and running!")



	storeAddrs := GenerateAddrs(ExistingAddres, 5)
	ExistingAddres = append(ExistingAddres, storeAddrs...)

	servAddrs := GenerateAddrs(ExistingAddres, 5)
	ExistingAddres = append(ExistingAddres, servAddrs...)

	paxosAddrs := GenerateAddrs(ExistingAddres, 5)
	ExistingAddres = append(ExistingAddres, paxosAddrs...)

	pxss := make([]*paxosserver.PaxosServer, 0)

	for i := 0; i < 5; i++ {
		st := store.NewStorageId(i)
		ready := make(chan bool)
		go store.ServeStoreRPC(storeAddrs[i], st, ready)
		<-ready
		close(ready)
	}

	for i := 0; i < 5; i++ {
		config := paxosserver.PaxosServerConfig{
			PaxosServerRpcaddr: paxosAddrs[i],
			Peers: paxosAddrs,
			ServerAddr: servAddrs[i],
			ServerID: i,
			StoreAddr: storeAddrs[i],
		}
		pxs := paxosserver.NewPaxosServer(&config)
		pxss = append(pxss, pxs)
	}

	time.Sleep(5 * time.Second)

	return pxss
}

func GenerateClientAddr(existingAddr []string) string {
	clientAddr := randomaddr.RandomLocalAddr()
	for addrExists(clientAddr, existingAddr) {
		clientAddr = randomaddr.RandomLocalAddr()
	}
	return clientAddr
}

func GenerateAddrs(existingAddr []string, num int) []string {
	addrs := make([]string, 0)
	copy := existingAddr[:]
	for num > 0 {
		num--
		addr := GenerateClientAddr(copy)
		addrs = append(addrs, addr)
		copy = append(copy, addr)
	}
	return addrs
}

func addrExists(addr string, existingAddrs []string) bool {
	for _, existingAddr := range existingAddrs {
		if addr == existingAddr || addr == dns.DNSAddr {
			return true
		}
	}
	return false
}

func NewFileName(fileBaseName string, fileNum int) string {
	ret := fileBaseName + strconv.Itoa(fileNum)
	return ret
}

func NotError(e error) {
	if e != nil {
		debug.PrintStack()
		log.Panicf("unexpected error: %s", e.Error())
	}
}

func As(cond bool) {
	if !cond {
		debug.PrintStack()
		log.Panicf("expected true, get false")
	}
}

func IsError(e error) {
	if e == nil {
		debug.PrintStack()
		log.Panicf(e.Error())
	}
}

func SetT(t1 *testing.T) {
	t = t1
}
