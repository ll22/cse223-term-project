package main

import (
	"flag"
	"log"
	"randomaddr"
	"store"
)

func main() {
	help := flag.Bool("help", false, "whether to show help")
	if help != nil && *help {
		log.Println("Usage: cmd-store -id=<string> -addr=<string>")
		return
	}
	id := flag.Int("id", 0, "id of the store")
	addr := flag.String("addr", randomaddr.RandomLocalAddr(), "address of the store")
	st := store.NewStorageId(*id)
	ready := make(chan bool)
	go store.ServeStoreRPC(*addr, st, ready)
	if !<-ready {
		panic("fail to set up a store")
	}
	log.Printf("cmd-store starts serving on %s", *addr)
	select {}
}
