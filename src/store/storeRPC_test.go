package store_test

import (
	"log"
	"randomaddr"
	"store"
	"testing"
)

func TestRPC(t *testing.T) {
	addr := randomaddr.RandomLocalAddr()
	ready := make(chan bool)

	go func() {
		if e := store.ServeStoreRPC(addr, store.NewStorage(), ready); e != nil {
			t.Fatal(e)
		}
	}()

	r := <-ready

	if !r {
		t.Fatal("not ready")
	}

	c := store.NewStoreRPCclient(addr)

	CheckStorage(t, c) // defined in store_test.go
}

func TestMultipleRPCServer(t *testing.T) {
	const multiple = 20
	randAddrs := make(map[string]bool)
	done := make(chan bool)

	for i := 0; i < multiple; i++ {
		addr := randomaddr.RandomLocalAddr()
		_, already := randAddrs[addr]
		for already {
			addr = randomaddr.RandomLocalAddr()
			_, already = randAddrs[addr]
		}
		randAddrs[addr] = true
		ready := make(chan bool)
		log.Printf("Setting server for address: %s", addr)
		go startStorageRPCServer(addr, ready)
		<-ready
	}

	for addr, _ := range randAddrs {
		log.Printf("Setting client for address: %s", addr)
		go func(addr string, done chan bool) {
			c := store.NewStoreRPCclient(addr)
			CheckStorage(t, c)
			done <- true
		}(addr, done)
	}

	for _ = range randAddrs {
		<-done
	}
}

func startStorageRPCServer(addr string, ready chan bool) {

	if e := store.ServeStoreRPC(addr, store.NewStorage(), ready); e != nil {
		panic(e.Error())
	}
}
