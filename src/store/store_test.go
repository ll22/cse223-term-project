package store_test

import (
	"fmt"
	"math/rand"
	"sort"
	"store"
	"testing"
	"testp"
)

func TestStorage(t *testing.T) {
	s := store.NewStorage()

	CheckStorage(t, s)
}

func CheckStorage(t *testing.T, s store.StorageT) {
	var v string
	var b bool
	var l = new(store.List)
	var n int

	kv := func(k, v string) *store.KeyValue {
		return &store.KeyValue{k, v}
	}

	pat := func(pre, suf string) *store.Pattern {
		return &store.Pattern{pre, suf}
	}

	v = "_"
	testp.NotError(s.Get("", &v))
	testp.As(v == "")

	v = "_"
	testp.NotError(s.Get("hello", &v))
	testp.As(v == "")

	testp.NotError(s.Set(kv("h8liu", "run"), &b))
	testp.As(b)
	v = ""
	testp.NotError(s.Get("h8liu", &v))
	testp.As(v == "run")

	testp.NotError(s.Set(kv("h8liu", "Run"), &b))
	testp.As(b)
	v = ""
	testp.NotError(s.Get("h8liu", &v))
	testp.As(v == "Run")

	testp.NotError(s.Set(kv("h8liu", ""), &b))
	testp.As(b)
	v = "_"
	testp.NotError(s.Get("h8liu", &v))
	testp.As(v == "")

	testp.NotError(s.Set(kv("h8liu", "k"), &b))
	testp.As(b)
	v = "_"
	testp.NotError(s.Get("h8liu", &v))
	testp.As(v == "k")

	testp.NotError(s.Set(kv("h8he", "something"), &b))
	testp.As(b)
	v = "_"
	testp.NotError(s.Get("h8he", &v))
	testp.As(v == "something")

	testp.NotError(s.Keys(pat("h8", ""), l))
	sort.Strings(l.L)
	testp.As(len(l.L) == 2)
	testp.As(l.L[0] == "h8he")
	testp.As(l.L[1] == "h8liu")

	testp.NotError(s.ListGet("lst", l))
	testp.As(l.L != nil && len(l.L) == 0)

	testp.NotError(s.ListAppend(kv("lst", "a"), &b))
	testp.As(b)

	testp.NotError(s.ListGet("lst", l))
	testp.As(len(l.L) == 1)
	testp.As(l.L[0] == "a")

	testp.NotError(s.ListAppend(kv("lst", "a"), &b))
	testp.As(b)

	testp.NotError(s.ListGet("lst", l))
	testp.As(len(l.L) == 2)
	testp.As(l.L[0] == "a")
	testp.As(l.L[1] == "a")

	testp.NotError(s.ListRemove(kv("lst", "a"), &n))
	testp.As(n == 2)

	testp.NotError(s.ListGet("lst", l))
	testp.As(l.L != nil && len(l.L) == 0)

	testp.NotError(s.ListAppend(kv("lst", "h8liu"), &b))
	testp.As(b)
	testp.NotError(s.ListAppend(kv("lst", "h7liu"), &b))
	testp.As(b)

	testp.NotError(s.ListGet("lst", l))
	testp.As(len(l.L) == 2)
	testp.As(l.L[0] == "h8liu")
	testp.As(l.L[1] == "h7liu")

	testp.NotError(s.ListKeys(pat("ls", "st"), l))
	testp.As(len(l.L) == 1)
	testp.As(l.L[0] == "lst")

	testp.NotError(s.ListKeys(pat("z", ""), l))
	testp.As(l.L != nil && len(l.L) == 0)

	testp.NotError(s.ListKeys(pat("", ""), l))
	testp.As(len(l.L) == 1)
	testp.As(l.L[0] == "lst")

	for i := 0; i < 10; i++ {
		succ := false
		testp.NotError(s.ListAppend(kv("len", fmt.Sprintf("%d", i)), &succ))
		testp.As(succ == true)
		length := 0
		s.ListLen("len", &length)
		testp.As(length == i+1)
	}

	for i := 0; i < 10; i++ {
		cnt := 0
		testp.NotError(s.ListRemove(kv("len", fmt.Sprintf("%d", i)), &cnt))
		testp.As(cnt == 1)
		length := 0
		s.ListLen("len", &length)
		testp.As(length == 9-i)
	}

	succ := false
	mode := ""
	rf := fmt.Sprintf("%f", rand.Float64())
	testp.NotError(s.WLock(kv("lock"+rf, "a"), &succ))
	testp.As(succ)
	testp.NotError(s.LockMode("lock"+rf, &mode))
	testp.As(mode == "W")
	testp.NotError(s.RLock(kv("lock"+rf, "b"), &succ))
	testp.As(!succ)
	testp.NotError(s.WLock(kv("lock"+rf, "b"), &succ))
	testp.As(!succ)
	testp.NotError(s.WUnlock(kv("lock"+rf, "a"), &succ))
	testp.As(succ)
	testp.NotError(s.RLock(kv("lock"+rf, "b"), &succ))
	testp.As(succ)
	testp.NotError(s.LockMode("lock"+rf, &mode))
	testp.As(mode == "R")
	testp.NotError(s.RUnlock(kv("lock"+rf, "b"), &succ))
	testp.As(succ)

	testp.NotError(s.WLock(kv("lock"+rf, "c"), &succ))
	testp.As(succ)
	succ = false
	testp.IsError(s.WLock(kv("lock"+rf, "c"), &succ))
	testp.As(!succ)
	testp.NotError(s.WUnlock(kv("lock"+rf, "c"), &succ))
	testp.As(succ)
	succ = false
	testp.IsError(s.WUnlock(kv("lock"+rf, "c"), &succ))
	testp.As(!succ)

	su := false
	testp.NotError(s.WLock(kv("lock11"+rf, "d"), &su))
	testp.As(su)
	testp.NotError(s.WLock(kv("lock11"+rf, "g"), &su))
	testp.As(!su)
}
