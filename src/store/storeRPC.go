package store

import (
	"log"
	"net"
	"net/rpc"
	"util"
)

// define the store RPC type
type storeRPCclient struct {
	*util.PersistentRPC
}

func (c *storeRPCclient) Get(key string, value *string) error {
	return c.TryCall("StorageT.Get", key, value)
}

func (c *storeRPCclient) Set(kv *KeyValue, succ *bool) error {
	return c.TryCall("StorageT.Set", kv, succ)
}

func (c *storeRPCclient) Keys(p *Pattern, list *List) error {
	list.L = []string{}
	err := c.TryCall("StorageT.Keys", p, list)
	if list.L == nil {
		list.L = []string{}
	}
	return err
}

func (c *storeRPCclient) Remove(key string, succ *bool) error {
	return c.TryCall("StorageT.Remove", key, succ)
}

func (c *storeRPCclient) ListGet(key string, list *List) error {
	list.L = []string{}
	err := c.TryCall("StorageT.ListGet", key, list)
	if list.L == nil {
		list.L = []string{}
	}
	return err
}

// Append a string to the list. Set succ to true when no error.
func (c *storeRPCclient) ListAppend(kv *KeyValue, succ *bool) error {
	return c.TryCall("StorageT.ListAppend", kv, succ)
}

// Removes all elements that equals to kv.Value in list kv.Key
// n is set to the number of elements removed.
func (c *storeRPCclient) ListRemove(kv *KeyValue, n *int) error {
	return c.TryCall("StorageT.ListRemove", kv, n)
}

// List all the keys of non-empty lists, where the key matches
// the given pattern.
func (c *storeRPCclient) ListKeys(p *Pattern, list *List) error {
	list.L = []string{}
	err := c.TryCall("StorageT.ListKeys", p, list)
	if list.L == nil {
		list.L = []string{}
	}
	return err
}

func (c *storeRPCclient) ListLen(k string, length *int) error {
	return c.TryCall("StorageT.ListLen", k, length)
}

func (c *storeRPCclient) Clock(kc KeyClock, ret *uint64) error {
	return c.TryCall("StorageT.Clock", kc, ret)
}

func (c *storeRPCclient) RLock(kv *KeyValue, succ *bool) error {
	return c.TryCall("StorageT.RLock", kv, succ)
}

func (c *storeRPCclient) WLock(kv *KeyValue, succ *bool) error {
	return c.TryCall("StorageT.WLock", kv, succ)
}

func (c *storeRPCclient) RUnlock(kv *KeyValue, succ *bool) error {
	return c.TryCall("StorageT.RUnlock", kv, succ)
}

func (c *storeRPCclient) WUnlock(kv *KeyValue, succ *bool) error {
	return c.TryCall("StorageT.WUnlock", kv, succ)
}

func (c *storeRPCclient) LockMode(k string, mode *string) error {
	return c.TryCall("StorageT.LockMode", k, mode)
}

//NewStoreRPCclient create a RPC client of remote store
func NewStoreRPCclient(addr string) StorageT {
	c := &storeRPCclient{util.NewPersistentRPC(addr)}
	return c
}

//ServeStoreRPC Serve as a backend based on the given configuration
func ServeStoreRPC(addr string, s StorageT, ready chan bool) error {
	server := rpc.NewServer()
	server.RegisterName("StorageT", s)

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		util.FeedReady(ready, false)
		return err
	}
	defer ln.Close()
	// DefConnMngr.AddListener(b.Addr, ln)
	util.FeedReady(ready, true)

	conns := []net.Conn{}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("The store server %s is dead", addr)
			for _, c := range conns {
				c.Close()
			}
			return err
		}
		conns = append(conns, conn)
		go server.ServeConn(conn)
	}
}
