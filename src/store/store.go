// Package store provides a simple in-memory key value store.
package store

import (
	"container/list"
	"fmt"
	"log"
	"sync"
	//"os"
)

var Logging bool = false

type strList []string

// In-memory storage implementation. All calls always returns nil.
type Storage struct {
	clocks map[string]uint64

	strs   map[string]string
	lists  map[string]*list.List
	rlocks map[string]*list.List
	wlocks map[string]string

	clockLock sync.Mutex
	strLock   sync.Mutex
	listLock  sync.Mutex
	lockLock  sync.Mutex
}

//var _ trib.Storage = new(Storage)

func NewStorageId(id int) StorageT {
	return &Storage{
		strs:   make(map[string]string),
		lists:  make(map[string]*list.List),
		rlocks: make(map[string]*list.List),
		wlocks: make(map[string]string),
	}
}

func NewStorage() StorageT {
	return NewStorageId(0)
}

func (self *Storage) Clock(kc KeyClock, ret *uint64) error {
	self.clockLock.Lock()
	defer self.clockLock.Unlock()

	key := kc.Key
	atLeast := kc.AtLeast

	if clock, exist := self.clocks[key]; !exist || clock < atLeast {
		self.clocks[key] = atLeast
	}
	*ret = self.clocks[key]
	self.clocks[key]++

	if Logging {
		log.Printf("Clock(%s, %d) => %d", key, atLeast, *ret)
	}

	return nil
}

func (self *Storage) Get(key string, value *string) error {
	self.strLock.Lock()
	defer self.strLock.Unlock()

	*value = self.strs[key]

	if Logging {

		//log.Printf("Get(%q) => %q", key, *value)
	}

	return nil
}

func (self *Storage) Set(kv *KeyValue, succ *bool) error {
	self.strLock.Lock()
	defer self.strLock.Unlock()

	// log.Printf("%#v\n\n", kv)

	if kv.Value != "" {
		self.strs[kv.Key] = kv.Value
		*succ = true
	} else {
		delete(self.strs, kv.Key)
		*succ = true
	}

	//if Logging {
	//	log.Printf("Set(%q, %q)", kv.Key, kv.Value)
	//}

	//log.Println(*succ)

	return nil
}

func (self *Storage) Keys(p *Pattern, r *List) error {
	self.strLock.Lock()
	defer self.strLock.Unlock()

	ret := make([]string, 0, len(self.strs))

	for k := range self.strs {
		if p.Match(k) {
			ret = append(ret, k)
		}
	}

	r.L = ret

	if Logging {
		log.Printf("Keys(%q, %q) => %d", p.Prefix, p.Suffix, len(r.L))
		for i, s := range r.L {
			log.Printf("  %d: %q", i, s)
		}
	}

	return nil
}

func (self *Storage) Remove(key string, succ *bool) error {
	self.strLock.Lock()
	defer self.strLock.Unlock()
	_, *succ = self.strs[key]
	delete(self.strs, key)

	return nil
}

func (self *Storage) ListKeys(p *Pattern, r *List) error {
	self.listLock.Lock()
	defer self.listLock.Unlock()

	ret := make([]string, 0, len(self.lists))
	for k := range self.lists {
		if p.Match(k) {
			ret = append(ret, k)
		}
	}

	r.L = ret

	if Logging {
		log.Printf("ListKeys(%q, %q) => %d", p.Prefix, p.Suffix, len(r.L))
		for i, s := range r.L {
			log.Printf("  %d: %q", i, s)
		}
	}

	return nil
}

func (self *Storage) ListGet(key string, ret *List) error {
	self.listLock.Lock()
	defer self.listLock.Unlock()

	if lst, found := self.lists[key]; !found {
		ret.L = []string{}
	} else {
		ret.L = make([]string, 0, lst.Len())
		for i := lst.Front(); i != nil; i = i.Next() {
			ret.L = append(ret.L, i.Value.(string))
		}
	}

	if Logging {
		log.Printf("ListGet(%q) => %d", key, len(ret.L))
		for i, s := range ret.L {
			log.Printf("  %d: %q", i, s)
		}
		log.Printf("logging of ListGet ended")
	}

	return nil
}

func (self *Storage) ListAppend(kv *KeyValue, succ *bool) error {
	self.listLock.Lock()
	defer self.listLock.Unlock()

	lst, found := self.lists[kv.Key]
	if !found {
		lst = list.New()
		self.lists[kv.Key] = lst
	}

	lst.PushBack(kv.Value)

	*succ = true

	if Logging {
		log.Printf("ListAppend(%q, %q)", kv.Key, kv.Value)
	}

	return nil
}

func (self *Storage) ListRemove(kv *KeyValue, n *int) error {
	self.listLock.Lock()
	defer self.listLock.Unlock()

	*n = 0

	lst, found := self.lists[kv.Key]
	if !found {
		return nil
	}

	i := lst.Front()
	for i != nil {
		if i.Value.(string) == kv.Value {
			hold := i
			i = i.Next()
			lst.Remove(hold)
			*n++
			continue
		}

		i = i.Next()
	}

	if lst.Len() == 0 {
		delete(self.lists, kv.Key)
	}

	if Logging {
		log.Printf("ListRemove(%q, %q) => %d", kv.Key, kv.Value, *n)
	}

	return nil
}

func (self *Storage) ListLen(k string, length *int) error {
	list, ok := self.lists[k]
	if !ok {
		*length = 0
	} else {
		*length = list.Len()
	}
	return nil
}

func (self *Storage) RLock(lockby *KeyValue, succ *bool) error {
	self.lockLock.Lock()
	defer self.lockLock.Unlock()
	*succ = false
	_, wok := self.wlocks[lockby.Key]
	if wok {
		return nil
	}
	ls, ok := self.rlocks[lockby.Key]
	if !ok {
		self.rlocks[lockby.Key] = list.New()
	}
	ls = self.rlocks[lockby.Key]

	for itr := ls.Front(); itr != nil; itr = itr.Next() {
		if itr.Value.(string) == lockby.Value {
			*succ = false
			return fmt.Errorf("RLock %s gets locked twice by %s", lockby.Key, lockby.Value)
		}
	}
	ls.PushBack(lockby.Value)
	*succ = true
	if Logging {
		log.Printf("RLock(%q, %q)", lockby.Key, lockby.Value)
	}

	return nil
}

func (self *Storage) WLock(lockby *KeyValue, succ *bool) error {
	self.lockLock.Lock()
	defer self.lockLock.Unlock()
	w, wok := self.wlocks[lockby.Key]
	if Logging {
		log.Printf("lock is holding by %s ? %v", w, wok)
	}

	if !wok {
		r, rok := self.rlocks[lockby.Key]
		if rok && r.Len() > 0 { // someone's holding the read lock
			*succ = false
			return nil
		}
		if Logging {
			log.Println("WLockable")
		}

		self.wlocks[lockby.Key] = lockby.Value

		*succ = true
		if Logging {
			log.Printf("WLock(%q, %q)", lockby.Key, lockby.Value)
		}
		return nil
	}
	*succ = false
	if w == lockby.Value {
		return fmt.Errorf("WLock %s gets locked twice by %s", lockby.Key, lockby.Value)
	}
	return nil
}

func (self *Storage) RUnlock(lockby *KeyValue, succ *bool) error {
	self.lockLock.Lock()
	defer self.lockLock.Unlock()
	*succ = false

	rs, ok := self.rlocks[lockby.Key]
	if !ok {
		return fmt.Errorf("RLock %s is not held by anyone", lockby.Key)
	}
	for itr := rs.Front(); itr != nil; itr = itr.Next() {
		if itr.Value.(string) == lockby.Value {
			*succ = true
			rs.Remove(itr)
			if Logging {
				log.Printf("RUnlock(%q, %q)", lockby.Key, lockby.Value)
			}
			if rs.Len() == 0 {
				delete(self.rlocks, lockby.Key)
			}
			return nil
		}
	}

	return fmt.Errorf("RLock %s is not held by %s", lockby.Key, lockby.Value)
}

func (self *Storage) WUnlock(lockby *KeyValue, succ *bool) error {
	self.lockLock.Lock()
	defer self.lockLock.Unlock()
	w, ok := self.wlocks[lockby.Key]
	*succ = false
	if !ok {
		return fmt.Errorf("WLock %s is not held by anyone", lockby.Key)
	}
	if w != lockby.Value {
		return fmt.Errorf("WLock %s is not held by %s", lockby.Key, lockby.Value)
	}
	*succ = true
	delete(self.wlocks, lockby.Key)
	if Logging {
		log.Printf("WUnlock(%q, %q)", lockby.Key, lockby.Value)
	}
	return nil
}

func (self *Storage) LockMode(lock string, mode *string) error {
	self.lockLock.Lock()
	defer self.lockLock.Unlock()

	_, rok := self.rlocks[lock]
	_, wok := self.wlocks[lock]
	if rok && wok {
		panic("should not happen")
	}
	if rok {
		*mode = "R"
	} else if wok {
		*mode = "W"
	} else {
		panic("should not happen")
	}
	if Logging {
		log.Printf("LockMode => %s", *mode)
	}
	return nil
}
