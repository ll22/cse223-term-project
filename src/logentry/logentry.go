package logentry

import (
	"encoding/binary"
	"errors"
	"io"
	"log"
	"os"
)

const (
	Done    byte = 0xff
	NotDone byte = 0x00
)

//EOF file end correctly
var EOF = errors.New("File End")

// WriteNextLogEntry write the next entry to the file
func WriteNextLogEntry(file *os.File, entry LogEntry) error {
	var ioe error
	bs := make([]byte, 4)

	fl := uint32(len(entry.FileName))
	binary.BigEndian.PutUint32(bs, fl)
	if _, ioe = file.Write(bs); ioe != nil {
		return ioe
	}
	if _, ioe = file.Write([]byte(entry.FileName)); ioe != nil {
		return ioe
	}

	cl := uint32(len(entry.WriteContent))
	binary.BigEndian.PutUint32(bs, cl)
	if _, ioe = file.Write(bs); ioe != nil {
		return ioe
	}
	if _, ioe = file.Write([]byte(entry.WriteContent)); ioe != nil {
		return ioe
	}

	var donebyte byte

	if entry.IsDone {
		donebyte = Done
	} else {
		donebyte = NotDone
	}

	if _, ioe = file.Write([]byte{donebyte}); ioe != nil {
		return ioe
	}

	return nil
}

// ReadNextLogEntry read the next entry from the file
func ReadNextLogEntry(file *os.File) (*LogEntry, error) {
	bs := make([]byte, 4)

	if _, e := file.Read(bs); e != nil {
		if e != nil {
			log.Println(e.Error())
		}
		if e == io.EOF {
			e = EOF
		}
		return nil, e
	}
	flen := binary.BigEndian.Uint32(bs)
	fbytes := make([]byte, flen)
	if _, e := file.Read(fbytes); e != nil {
		return nil, e
	}
	fname := string(fbytes)

	if _, e := file.Read(bs); e != nil {
		return nil, e
	}
	clen := binary.BigEndian.Uint32(bs)
	cbytes := make([]byte, clen)
	if _, e := file.Read(cbytes); e != nil {
		return nil, e
	}
	content := string(cbytes)

	ds := make([]byte, 1)
	if _, e := file.Read(ds); e != nil {
		return nil, e
	}
	isDone := false
	if ds[0] == Done {
		isDone = true
	} else if ds[0] == NotDone {
		isDone = false
	} else {
		return nil, errors.New("misformated file")
	}

	return &LogEntry{
		FileName:     fname,
		WriteContent: content,
		IsDone:       isDone,
	}, nil
}

func SetLastDone(file *os.File) error {
	return SetLastFlag(file, true)
}

func SetLastNotDoe(file *os.File) error {
	return SetLastFlag(file, false)
}

func SetLastFlag(file *os.File, done bool) error {
	offset, e := file.Seek(-1, 0)
	if e != nil {
		return e
	}
	d := NotDone
	if done {
		d = Done
	}
	if _, e = file.WriteAt([]byte{d}, offset); e != nil {
		return e
	}
	return nil
}

// SeekToNextFlag go to position of the next flag
func SeekToNextFlag(file *os.File) error {
	bs := make([]byte, 4)

	if _, e := file.Read(bs); e != nil {
		return e
	}
	flen := binary.BigEndian.Uint32(bs)
	if _, e := file.Seek(int64(flen), 1); e != nil {
		return e
	}

	if _, e := file.Read(bs); e != nil {
		return e
	}
	clen := binary.BigEndian.Uint32(bs)
	if _, e := file.Seek(int64(clen), 1); e != nil {
		return e
	}
	return nil
}

// SetNextFlag set next flag to be Done or NoDone
func SetNextFlag(file *os.File, done bool) error {
	if e := SeekToNextFlag(file); e != nil {
		return e
	}
	var donebyte byte
	if done {
		donebyte = Done
	} else {
		donebyte = NotDone
	}
	_, e := file.Write([]byte{donebyte})
	return e
}

// LogEntry log entry for the contract operations.
// For now we only support writes to the contents.
// The format of the log entry (when written on file) will be:
// n = Length of Filename  | Filename  | m = Length of Content | Content | Flag
//     4 bytes             | n bytes   |       4 bytes         | m bytes | 1 byte
type LogEntry struct {
	FileName     string
	WriteContent string
	IsDone       bool
}

func CheckFileLegal(path string) error {
	file, e := os.Open(path)
	if e != nil {
		return e
	}
	defer file.Close()
	for {
		le, e := ReadNextLogEntry(file)
		log.Println(le)
		if e == EOF {
			break
		}
		if e != nil {
			log.Println(e == EOF)
			log.Println(e.Error())
			return e
		}
	}
	return nil
}
