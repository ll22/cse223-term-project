package events

// EventType Integer indicating the type of events
type EventType int

// Chubby events
const (
	EventNil EventType = iota
	EventFileContentModified
	EventChildNodeChanged
	EventChubbyMasterFailedOver
	EventHandlerInvalid
	EventLockAcquired
	EventLockConflict
	EventSessionDeprecated
)

// Event - Chubby events
// for EventHandlerInvalid, Msg is a regex to match to filename to invalid
type Event struct {
	Type EventType
	Msg  string
}
