package experiment

import (
	"log"
	"net"
	"net/rpc"
	"sync"
	"time"
)

var ServerAddr string = "localhost:6666"

// server object
type TestObjT interface {
	Boo(key string, val *string) error
}

type TestObj struct {
	addr string
}

func NewTestObj() *TestObj {
	return &TestObj{
		addr: ServerAddr,
	}
}

func (self *TestObj) Boo(key string, val *string) error {

	log.Printf(key)
	time.Sleep(time.Second * 5)
	return nil
}

type ServerRPC struct {
	//server *rpc.Server
	Addr  string
	Conns []net.Conn
}

func NewRPCServer(addr string) *ServerRPC {
	return &ServerRPC{
		addr,
		make([]net.Conn, 0),
	}
}

//ServeBack Serve as a backend based on the given configuration
func (self *ServerRPC) ServeTestServerRPC(s *TestObj, ready *chan bool) error {
	server := rpc.NewServer()
	server.RegisterName("TestObj", s)

	ln, err := net.Listen("tcp", self.Addr)
	if err != nil {
		feedReady(ready, false)
		return err
	}
	defer ln.Close()
	// DefConnMngr.AddListener(b.Addr, ln)
	feedReady(ready, true)

	self.Conns = []net.Conn{}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("The TestObj %s is dead", ServerAddr)
			for _, c := range self.Conns {
				c.Close()
			}
			return err
		}
		self.Conns = append(self.Conns, conn)
		go server.ServeConn(conn)
	}
}

func (self *ServerRPC) Kill() error {
	log.Printf("Trying to close all connections")
	var err error
	for _, c := range self.Conns {
		err = c.Close()
		if err != nil {
			return err
		}
		log.Printf("Closed one connection!")
	}
	return nil
}

func feedReady(r *chan bool, v bool) {
	if r != nil && *r != nil {
		*r <- v
		if v {
			log.Println("TestObj set true to ready")
		}
	} else {
		log.Println("TestObj ready channel is nil.")
	}
}

// define the store RPC type
type TestObjRPCclient struct {
	addr string
	m    *sync.Mutex
	conn *rpc.Client
}

func NewRPCclient(addr string) *TestObjRPCclient {
	return &TestObjRPCclient{
		addr: addr,
		m:    &sync.Mutex{},
		conn: nil,
	}
}

// functions of type DnsRPCclient
func (c *TestObjRPCclient) requireConn() error {
	c.m.Lock()
	defer c.m.Unlock()
	if c.conn == nil {
		conn, err := rpc.Dial("tcp", c.addr)
		c.conn = conn
		return err
	}
	return nil
}

func (c *TestObjRPCclient) call(method string, args interface{}, reply interface{}) error {
	if err := c.requireConn(); err != nil {
		return err
	}

	if err := c.conn.Call(method, args, reply); err != nil {
		c.conn = nil
		return err
	}

	return nil
}

func (c *TestObjRPCclient) Boo(key string, val *string) error {
	return c.call("TestObj.Boo", key, val)
}
