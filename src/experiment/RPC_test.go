package experiment_test

import (
	"testing"

	"experiment"
	"log"
	"time"
)

func TestDNS(t *testing.T) {
	ready := make(chan bool)

	RPCserver := experiment.NewRPCServer(experiment.ServerAddr)

	go func() {
		if e := RPCserver.ServeTestServerRPC(experiment.NewTestObj(), &ready); e != nil {
			t.Fatal(e)
		}
	}()

	r := <-ready

	if !r {
		t.Fatal("not ready")
	}
	c := experiment.NewRPCclient(experiment.ServerAddr)
	var req string
	var reply *string
	req = "Lincong"

	clientRoutine := func(sequenceNum int) {
		if err := c.Boo(req, reply); err != nil {
			log.Printf("RPC Boo() returned with error!")
			//log.Printf(err.Error() == "unexpected EOF")
			if err.Error() == "unexpected EOF" {
				log.Printf("Connection failed")
			} else {
				log.Printf("Other failures")
			}
		}
		log.Printf(" done ", sequenceNum)
		ready <- true
	}

	go clientRoutitest_primitives.NotError(1)
	go clientRoutitest_primitives.NotError(2)
	go clientRoutitest_primitives.NotError(3)

	time.Sleep(time.Second * 1)
	//RPCserver.Kill()

	clientNum := 3
	for clientNum > 0 {
		<-ready
		clientNum--
	}

}
