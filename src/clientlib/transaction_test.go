package clientlib_test

import (
	"clientlib"
	"events"
	"log"
	"servernode"
	"testing"
	"testp"
	"time"
	//"os"
)

func TestTransaction(t *testing.T) {
	log.SetFlags(log.Lshortfile | log.Ldate)

	// ******************************************************
	// System bootstrap
	// ******************************************************
	var ready chan bool
	var serverAddr string
	var serverStoreAddr string
	var rwFiles string = "/ls/foo/wombat/pouch"
	//var fh clientlib.FileHandleT
	//var err error

	existingAddresses := make([]string, 0)

	ready = make(chan bool)
	// start server, server store and DNS server
	serverAddr, serverStoreAddr = testp.ServerDnsBootstrap(ready)
	existingAddresses = append(existingAddresses, serverAddr)
	existingAddresses = append(existingAddresses, serverStoreAddr)

	// ******************************************************
	// Tests start
	// ******************************************************

	//oneClientContractsOneFile(existingAddresses)
	//multipleClientMultipleContractsOneFile(existingAddresses)
	//oneClientContractsOneFile(existingAddresses)

	oneClientCrash(existingAddresses, rwFiles)
	cacheTest(existingAddresses, rwFiles)
}

func cacheTest(existingAddresses []string, rwFiles string) {
	var err error
	var fhOne clientlib.FileHandleT
	var fhTwo clientlib.FileHandleT
	initialContent := "blah blah blah "

	clientAddrOne := testp.GenerateClientAddr(existingAddresses)
	existingAddresses = append(existingAddresses, clientAddrOne)

	clientAddrTwo := testp.GenerateClientAddr(existingAddresses)

	e, clientOne := clientlib.NewChubbyClient(clientAddrOne)
	testp.NotError(e)
	// create and open a file
	err, fhOne = clientOne.Open(
		rwFiles,
		servernode.WriteHandle,
		events.EventFileContentModified,
		true,
		initialContent,
		servernode.FileACLInfo{
			Writer:     []string{clientAddrOne, clientAddrTwo},
			Reader:     []string{clientAddrOne, clientAddrTwo},
			ACLChanger: []string{clientAddrOne},
		},
		false,
	)
	testp.NotError(err)

	rCount := 0
	for rCount < 10 {
		rCount++
		e, contentInfo, _ := fhOne.GetContentsAndStat()
		testp.NotError(e)
		testp.As(contentInfo.Content == initialContent)
	}

	// start another client to open and write to the same file

	e, clientTwo := clientlib.NewChubbyClient(clientAddrTwo)

	err, fhTwo = clientTwo.Open(
		rwFiles,
		servernode.WriteHandle,
		events.EventFileContentModified,
		false,
		initialContent,
		servernode.FileACLInfo{
			Writer:     []string{clientAddrTwo},
			Reader:     []string{clientAddrOne, clientAddrTwo},
			ACLChanger: []string{clientAddrTwo},
		},
		false,
	)
	testp.NotError(err)

	// get lock
	// err = fhTwo.Acquire("W", 100)
	// testp.NotError(err)

	setContents := "dummy_write "

	// reset the initial contents of the file
	err = fhTwo.SetContents(setContents)
	testp.NotError(err)

	for rCount > 0 {
		rCount--
		e, contentInfo, _ := fhTwo.GetContentsAndStat()
		testp.NotError(e)
		testp.As(contentInfo.Content == setContents)
	}

	setContents = "dummy_write2 "

	// reset the initial contents of the file
	err = fhTwo.SetContents(setContents)
	testp.NotError(err)

	for rCount < 3 {
		rCount++
		e, contentInfo, _ := fhTwo.GetContentsAndStat()
		testp.NotError(e)
		testp.As(contentInfo.Content == setContents)
	}

	e, contentInfo, _ := fhOne.GetContentsAndStat()
	testp.NotError(e)
	testp.As(contentInfo.Content == setContents)

	setContents = "dummy_write3 "

	// reset the initial contents of the file
	err = fhTwo.SetContents(setContents)
	testp.NotError(err)

}

func oneClientCrash(existingAddresses []string, rwFiles string) {
	var err error
	var fh clientlib.FileHandleT
	clientAddr := testp.GenerateClientAddr(existingAddresses)

	// start a client which starts contract and put log in GFS and quit
	rwCount := 10
	go startOneCrashClient(clientAddr, rwFiles, rwCount)

	log.Print("Waiting for a few seconds.\n.\n.\n.\n")
	// 10 seconds later
	time.Sleep(10000 * time.Millisecond)

	// start another client with the same address to read the file and verify if the
	// content of the file has been changed by the server
	log.Println("Start a new client to verify file content")
	e, client := clientlib.NewChubbyClient(clientAddr)
	testp.NotError(e)

	rwFiles += clientAddr
	// create and open a file
	err, fh = client.Open(
		rwFiles,
		servernode.ReadHandle,
		events.EventFileContentModified,
		false,
		"",
		servernode.FileACLInfo{
			Writer:     []string{clientAddr},
			Reader:     []string{clientAddr},
			ACLChanger: []string{clientAddr},
		},
		false,
	)
	testp.NotError(err)

	log.Printf("Second client tries to get Read lock for file %s", rwFiles)
	err = fh.Acquire("R", 1)
	testp.NotError(err)
	log.Println("Second client tries to get Read lock succeed!")

	log.Printf("Second client tries to read file %s", rwFiles)
	e3, content, _ := fh.GetContentsAndStat()
	testp.NotError(e3)

	refContent := "dummy_write "
	for rwCount > 0 {
		refContent += " dummy_write"
		rwCount--
	}

	log.Printf("File content read by the second client is: %s", content.Content)
	testp.As(content.Content == refContent)

	fh.Release()
	client.CloseWith(fh)
	log.Printf("Second client is trying to shut itself down!")
	client.ShutDown()
}

func startOneCrashClient(clientAddr string, rwFiles string, rwCounter int) {
	var err error
	var fh clientlib.FileHandleT

	var defaultLogFilePath string = "./gfs/test_log"
	initialContent := "blah blah blah "

	rwFiles += clientAddr
	e, client := clientlib.NewChubbyClient(clientAddr)
	testp.NotError(e)
	// create and open a file
	err, fh = client.Open(
		rwFiles,
		servernode.WriteHandle,
		events.EventFileContentModified,
		true,
		initialContent,
		servernode.FileACLInfo{
			Writer:     []string{clientAddr},
			Reader:     []string{clientAddr},
			ACLChanger: []string{clientAddr},
		},
		false,
	)
	testp.NotError(err)

	err = fh.Acquire("W", 100)
	testp.NotError(err)

	// apply a contract on that file
	ct := client.NewContract(defaultLogFilePath, "1")

	setContents := "dummy_write "

	// reset the initial contents of the file
	err = fh.SetContents(setContents)
	testp.NotError(err)
	// contract looks like this
	contractOps := func(ct clientlib.ContractT) error {
		i := 0
		for i < rwCounter {
			i++
			setContents, err = ct.WillRead(fh)
			setContents += " dummy_write"

			err = ct.WillWrite(fh, setContents)
			if err != nil {
				return err
			}
		}
		return nil
	}

	// register contract
	if err = ct.Register(contractOps); err != nil {
		testp.NotError(err)
	}

	log.Printf("First client is trying to shut itself down!")
	client.ShutDown()
}

func oneClientContractsOneFile(existingAddresses []string) {
	var clientAddr string
	var defaultTestFilePath string
	var testFilePath string = "/ls/foo/wombat/pouch"
	testFilePath = defaultTestFilePath + clientAddr

	clientAddr = testp.GenerateClientAddr(existingAddresses)
	startOneClient(clientAddr, testFilePath)
}

func startOneClient(clientAddr string, rwFiles string) {

	var defaultLogFilePath string
	var initialContent string
	var fh clientlib.FileHandleT
	var err error

	initialContent = "blah blah blah "
	defaultLogFilePath = "./gfs/test_log"

	e, client := clientlib.NewChubbyClient(clientAddr)
	testp.NotError(e)
	log.Printf("Chubby client started at: %s: ", clientAddr)
	// create and open a file
	err, fh = client.Open(
		rwFiles,
		servernode.WriteHandle,
		events.EventFileContentModified,
		true,
		initialContent,
		servernode.FileACLInfo{
			Writer:     []string{clientAddr},
			Reader:     []string{clientAddr},
			ACLChanger: []string{clientAddr},
		},
		false,
	)
	testp.NotError(err)

	err = fh.Acquire("W", 100)
	testp.NotError(err)

	setContents := "write "
	err = fh.SetContents(setContents)
	testp.NotError(err)
	rwCounter := 2000

	// apply a contract on that file
	ct := client.NewContract(defaultLogFilePath, "1")

	// contract
	contractOps := func(ct clientlib.ContractT) error {
		i := 0
		for i < rwCounter {
			i++
			setContents, err = ct.WillRead(fh)
			setContents += " write"

			err = ct.WillWrite(fh, setContents)
			if err != nil {
				return err
			}
		}
		return nil
	}

	if err = ct.Register(contractOps); err != nil {
		testp.NotError(err)
	}

	if err = ct.Commit(); err != nil {
		testp.NotError(err)
	}
	log.Printf("Contract finished!")

	// read file contents and compare
	e3, content, _ := fh.GetContentsAndStat()
	testp.NotError(e3)
	//log.Println(content.Content)

	fh.Release()
	client.CloseWith(fh)

	referenceContent := "write "
	i := 0
	for i < rwCounter {
		i++
		referenceContent += " write"
	}

	testp.As(content.Content == referenceContent)
	log.Printf("startOneClient() finished!")
}

func multipleClientMultipleContractsOneFile(existingAddresses []string) {
	var clientAddr string
	var defaultTestFilePath string = "/ls/foo/wombat/pouch"
	done := make(chan bool)

	clientNum := 20
	startClientCount := 0
	for startClientCount < clientNum {
		startClientCount++
		clientAddr = testp.GenerateClientAddr(existingAddresses)
		existingAddresses = append(existingAddresses, clientAddr)

		go func(addr string, finished chan bool) {
			startOneClient(addr, defaultTestFilePath+addr)
			finished <- true

		}(clientAddr, done)
	}

	for startClientCount > 0 {
		startClientCount--
		<-done
	}
	//log.Printf("%i clients and each does a contract test finished!\n", clientNum)
}
