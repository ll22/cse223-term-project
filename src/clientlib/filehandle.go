package clientlib

import (
	"errors"
	"events"
	"fmt"
	"servernode"
	"sync"
	//"log"
	"log"
)

const (
	holdWriteLock string = "W"
	holdReadLock  string = "R"
	holdNoLock    string = "N"
)

// FileHandleT is a alias of io.ReadWriteCloser
type FileHandleT interface {
	GetFileName() string
	SetContents(content string) error
	SetACL(reader []string, writer []string, aclChanger []string) error
	GetContentsAndStat() (error, *servernode.FileContentInfo, *servernode.FileMetaInfo)
	GetStat() (error, *servernode.FileMetaInfo)
	ReadDir() (error, []string)
	Acquire(mode string, retries uint) error
	TryAcquire(mode string) (error, bool)
	Release() error
	GetSequencer() (error, *servernode.Sequencer)
	SetSequencer(seq *servernode.Sequencer)
	CheckSequencer(seq *servernode.Sequencer) (error, bool)
	close()
	Invalidate()

	GetLockStatus() string
	SetLockStatus(string) error
}

// fileHandle implementation of FileHandleT, according to the chubby paper
type fileHandle struct {
	filename            string
	mode                string
	lockStatus          string
	subscribedEventType events.EventType
	isClosed            bool
	isDir               bool
	cache               filecache
	serverRPCclient     servernode.ServerNodeT
	addr                string
	m                   sync.Mutex
	cacheEnabled        bool
}

type filecache struct {
	content servernode.FileContentInfo
	stat    servernode.FileMetaInfo
	isEmpty bool
}

// newFileHandle new a file handle
func newFileHandle(
	filename string,
	mode string,
	addr string,
	isDir bool,
	subscribedEventType events.EventType,
	serverRPCclient servernode.ServerNodeT,
) FileHandleT {

	cache := filecache{}
	cache.isEmpty = true // initially cache is empty

	return &fileHandle{
		filename:            filename,
		mode:                mode,
		lockStatus:          holdNoLock,
		subscribedEventType: subscribedEventType,
		isClosed:            false,
		isDir:               isDir,
		cache:               cache,
		serverRPCclient:     serverRPCclient,
		addr:                addr,
		m:                   sync.Mutex{},
		cacheEnabled:        true, // cache is enabled by default
	}
}

// GetFilename get the file name associated with the file handle
func (fd *fileHandle) GetFileName() string {
	return fd.filename
}

func (fd *fileHandle) GetLockStatus() string {
	return fd.lockStatus
}

func (fd *fileHandle) SetLockStatus(newLockStatus string) error {
	if newLockStatus != holdNoLock && newLockStatus != holdWriteLock && newLockStatus != holdReadLock {
		return errors.New("Trying to set invalid lock status to file handle")
	}
	fd.lockStatus = newLockStatus
	return nil
}

// SetContents set content of the remote file if generation is the most up-to-date
func (fd *fileHandle) SetContents(content string) error {
	if fd.isClosed {
		return errors.New("The file handle is already closed")
	}

	if fd.mode == "R" {
		return errors.New("The file handle is not opened in W mode")
	}

	request := servernode.SetContentsRequestInfo{
		Requestor: fd.addr,
		Content:   content,
		FilePath:  fd.filename,
		// TODO: implement generation number Generation: generation,
	}

	reply := servernode.SetContentsReplyInfo{
		Succ: false,
	}

	if err := fd.serverRPCclient.SetContents(request, &reply); err != nil {
		return err
	}

	fd.m.Lock()

	if fd.cacheEnabled {
		fd.cache.content = servernode.FileContentInfo{Content: content}
		fd.cache.isEmpty = false
	}
	fd.m.Unlock()

	return nil
}

func (fd *fileHandle) SetACL(reader []string, writer []string, aclChanger []string) error {
	if fd.isClosed {
		return errors.New("The file handle is already closed")
	}
	if fd.mode != "ACL" {
		return errors.New("The file handle is not opened in ACL mode")
	}
	request := servernode.SetACLRequestInfo{
		FilePath: fd.filename,
		ACL: servernode.FileACLInfo{
			ACLChanger: aclChanger,
			Reader:     reader,
			Writer:     writer,
		},
	}
	reply := servernode.SetACLReplyInfo{
		Succ: false,
	}

	return fd.serverRPCclient.SetACL(request, &reply)
}

// IsClosed check if the file handle is closed
func (fd *fileHandle) IsClosed() bool {
	return fd.isClosed
}

func (fd *fileHandle) close() {
	fd.m.Lock()
	defer fd.m.Unlock()
	fd.isClosed = true
}

func (fd *fileHandle) GetContentsAndStat() (
	error,
	*servernode.FileContentInfo,
	*servernode.FileMetaInfo,
) {

	if fd.isClosed {
		return errors.New("The file handle is closed"), nil, nil
	}

	if !fd.cache.isEmpty {
		log.Printf("Client %s cache hit", fd.addr)
		//log.Printf("Cached content is: %s", fd.cache.content.Content)
		return nil, &fd.cache.content, &fd.cache.stat
	}
	log.Printf("Client %s cache missed", fd.addr)

	request := servernode.GetContentsAndStatRequestInfo{
		FilePath:  fd.filename,
		Requestor: fd.addr,
	}

	reply := servernode.GetContentsAndStatReplyInfo{
		Content: servernode.FileContentInfo{},
		Stat:    servernode.FileMetaInfo{},
	}

	if err := fd.serverRPCclient.GetContentsAndStat(request, &reply); err != nil {
		return err, nil, nil
	}

	fd.m.Lock()
	if fd.cacheEnabled {
		fd.cache.content = reply.Content
		fd.cache.stat = reply.Stat
		fd.cache.isEmpty = false
	}
	fd.m.Unlock()

	//log.Printf("In GetContentsAndStat() ####, content read is %s", reply.Content.Content)

	return nil, &reply.Content, &reply.Stat
}

func (fd *fileHandle) GetStat() (error, *servernode.FileMetaInfo) {
	if fd.isClosed {
		return errors.New("The file handle is closed"), nil
	}

	// if cache is not enabled, cache will always be empty
	if !fd.cache.isEmpty {
		return nil, &fd.cache.stat
	}

	request := servernode.GetStatRequestInfo{
		FilePath: fd.filename,
	}

	reply := servernode.GetStatReplyInfo{
		Stat: servernode.FileMetaInfo{},
	}

	if err := fd.serverRPCclient.GetStat(request, &reply); err != nil {
		return err, nil
	}

	// only cache data when cache is enabled
	if fd.cacheEnabled {
		fd.m.Lock()
		fd.cache.stat = reply.Stat
		fd.cache.isEmpty = false
		fd.m.Unlock()
	}

	return nil, &reply.Stat
}

func (fd *fileHandle) EnableCache() {
	fd.m.Lock()
	defer fd.m.Unlock()
	fd.cacheEnabled = true
}

func (fd *fileHandle) DisableCache() {
	fd.m.Lock()
	defer fd.m.Unlock()
	fd.cacheEnabled = false
}

func (fd *fileHandle) ReadDir() (error, []string) {
	if !fd.isDir {
		return fmt.Errorf("%s is not a directory", fd.filename), nil
	}
	listReply := servernode.ListReply{L: []string{}}
	err := fd.serverRPCclient.ListAllFiles(fd.filename, &listReply)
	return err, listReply.L
}

func (fd *fileHandle) isLockModeValid(lockMode string) bool {
	openMode := fd.mode
	if openMode == "ACL" || openMode == holdWriteLock {
		return lockMode == holdWriteLock
	}
	return lockMode == holdReadLock || lockMode == holdWriteLock

}

func (fd *fileHandle) Acquire(mode string, retries uint) error {
	if fd.isClosed {
		return errors.New("Could not acquire a lock on a closed file")
	}
	if !fd.isLockModeValid(mode) {
		return errors.New("Lock mode mismatches with open mode")
	}
	request := servernode.AcquireRequestInfo{
		Acquirer: fd.addr,
		FilePath: fd.filename,
		Mode:     mode,
		Retries:  retries,
	}
	reply := servernode.AcquireReplyInfo{
		Succ: false,
	}

	if err := fd.serverRPCclient.Acquire(request, &reply); err != nil {
		return err
	}
	fd.SetLockStatus(mode)
	return nil
}

func (fd *fileHandle) TryAcquire(mode string) (error, bool) {
	if fd.isClosed {
		return errors.New("Could not acquire a lock on a closed file"), false
	}
	if !fd.isLockModeValid(mode) {
		return errors.New("Lock mode mismatches with open mode"), false
	}
	request := servernode.TryAcquireRequestInfo{
		Acquirer: fd.addr,
		FilePath: fd.filename,
		Mode:     mode,
	}

	reply := servernode.TryAcquireReplyInfo{
		Succ: false,
	}
	err := fd.serverRPCclient.TryAcquire(request, &reply)
	if err == nil && reply.Succ {
		fd.SetLockStatus(mode)
	}

	return err, reply.Succ
}

func (fd *fileHandle) Release() error {
	lockStatus := fd.GetLockStatus()
	if lockStatus == holdNoLock {
		return errors.New("no lock is held by the current handle")
	}

	request := servernode.ReleaseRequestInfo{
		FilePath: fd.filename,
		Holder:   fd.addr,
		Mode:     fd.mode,
	}
	reply := servernode.ReleaseReplyInfo{
		Succ: false,
	}

	if err := fd.serverRPCclient.Release(request, &reply); err != nil {
		return err
	}
	if !reply.Succ {
		return errors.New("Release lock failed")
	}
	// successfully released lock
	fd.SetLockStatus(holdNoLock)
	return nil
}

func (fd *fileHandle) GetSequencer() (error, *servernode.Sequencer) {
	return nil, nil
}

func (fd *fileHandle) SetSequencer(seq *servernode.Sequencer) {

}

func (fd *fileHandle) CheckSequencer(seq *servernode.Sequencer) (error, bool) {
	return nil, false
}

func (fd *fileHandle) Invalidate() {
	fd.m.Lock()
	defer fd.m.Unlock()
	log.Printf("Client %s invalidate cache for file %s", fd.addr, fd.filename)
	fd.cache.isEmpty = true
	//fd.cache.hasCache = false
}
