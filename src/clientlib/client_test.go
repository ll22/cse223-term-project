package clientlib_test

import (
	"clientlib"
	"events"
	"fmt"
	"log"
	"servernode"
	"testing"
	"testp"
)

func TestChubbyClient_Create(t *testing.T) {
	log.SetFlags(log.Lshortfile | log.Ldate)
	ready := make(chan bool)
	testp.ServerDnsBootstrap(ready)
	//<-ready
	addrs := make([]string, 0)
	for i := 0; i < 3; i++ {
		clientAddr1 := testp.GenerateClientAddr(addrs)
		addrs = append(addrs, clientAddr1)
	}
	e, chubby := clientlib.NewChubbyClient(addrs[0])
	testp.NotError(e)
	e, filehandle := chubby.Create(
		"file1",
		servernode.WriteHandle,
		events.EventNil,
		"content1",
		servernode.FileACLInfo{
			ACLChanger: addrs,
			Reader:     addrs,
			Writer:     addrs,
		},
	)
	testp.NotError(e)
	e, content, _ := filehandle.GetContentsAndStat()
	testp.NotError(e)
	testp.As(content.Content == "content1")

	e, _ = chubby.Create(
		"file1",
		servernode.WriteHandle,
		events.EventNil,
		"content1",
		servernode.FileACLInfo{
			ACLChanger: addrs,
			Reader:     addrs,
			Writer:     addrs,
		},
	)
	testp.IsError(e)
}

func TestChubbyClient_OpenExisting(t *testing.T) {
	log.SetFlags(log.Lshortfile | log.Ldate)
	ready := make(chan bool)
	testp.ServerDnsBootstrap(ready)
	//<-ready
	addrs := make([]string, 0)
	for i := 0; i < 3; i++ {
		clientAddr1 := testp.GenerateClientAddr(addrs)
		addrs = append(addrs, clientAddr1)
	}
	e, chubby1 := clientlib.NewChubbyClient(addrs[0])
	e, chubby2 := clientlib.NewChubbyClient(addrs[1])
	testp.NotError(e)
	e, filehandle := chubby1.Create(
		"file1",
		servernode.WriteHandle,
		events.EventNil,
		"content1",
		servernode.FileACLInfo{
			ACLChanger: addrs,
			Reader:     addrs,
			Writer:     addrs,
		},
	)
	testp.NotError(e)
	e, content, _ := filehandle.GetContentsAndStat()
	testp.NotError(e)
	testp.As(content.Content == "content1")
	chubby1.CloseWith(filehandle)

	e, filehandle1 := chubby1.OpenExisting("file1", servernode.ReadHandle)
	testp.NotError(e)
	e, content1, _ := filehandle1.GetContentsAndStat()
	testp.NotError(e)
	testp.As(content1.Content == "content1")

	e, filehandle2 := chubby2.OpenExisting("file1", servernode.WriteHandle)
	testp.NotError(e)
	testp.NotError(filehandle2.SetContents("content2"))
	chubby2.CloseWith(filehandle2)

	e, content1, _ = filehandle1.GetContentsAndStat()
	testp.NotError(e)
	testp.As(content1.Content == "content2")
	chubby1.CloseWith(filehandle1)
}

func TestFileHandle_AcquireRelease(t *testing.T) {
	log.SetFlags(log.Lshortfile | log.Ldate)
	ready := make(chan bool)
	testp.ServerDnsBootstrap(ready)
	//<-ready
	addrs := make([]string, 0)
	for i := 0; i < 3; i++ {
		clientAddr1 := testp.GenerateClientAddr(addrs)
		addrs = append(addrs, clientAddr1)
	}
	e, chubby1 := clientlib.NewChubbyClient(addrs[0])
	testp.NotError(e)
	e, chubby2 := clientlib.NewChubbyClient(addrs[1])
	testp.NotError(e)
	e, fh1 := chubby1.Create(
		"file1",
		servernode.WriteHandle,
		events.EventNil,
		"content1",
		servernode.FileACLInfo{
			ACLChanger: addrs,
			Reader:     addrs,
			Writer:     addrs,
		},
	)
	e, fh2 := chubby2.OpenExisting(
		"file1", servernode.WriteHandle,
	)

	done := make(chan bool)

	acquireAction := func(fh clientlib.FileHandleT, action func()) {
		fh.Acquire("W", 100)
		action()
		fh.Release()
	}

	value := 10

	go func() {
		acquireAction(fh1, func() {
			i := 5
			for i > 0 {
				value += 10
				i--
			}
		})
		done <- true
	}()

	go func() {
		acquireAction(fh2, func() {
			i := 5
			for i > 0 {
				value *= 10
				i--
			}
		})
		done <- true
	}()

	<-done
	<-done
	log.Println(value)
	testp.As(value == 6000000 || value == 1000050)

}

func TestContract_TryMultiAcquire(t *testing.T) {
	log.SetFlags(log.Lshortfile | log.Ldate)
	ready := make(chan bool)
	testp.ServerDnsBootstrap(ready)
	//<-ready
	addrs := make([]string, 0)
	clientAddr := testp.GenerateClientAddr(addrs)
	addrs = append(addrs, clientAddr)
	e, chubby := clientlib.NewChubbyClient(addrs[0])
	testp.NotError(e)
	ct := chubby.NewContract("gfs", "contract 1")
	fhs := make([]clientlib.FileHandleT, 0)
	modes := make([]string, 0)
	for i := 0; i < 1000; i++ {
		e, fh := chubby.Create(
			fmt.Sprintf("file%d", i),
			servernode.WriteHandle,
			events.EventNil,
			"content",
			servernode.FileACLInfo{
				ACLChanger: addrs,
				Reader:     addrs,
				Writer:     addrs,
			},
		)
		testp.NotError(e)
		fhs = append(fhs, fh)
		modes = append(modes, "W")

	}
	succ, e := ct.TryMultiAcquire(100, fhs, modes)
	testp.NotError(e)
	testp.As(succ)
}

func TestContract_TryMultiRelease(t *testing.T) {
	log.SetFlags(log.Lshortfile | log.Ldate)
	ready := make(chan bool)
	testp.ServerDnsBootstrap(ready)
	//<-ready
	addrs := make([]string, 0)
	clientAddr := testp.GenerateClientAddr(addrs)
	addrs = append(addrs, clientAddr)
	e, chubby := clientlib.NewChubbyClient(addrs[0])
	testp.NotError(e)
	ct := chubby.NewContract("gfs", "contract 1")
	fhs := make([]clientlib.FileHandleT, 0)
	modes := make([]string, 0)
	for i := 0; i < 100; i++ {
		e, fh := chubby.Create(
			fmt.Sprintf("file%d", i),
			servernode.WriteHandle,
			events.EventNil,
			"content",
			servernode.FileACLInfo{
				ACLChanger: addrs,
				Reader:     addrs,
				Writer:     addrs,
			},
		)
		testp.NotError(e)
		fhs = append(fhs, fh)
		modes = append(modes, "W")

	}
	succ, e := ct.TryMultiAcquire(100, fhs, modes)
	testp.NotError(e)
	testp.As(succ)
	succ, e = ct.TryMultiRelease(100)
	testp.NotError(e)
	testp.As(succ)
}
