package clientlib

import (
	"dns"
	"errors"
	"events"
	"fmt"
	"log"
	"regexp"
	"servernode"
	"sync"
	"time"
	//"strings"
	"strings"
)

// ClientT a basic interface for ChubbyClient
type ClientT interface {
	Open(filePath string, handleType string, eventSubscribed events.EventType,
		createFile bool, initialContent string, initialACL servernode.FileACLInfo, isDir bool) (error, FileHandleT)
	ListAllFiles() ([]string, error)
	Close(filePath string) error
	CloseWith(fh FileHandleT) error
	ShutDown()
	Reconnect() error
	OnDisconnect(handler DisconnectHandler)
}

// ChubbyClient A default Chubby Client implementation
type ChubbyClient struct {
	addr               string
	servRPCClient      servernode.ServerNodeT
	dnsRPCclient       dns.DNST
	handles            map[string]FileHandleT
	m                  sync.Mutex
	clientDown         bool
	errCh              chan error
	disconnectHandlers []DisconnectHandler
}

var _ ClientT = &ChubbyClient{}

// DefaultClientTimeout default client timeout
var DefaultClientTimeout = time.Second * 12

// NewChubbyClient constructor used by user
func NewChubbyClient(selfAddr string) (error, *ChubbyClient) {
	dnsClient := dns.NewDnsRPCclient(dns.DNSAddr)
	serverIP := ""

	if err := dnsClient.GetIPwithName(servernode.Primary, &serverIP); err != nil {
		return err, nil
	}

	log.Printf("Server IP looked up is: %s \n", serverIP)

	if !strings.Contains(serverIP, ":") {
		return errors.New("not a valid server IP address"), nil
	}

	client := &ChubbyClient{
		addr:          selfAddr,
		dnsRPCclient:  dnsClient,
		handles:       make(map[string]FileHandleT),
		m:             sync.Mutex{},
		servRPCClient: servernode.NewServerRPCClient(serverIP),
		clientDown:    false,
		errCh:         make(chan error),
	}

	go client.listening()

	return nil, client
}

func (chubby *ChubbyClient) requireInitSession() error {
	chubby.m.Lock()
	defer chubby.m.Unlock()

	if len(chubby.handles) == 0 {
		log.Println("trigger open session")
		return chubby.openSession()
	}
	return nil
}

func (chubby *ChubbyClient) openSession() error {
	openReq := servernode.OpenSessionRequestInfo{
		Addr: chubby.addr,
	}
	openRes := servernode.OpenSessionReplyInfo{}
	if e := chubby.servRPCClient.OpenSession(openReq, &openRes); e != nil || !openRes.Succ {
		return errors.New("fail to open session")
	}
	return nil
}

func (chubby *ChubbyClient) closeSession() error {
	closeReq := servernode.CloseSessionRequestInfo{
		Addr: chubby.addr,
	}
	closeRes := servernode.CloseSessionReplyInfo{}
	if e := chubby.servRPCClient.CloseSession(closeReq, &closeRes); e != nil || !closeRes.Succ {
		return errors.New("fail to close session")
	}
	return nil
}

func (chubby *ChubbyClient) ShutDown() {
	chubby.clientDown = true
	closeReq := servernode.CloseSessionRequestInfo{
		Addr: chubby.addr,
	}
	closeRes := servernode.CloseSessionReplyInfo{}
	if e := chubby.servRPCClient.CloseSession(closeReq, &closeRes); e != nil {
		log.Println("fail to close session")
	}
}

// OpenExisting opens existing file for read or write
func (chubby *ChubbyClient) OpenExisting(
	filePath string,
	handleType string,
) (error, FileHandleT) {
	return chubby.Open(
		filePath,
		handleType,
		events.EventNil,
		false,
		"",
		servernode.FileACLInfo{},
		false,
	)
}

// Create create a file with initial content and initial ACL list
// The handle type would be server.WriteHandle.
func (chubby *ChubbyClient) Create(
	filePath string,
	handleType string,
	eventsSubscribed events.EventType,
	initialContent string,
	initialACL servernode.FileACLInfo,
) (error, FileHandleT) {
	return chubby.Open(
		filePath,
		servernode.WriteHandle,
		eventsSubscribed,
		true,
		initialContent,
		initialACL,
		false,
	)
}

// Open open a remote Chubby file
func (chubby *ChubbyClient) Open(
	filePath string,
	handleType string,
	eventsSubscribed events.EventType,
	createFile bool,
	initialContent string,
	initialACL servernode.FileACLInfo,
	isDir bool,
) (error, FileHandleT) {
	if chubby.clientDown {
		panic("the client is already shut down")
	}

	// create a temporary RPC client to talk to Chubby master
	req := servernode.GetHandleRequestInfo{
		FilePath:         filePath,
		HandleType:       handleType,
		EventsSubscribed: eventsSubscribed,
		CreateFile:       createFile,
		InitialContent:   initialContent,
		InitialACL:       initialACL,
		IsDir:            isDir,
		Addr:             chubby.addr,
	}

	if err := chubby.checkGetHandleReq(&req); err != nil {
		return err, nil
	}
	if err := chubby.requireFileHandleNotExist(filePath); err != nil {
		return err, nil
	}

	if err := chubby.requireInitSession(); err != nil {
		return err, nil
	}

	reply := &servernode.GetHandleReplyInfo{}
	if e := chubby.servRPCClient.GetHandle(req, reply); e != nil {
		return e, nil
	}

	// return another RPC client as a file handle to user
	if reply.Succ {
		// add the new handle to the handle maps of the Chubby client
		fileHandle := newFileHandle(filePath, handleType, chubby.addr, isDir, eventsSubscribed, chubby.servRPCClient)
		chubby.m.Lock()
		chubby.handles[filePath] = fileHandle
		chubby.m.Unlock()
		chubby.requireKeepAlive()
		return nil, fileHandle
	}
	return errors.New("Open succ failed"), nil
}

func (chubby *ChubbyClient) requireFileHandleNotExist(filePath string) error {
	if _, ok := chubby.handles[filePath]; ok {
		return fmt.Errorf("file handle %s already exist", filePath)
	}
	return nil
}

func (chubby *ChubbyClient) requireKeepAlive() {
	//chubby.handles[filePath] = fileHandle
	// start KeepAlive session if this is the first handle this Chubby client has ever received
	if len(chubby.handles) == 1 {
		go func() {
			chubby.errCh <- chubby.keepAlive()
		}()
	}
}

func (chubby *ChubbyClient) CloseWith(filehandle FileHandleT) error {
	return chubby.Close(filehandle.GetFileName())
}

// close this call never fail
func (chubby *ChubbyClient) Close(filePath string) error {
	if filehandle, exist := chubby.handles[filePath]; exist {
		// use file handle to tell the server about the close
		req := servernode.ReturnHandleRequestInfo{FilePath: filePath, ClientAddr: chubby.addr}
		reply := servernode.ReturnHandleReplyInfo{}
		if err := chubby.servRPCClient.ReturnHandle(req, &reply); err != nil {
			return err
		}

		filehandle.close()

		// remove entry (file name : file handle) from the handle map
		chubby.m.Lock()
		delete(chubby.handles, filePath)
		chubby.m.Unlock()
		if len(chubby.handles) == 0 {
			if e := chubby.closeSession(); e != nil {
				return e
			}
		}
		return nil
	}

	return fmt.Errorf("File %s has never been opened", filePath)
}

func (chubby *ChubbyClient) dealWithEvent(e *events.Event) {
	log.Println(e.Type)
	log.Println(e.Msg)
	switch e.Type {
	case events.EventNil:
		return
	case events.EventHandlerInvalid:

		fileRegex := e.Msg

		chubby.m.Lock()
		defer chubby.m.Unlock()
		for file, handle := range chubby.handles {
			match, e := regexp.MatchString(fileRegex, file)
			if match && e == nil {
				handle.Invalidate()
			}
			if e != nil {
				log.Println("failed to match handle to invalidate because", e.Error())
			}
		}
		// case events.EventFileContentModified:
	}
}

// private function
func (chubby *ChubbyClient) keepAlive() error {

	log.Printf("%s KeepAlive session starting from the client", chubby.addr)
	req := new(servernode.KeepLiveRequestInfo)
	req.ClientAddr = chubby.addr
	reply := new(servernode.KeepLiveReplyInfo)
	var err error

	for true {
		if chubby.clientDown {
			log.Printf("%s KeepAlive session is over", chubby.addr)
			return nil
		}

		log.Printf("%s KeepAlive session continues!", chubby.addr)
		// this call is going to be blocked on the server side for about "lease" number of seconds
		err = chubby.servRPCClient.KeepLive(*req, reply)
		if err != nil {
			return err
		}

		if reply.Event.Type != events.EventNil {
			log.Printf("There is an event for %s !", chubby.addr)
			chubby.dealWithEvent(&reply.Event)
		}

		if reply.Terminate && reply.Event.Type == events.EventSessionDeprecated {
			log.Println("client's session has been deprecated")
			return nil
		}

		if reply.Terminate == true { // when the client close all the file handle
			chubby.dealWithEvent(&events.Event{Msg: "*", Type: events.EventHandlerInvalid})
			//<-time.After(DefaultClientTimeout)
			return nil
		}

		//log.Printf("%s KeepAlive session receives response %s from Chubby server!", chubby.addr, reply.Event.Msg)
	}
	log.Printf("%s KeepAlive session terminated!", chubby.addr)
	return err // session terminates successfully
}

// ListAllFiles make a ListAllFiles RPC call
func (chubby *ChubbyClient) ListAllFiles() ([]string, error) {
	listReply := &servernode.ListReply{L: make([]string, 0)}
	if err := chubby.servRPCClient.ListAllFiles("", listReply); err != nil {
		log.Printf("servRPCClient.ListAllFiles() failed: %s", err.Error())
		return nil, err
	}

	files := listReply.L

	log.Println("current files: ")
	for _, file := range files {
		log.Print(file)
	}
	return files, nil
}

func (chubby *ChubbyClient) checkGetHandleReq(req *servernode.GetHandleRequestInfo) error {
	// check if Handle is valid
	if t := req.HandleType; t != servernode.ReadHandle && t != servernode.WriteHandle {
		return fmt.Errorf("Invalid handle: %s", req.HandleType)
	}

	// check if file path already has a handle
	if _, exist := chubby.handles[req.FilePath]; exist {
		return fmt.Errorf("file %s already has a handle", req.FilePath)
	}

	return nil
}

func (chubby *ChubbyClient) Reconnect() error {
	serverIP := ""
	if e := chubby.dnsRPCclient.GetIPwithName(servernode.Primary, &serverIP); e != nil {
		return e
	}
	chubby.servRPCClient = servernode.NewServerRPCClient(serverIP)
	chubby.openSession()
	return nil
}

type DisconnectHandler func(e error) error

// OnDisconnect registers a handler for disconnection
func (chubby *ChubbyClient) OnDisconnect(handler DisconnectHandler) {
	if chubby == nil {
		return
	}
	chubby.m.Lock()
	defer chubby.m.Unlock()
	chubby.disconnectHandlers = append(chubby.disconnectHandlers, handler)
}

func (chubby *ChubbyClient) listening() {
	for {
		e := <-chubby.errCh
		if e == nil {
			return
		}

		if len(chubby.disconnectHandlers) != 0 {
			for _, handler := range chubby.disconnectHandlers {
				if err := handler(e); err != nil {
					log.Printf("unchecked error: %s", err.Error())
				}
			}
		} else {
			log.Printf("no handler for error: %s, default handler Reconnect is called", e.Error())
			chubby.Reconnect()
		}
	}
}
