package clientlib

import (
	"errors"
	"fmt"
	"log"
	"logentry"
	"os"
	"path/filepath"
	"servernode"
	"sync"
)

// Bytes indicating whether the log entry is done

// ContractT A contract interface
type ContractT interface {
	Register(action func(ContractT) error) error
	TryMultiAcquire(retries int, fh []FileHandleT, modes []string) (bool, error)
	TryMultiRelease(retries int) (bool, error)
	WillRead(fh FileHandleT) (string, error)
	WillWrite(fh FileHandleT, content string) error
	Commit() error
}

var (
	ContractEndedErr = errors.New("the contract is ended")
)

// contract is a structure representing a transaction.
// Its usage will be like:
// contract := chubbyClient.NewContract("./gfs", "whoami")
// if e := contract.Register(func(contract ContractT) error {
// 	f1, e1 := contract.WillRead(fh)
// 	if e1 != nil {
// 		return e1
// 	}
// 	e2 := contract.WillWrite(fh, f1)
// 	if e2 != nil {
// 		return e2
// 	}
// 	return nil
// }); e != nil {
// 	contract.Commit()
// }
type contract struct {
	sync.Mutex
	writes  map[string]string
	locks   map[string]string
	addr    string
	baseDir string
	id      string
	servRPC servernode.ServerNodeT
	isEnded bool
}

var _ ContractT = &contract{}

// Do all operations specified in contract and store the final SetContent to GFS
func (ct *contract) Register(action func(ct ContractT) error) error {
	if e := action(ct); e != nil {
		return e
	}
	return ct.flush()
}

func (ct *contract) TryMultiAcquire(retries int, fhs []FileHandleT, modes []string) (bool, error) {
	if e := ct.requireContractValid(); e != nil {
		return false, e
	}
	if len(fhs) != len(modes) {
		panic("#file handles and #modes are not equal")
	}
	fnames := make([]string, 0)
	for i, fh := range fhs {
		ct.locks[fh.GetFileName()] = modes[i]
		fnames = append(fnames, fh.GetFileName())
	}
	var err error
	for retries > 0 {
		req := servernode.TryMultiAcquireRequestInfo{
			Acquirer:  ct.addr,
			Modes:     modes,
			FilePaths: fnames,
		}
		res := servernode.TryMultiAcquireReplyInfo{}
		if e := ct.servRPC.TryMultiAcquire(req, &res); e != nil || !res.Succ {
			err = e
			retries--
		}
		return true, nil
	}
	return false, err
}

func (ct *contract) TryMultiRelease(retries int) (bool, error) {
	if e := ct.requireContractValid(); e != nil {
		return false, e
	}
	files := make([]string, 0)
	modes := make([]string, 0)
	for filename, mode := range ct.locks {
		files = append(files, filename)
		modes = append(modes, mode)
	}
	req := servernode.TryMultiReleaseRequestInfo{
		Modes:     modes,
		Holder:    ct.addr,
		FilePaths: files,
	}
	res := servernode.TryMultiReleaseReplyInfo{}
	var err error
	for retries > 0 {
		if e := ct.servRPC.TryMultiRelease(req, &res); e != nil {
			log.Println(e)
			retries--
			err = e
		} else {
			ct.locks = make(map[string]string)
			return true, nil
		}
	}

	return false, err
}

// Read from either Chubby server or from local "shadow object"
func (ct *contract) WillRead(fh FileHandleT) (string, error) {
	if e := ct.requireContractValid(); e != nil {
		return "", e
	}
	handleLockStatus := fh.GetLockStatus()
	if handleLockStatus == holdNoLock {
		return "", errors.New("Can't read if no read/write lock is held by the handle")
	}
	if w, ok := ct.writes[fh.GetFileName()]; ok {
		return w, nil
	}

	e, content, _ := fh.GetContentsAndStat()
	if e != nil {
		return "", e
	}
	return content.Content, nil
}

// Write to a local "shadow object"
func (ct *contract) WillWrite(fh FileHandleT, content string) error {
	if e := ct.requireContractValid(); e != nil {
		return e
	}
	handleLockStatus := fh.GetLockStatus()

	if handleLockStatus != holdWriteLock {
		return errors.New("Can't write if no write lock is held by the handle")
	}

	ct.Lock()
	defer ct.Unlock()
	// set content of local "shadow object"
	ct.writes[fh.GetFileName()] = content
	fh.Invalidate() // TODO:
	return nil
}

// Commit commit all the registered operations
func (ct *contract) Commit() error {
	if e := ct.requireContractValid(); e != nil {
		return e
	}
	// acquire path
	dir, e := ct.getDir()
	if e != nil {
		log.Println(e.Error())
		return e
	}

	log.Printf("In Commit() for %s and file %s *******", ct.addr, dir)

	if e := ct.requireFileValid(dir, ct.id); e != nil {
		log.Printf("file %s is not valid: %s", dir, e.Error())
		return e
	}

	path := filepath.Join(dir, ct.id)
	if e := logentry.CheckFileLegal(path); e != nil {
		log.Printf("file %s is not legal: %s", path, e.Error())
		return e
	}

	// send contract
	// send server the path to our contract in case the client crashed in the middle of applying
	// logs
	req := servernode.SendContractRequestInfo{Addr: ct.addr, Id: ct.id, FilePath: path}
	reply := &servernode.SendContractReplyInfo{}
	if e := ct.servRPC.SendContract(req, reply); e != nil {
		log.Println(e.Error())
		return e
	}

	// do work
	if e := ct.doWork(path); e != nil {
		return e
	}

	// delete file
	if e := os.Remove(path); e != nil {
		log.Printf("removed %s failed", path)
		return e
	}

	// delete contract
	dcreq := servernode.DeleteContractRequestInfo{Addr: ct.addr, Id: ct.id, Realpath: ""}
	dcreply := servernode.DeleteContractReplyInfo{}
	if e := ct.servRPC.DeleteContract(dcreq, &dcreply); e != nil {
		log.Printf("DeleteContract has error: %s", e.Error())
		return e
	}
	ct.isEnded = true

	return nil
}

func (ct *contract) doWork(path string) error {
	//Do work
	file, e := os.OpenFile(path, os.O_RDWR, os.ModePerm)
	if e != nil {
		log.Println(e.Error())
		return e
	}
	var logEntry *logentry.LogEntry
	for {
		logEntry, e = logentry.ReadNextLogEntry(file)
		if e == logentry.EOF {
			log.Println(e.Error())
			break
		}
		if logEntry.IsDone == true {
			continue
		}
		log.Printf("Write content is %s, File name is %s\n", logEntry.WriteContent, logEntry.FileName)
		request := servernode.SetContentsRequestInfo{
			Content:  logEntry.WriteContent,
			FilePath: logEntry.FileName,
		}
		reply := servernode.SetContentsReplyInfo{
			Succ: false,
		}
		if err := ct.servRPC.SetContents(request, &reply); err != nil {
			return err
		}
		logentry.SetLastDone(file)
	}
	return file.Close()
}

// NewContract create a new contract for atomic operations
func (chubby *ChubbyClient) NewContract(baseDir, id string) ContractT {
	return &contract{
		addr:    chubby.addr,
		baseDir: baseDir,
		id:      id,
		Mutex:   sync.Mutex{},
		writes:  make(map[string]string),
		servRPC: chubby.servRPCClient,
		locks:   make(map[string]string),
		isEnded: false,
	}
}

/*
	Helper functions
*/

func (ct *contract) requireContractValid() error {
	if ct.isEnded {
		return ContractEndedErr
	}
	return nil
}

func (ct *contract) requireDirExist(suffix ...string) error {
	parts := []string{ct.baseDir, ct.addr}
	parts = append(parts, suffix...)
	path := filepath.Join(parts...)
	stat, e := os.Stat(path)

	if stat != nil && !stat.IsDir() && e == nil {
		return fmt.Errorf("%s already exist, and it is not a directory", path)
	}
	if e != nil && os.IsNotExist(e) {
		if err := os.MkdirAll(path, os.ModePerm); err != nil {
			return err
		}
	}
	return nil
}

func (ct *contract) requireFileValid(dir, file string) error {
	p := filepath.Join(dir, file)
	stat, e := os.Stat(p)
	if stat != nil && stat.IsDir() && e == nil {
		return fmt.Errorf("%s already exist, and it's not a file", p)
	}
	return nil
}

func (ct *contract) getDir() (string, error) {
	if e := ct.requireDirExist(); e != nil {
		return "", e
	}

	return filepath.Join(ct.baseDir, ct.addr), nil
}

// flush Flush all the write operations to the GFS.
// The format of the log file will be:
// n = Length of Filename  | Filename  | m = Length of Content | Content | Flag
//     4 bytes             | n bytes   |       4 bytes         | m bytes | 1 byte
// So for each log entry, we need to write 4 bytes representing a interger,
// which is the length of the filename string. Then we write the filename.
// Again write 4 bytes representing the length of the content string and
// then the content. At last, we write a flat (a byte) to represent whether
// the operation is done. For now, we only support:
// Done: 0xff and NotDone 0x00
func (ct *contract) flush() error {
	if e := ct.requireDirExist(); e != nil {
		return e
	}
	dir, e := ct.getDir()
	if e != nil {
		return e
	}
	if e := ct.requireFileValid(dir, ct.id); e != nil {
		return e
	}
	path := filepath.Join(dir, ct.id)
	file, e := os.OpenFile(path, os.O_RDWR|os.O_CREATE, os.ModePerm)
	if e != nil {
		return e
	}
	var ioe error
	defer func() {
		if ioe != nil {
			//os.Remove(path)
			file.Close()
		}
	}()
	for f, w := range ct.writes {
		logentry.WriteNextLogEntry(
			file,
			logentry.LogEntry{
				FileName:     f,
				WriteContent: w,
				IsDone:       false,
			},
		)
	}
	return nil
}
