package dns

import (
	"errors"
	"fmt"
	"store"
	"strings"
)

var DNSAddr string = "localhost:6666"

type DNST interface {
	Register(kv *store.KeyValue, succ *bool) error
	GetIPwithName(domainName string, IP *string) error
}

type DNS struct {
	addr    string
	storage store.StorageT
}

func (self *DNS) Register(kv *store.KeyValue, succ *bool) error {
	// make sure the IP address being registered is in a valid format
	IP := kv.Value
	if strings.Contains(IP, ":") {
		return self.storage.Set(kv, succ)
	}

	return errors.New(fmt.Sprintf("Registering invalid IP address: %s", IP))
}

func (self *DNS) GetIPwithName(domainName string, IP *string) error {
	return self.storage.Get(domainName, IP)
}

func NewDNS() *DNS {
	return &DNS{
		addr:    DNSAddr,
		storage: store.NewStorage(),
	}
}
