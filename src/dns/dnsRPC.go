package dns

import (
	"log"
	"net"
	"net/rpc"
	"store"
	"sync"
	"time"
	"math"
)

// define the store RPC type
type DnsRPCclient struct {
	addr string
	m    *sync.Mutex
	conn *rpc.Client
}

// functions of type DnsRPCclient
func (c *DnsRPCclient) requireConn() error {
	c.m.Lock()
	defer c.m.Unlock()
	if c.conn == nil {
		conn, err := rpc.Dial("tcp", c.addr)
		c.conn = conn
		return err
	}
	return nil
}

func (c *DnsRPCclient) call(method string, args interface{}, reply interface{}) error {
	if err := c.requireConn(); err != nil {
		return err
	}
	i := 0
	for err := c.conn.Call(method, args, reply); err != nil && i < 10; i++ {
		c.conn = nil
		sleep := time.Duration(math.Pow(2, float64(i)))
		time.Sleep(sleep * 100 * time.Millisecond)
		return err
	}

	return nil
}

func (c *DnsRPCclient) GetIPwithName(key string, val *string) error {
	return c.call("DNS.GetIPwithName", key, val)
}

func (c *DnsRPCclient) Register(domainNameIP *store.KeyValue, succ *bool) error {
	return c.call("DNS.Register", domainNameIP, succ)
}

//NewClient Creates an RPC client that connects to addr.
func NewDnsRPCclient(addr string) DNST {
	c := &DnsRPCclient{
		addr: addr,
		m:    &sync.Mutex{},
		conn: nil,
	}
	return c
}

//ServeBack Serve as a backend based on the given configuration
func ServeDNSRPC(s DNST, ready *chan bool) error {
	server := rpc.NewServer()
	server.RegisterName("DNS", s)

	ln, err := net.Listen("tcp", DNSAddr)
	if err != nil {
		feedReady(ready, false)
		return err
	}
	defer ln.Close()
	// DefConnMngr.AddListener(b.Addr, ln)
	feedReady(ready, true)

	conns := []net.Conn{}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("The DNS server %s is dead", DNSAddr)
			for _, c := range conns {
				c.Close()
			}
			return err
		}
		conns = append(conns, conn)
		go server.ServeConn(conn)
	}
}

func feedReady(r *chan bool, v bool) {
	if r != nil && *r != nil {
		*r <- v
		if v {
			log.Println("DNS set true to ready")
		}
	} else {
		log.Println("DNS ready channel is nil.")
	}
}
