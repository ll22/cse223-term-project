package dns_test

import (
	"dns"
	"errors"
	"runtime/debug"
	"store"
	"testing"
	"testp"
)

func TestDNS(t *testing.T) {
	ready := make(chan bool)

	go func() {
		if e := dns.ServeDNSRPC(dns.NewDNS(), &ready); e != nil {
			t.Fatal(e)
		}
	}()

	r := <-ready

	if !r {
		t.Fatal("not ready")
	}
	c := dns.NewDnsRPCclient(dns.DNSAddr)
	CheckDNS(t, &c) // defined in store_test.go
}

func CheckDNS(t *testing.T, s *dns.DNST) {
	var v string
	var b bool
	//var l = new(store.List)
	//var n int

	ne := func(e error) {
		if e != nil {
			debug.PrintStack()
			t.Fatal(e)
		}
	}

	isError := func(e error) {
		if e == nil {
			t.Fatal(errors.New("Should have error"))
		}
	}

	as := func(cond bool) {
		if !cond {
			debug.PrintStack()
			t.Fatal("assertion failed")
		}
	}

	kv := func(k, v string) *store.KeyValue {
		return &store.KeyValue{k, v}
	}

	testp.NotError((*s).Register(kv("P1", "localhost:123"), &b))
	testp.As(b)

	testp.NotError((*s).GetIPwithName("B1", &v))
	testp.As(v == "")

	testp.NotError((*s).Register(kv("B1", "localhost:456"), &b))
	testp.As(b)
	v = ""
	testp.NotError((*s).GetIPwithName("B1", &v))
	testp.As(v == "localhost:456")

	testp.NotError((*s).Register(kv("B2", "localhost:789"), &b))
	testp.As(b)
	v = ""
	testp.NotError((*s).GetIPwithName("B2", &v))
	testp.As(v == "localhost:789")

	testp.NotError((*s).Register(kv("B3", "localhost:123"), &b))
	testp.As(b)

	testp.NotError((*s).GetIPwithName("B3", &v))
	testp.As(v == "localhost:123")

	testp.NotError((*s).Register(kv("B3", "localhost:123421"), &b))
	testp.As(b)

	testp.NotError((*s).GetIPwithName("B3", &v))
	testp.As(v == "localhost:123421")

	isError((*s).Register(kv("B3", "123421"), &b))
}
