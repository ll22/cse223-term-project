package stresstest_test

import (
	"clientlib"
	"events"
	"math/rand"
	//"io/ioutil"
	"log"
	"servernode"
	"strconv"
	"testing"
	"testp"
	"time"
)

func Test_OneServer1(t *testing.T) {
	t.Skip()
	log.SetFlags(log.Lshortfile | log.Ldate)
	//log.SetFlags(0)
	//log.SetOutput(ioutil.Discard)
	// ******************************************************
	// System bootstrap
	// ******************************************************
	var ready chan bool
	var serverAddr string
	var serverStoreAddr string
	//var rwFiles string = "/ls/foo/wombat/pouch"
	//var fh clientlib.FileHandleT
	//var err error

	existingAddresses := make([]string, 0)

	ready = make(chan bool)
	// start server, server store and DNS server
	serverAddr, serverStoreAddr = testp.ServerDnsBootstrap(ready)
	existingAddresses = append(existingAddresses, serverAddr)
	existingAddresses = append(existingAddresses, serverStoreAddr)

	// ******************************************************
	// Tests start
	// ******************************************************
	start := time.Now()
	multipleClientsMultipleReads1(10, 100, 500, 0)
	log.Println("Test_OneServers uses: ", time.Since(start).Seconds(), "seconds")
}

func Test_PaxosServers1(t *testing.T) {
	//t.Skip()
	_ = testp.PaxosDnsBootstrap()
	log.Println("address=", testp.ExistingAddres)
	res := make([]float64, 0)
	for i := 0; i < 10; i++ {
		start := time.Now()
		multipleClientsMultipleReads1(20, 100, 500, 0)
		res = append(res, time.Since(start).Seconds())
		log.Println("Test_PaxosServers uses: ", res, "seconds")
		//	time.Sleep(5 * time.Second)
		log.Print("now i:", i)
	}
	log.Println("Test_PaxosServers uses: ", res, "seconds")

}

func multipleClientsMultipleReads1(clientNum int, openFileNum int, eachClientReadNum int, maxClientOpenPeriod int) {
	var done chan bool
	var clientAddr string
	done = make(chan bool, 0)

	startClientCount := clientNum
	for startClientCount > 0 {
		startClientCount--
		clientAddr = testp.GenerateClientAddr(testp.ExistingAddres)
		testp.ExistingAddres = append(testp.ExistingAddres, clientAddr)
		go startOneClientRead1(clientAddr, openFileNum, eachClientReadNum, maxClientOpenPeriod, done)
	}

	finishCount := 0
	for clientNum > finishCount {
		finishCount++
		<-done
		log.Printf(strconv.Itoa(finishCount) + " done! \n")
	}
}

func startOneClientRead1(clientAddr string, openFileNum int, eachClientReadNum int, maxClientOpenPeriod int, done chan bool) {
	log.SetFlags(log.Ltime | log.Lshortfile)
	var defaultTestFilePath string
	var fileCount int
	var testFilePath string
	var err error
	var clientOpenPeriod int
	//var randIndex int
	var initialContent string
	var fh clientlib.FileHandleT

	fileCount = 1
	defaultTestFilePath = "/ls/foo/wombat/pouch"
	initialContent = "blah blah blah 1"
	// start a Chubby client
	e, client := clientlib.NewChubbyClient(clientAddr)
	testp.NotError(e)
	log.Printf("Chubby client started at: %s: ", clientAddr)

	// open a bunch of files
	openCount := openFileNum
	openedFiles := make([]string, 0)
	for openCount > 0 {
		openCount--

		testFilePath = testp.NewFileName(defaultTestFilePath, fileCount)
		testFilePath += clientAddr
		fileCount++
		openedFiles = append(openedFiles, testFilePath)

		err, fh = client.Open(
			testFilePath,
			servernode.WriteHandle,
			events.EventFileContentModified,
			true,
			initialContent,
			servernode.FileACLInfo{
				Writer:     []string{clientAddr},
				Reader:     []string{clientAddr},
				ACLChanger: []string{clientAddr},
			},
			false,
		)
		testp.NotError(err)
		log.Printf("Client opens file %s succeed!\n", testFilePath)
	}

	//err = fh.Acquire("R", 1)
	//testp.NotError(err)

	// client keep randomly reading from all opened files
	for eachClientReadNum > 0 {
		eachClientReadNum--
		e3, content, _ := fh.GetContentsAndStat()
		testp.NotError(e3)
		testp.As(content.Content == initialContent)
	}

	//testp.NotError( fh.Release() )
	//time.Sleep(time.Duration(10) * time.Second)

	openCount = len(openedFiles)
	for openCount > 0 {
		//randIndex = randomInt(openCount)

		log.Printf("Client trying to close file : %s\n", openedFiles[openCount-1])
		printDelim()
		err = client.Close(openedFiles[openCount-1])
		testp.NotError(err)
		log.Printf("Client closes file %s succeed!\n", openedFiles[openCount-1])
		clientOpenPeriod = randomInt(maxClientOpenPeriod)
		log.Printf("Client delays %d\n", clientOpenPeriod)
		time.Sleep(time.Duration(clientOpenPeriod) * time.Second)
		openCount--
	}
	log.Printf("All files closed\n")
	if done != nil {
		done <- true
	}
}

func randomInt(max int) int {
	if max == 0 {
		return 0
	}
	rand.Seed(time.Now().Unix())
	return rand.Intn(max)
}

func printDelim() {
	log.Printf("**************\n\n")
}
