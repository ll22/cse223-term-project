package paxos

//
// Paxos library, to be included in an application.
// Multiple applications will run, each including
// a Paxos peer.
//
// Manages a sequence of agreed-on values.
// The set of peers is fixed.
// Copes with network failures (partition, msg loss, &c).
// Does not store anything persistently, so cannot handle crash+restart.
//
// The application interface:
//
// px = paxos.Make(peers []string, me string)
// px.Start(seq int, v interface{}) -- start agreement on new instance
// px.Status(seq int) (decided bool, v interface{}) -- get info about an instance
// px.Dotest_primitives.NotError(seq int) -- ok to forget all instances <= seq
// px.Max() int -- highest instance seq known, or -1
// px.Min() int -- instances before this seq have been forgotten
//

import "net"
import "net/rpc"
import "log"
import "syscall"
import "sync"
import "fmt"
import (
	"math/rand"
	"time"
)
import (
	"strings"
	"util"
	"math"
)

var paxosUnreliable = false

type ArgsIsAlive struct {
	Sender int
}

type ArgsPushProposal struct {
	Sender   int
	Seq      int
	Proposal interface{}
}

type ArgsPrepare struct {
	Sender int
	Seq    int
	Round  int
}

type ArgsAcceptRequire struct {
	Sender   int
	Seq      int
	Round    int
	Proposal interface{}
}

type ArgsDecide struct {
	Sender   int
	Seq      int
	Decision interface{}
}

type ArgsUpdateDone struct {
	Sender int
	Done   int
}

type ReplyTypeIsAlive struct {
	Modified bool
	IsAlive  bool
}

type ReplyTypePushProposal struct {
	Modified bool
	Pushed   bool
	Decided  bool
	Decision interface{}
}

type ReplyTypePrepare struct {
	Modified         bool
	Round            int
	AcceptedProposal interface{}
}

type ReplyTypeAcceptRequire struct {
	Modified bool
	Accepted bool
}

type ReplyTypeDecide struct {
	Modified bool
	Decided  bool
}

type ReplyTypeUpdateDone struct {
	Modified bool
	Rec      bool
}

type Paxos struct {
	mu         sync.Mutex
	l          net.Listener
	dead       bool
	unreliable bool
	rpcCount   int
	peers      []string
	me         int // index into peers[]

	// Your data here.
	num_peers    int
	num_majority int

	decision    []interface{}
	num_decided []int
	decided     []bool

	accepts       []interface{}
	acceptrounds  []int
	preparerounds []int

	proposetoken     []bool
	proposeround     []int
	originalproposal []interface{}
	isoriginal       []bool

	leader     int
	highestseq int
	lowestseq  int
	token      bool
	doneseq    []int
	persistentRPCs map[string]*util.PersistentRPC
	rpcMutex sync.Mutex
}

var (
	//BroadCastGap =50*Millisecond
	BroadCastDoneGap = 50 * time.Millisecond
	//BroadCastDecide =50*Millisecond
	BroadCastDecideGap = 50 * time.Millisecond
	//StartGap = 50*Millisecond: start a proposal
	StartGap = 1 * time.Microsecond //5 * time.Millisecond
	//ProposeGap=10*Millisecond
	ProposeGap = 1 * time.Microsecond
	//LeaderElectionGap = 100 * Millisecond
	LeaderElectionGap = 100 * time.Millisecond
)

/*
type PaxosT interface {
	UpdateDotest_primitives.NotError(Args ArgsUpdateDone, reply *ReplyTypeUpdateDone) error
	Decide(Args ArgsDecide, reply *ReplyTypeDecide) error
	AreYouAlive(Args ArgsIsAlive, reply *ReplyTypeIsAlive) error
	AcceptRequire(Args ArgsAcceptRequire, reply *ReplyTypeAcceptRequire) error
}*/

//
// call() sends an RPC to the rpcname handler on server srv
// with arguments args, waits for the reply, and leaves the
// reply in reply. the reply argument should be a pointer
// to a reply structure.
//
// the return value is true if the server responded, and false
// if call() was not able to contact the server. in particular,
// the replys contents are only valid if call() returned true.
//
// you should assume that call() will time out and return an
// error after a while if it does not get a reply from the server.
//
// please do not change this function.
//


func (px *Paxos) call(srv string, name string, args interface{}, reply interface{}) bool {
	px.rpcMutex.Lock()
	if _, ok := px.persistentRPCs[srv]; !ok {
		px.persistentRPCs[srv] = util.NewPersistentRPC(srv)
	}
	persistentRPC := px.persistentRPCs[srv]
	px.rpcMutex.Unlock()

	//fmt.Println("BeforeDial!!!!!")
	//c, err := rpc.Dial("tcp", srv)
	////fmt.Println("AfterDial!!!!!")
	//if err != nil {
	//	err1 := err.(*net.OpError)
	//	if err1.Err != syscall.ENOENT && err1.Err != syscall.ECONNREFUSED {
	//		//fmt.Printf("paxos Dial() failed: %v\n", err1)
	//	}
	//	return false
	//}
	//defer c.Close()

	// fmt.Println("BeforeCall!!!!!")
	i := 0
	for err := persistentRPC.TryCall(name, args, reply); err != nil && i < 10; i++ {
		time.Sleep(time.Duration(math.Pow(2, float64(i)) * 100) * time.Millisecond)
		fmt.Println("error " + err.Error())
	}
	// fmt.Println("AfterCall!!!!!")
	if i < 10 {
		return true
	}


	return false
}

func (px *Paxos) updatesSeq(seq int) {

	px.mu.Lock()
	if seq > px.highestseq {
		px.highestseq = seq
		//  fmt.Printf("%d %d hahahahaha!!!",px.me,seq)
		for len(px.decision) <= seq {

			px.decision = append(px.decision, nil)
			px.num_decided = append(px.num_decided, 0)
			px.decided = append(px.decided, false)

			px.accepts = append(px.accepts, nil)
			px.acceptrounds = append(px.acceptrounds, -1)
			px.preparerounds = append(px.preparerounds, -1)

			px.proposetoken = append(px.proposetoken, false)
			px.proposeround = append(px.proposeround, 0)
			px.originalproposal = append(px.originalproposal, nil)
			//     fmt.Printf("%d %d %d hehehehehe!!!",px.me,seq,len(px.originalproposal))
			px.isoriginal = append(px.isoriginal, false)

		}
	}
	px.mu.Unlock()
}

func (px *Paxos) PushProposal(Args ArgsPushProposal, reply *ReplyTypePushProposal) error {

	defer func() {
		(*reply).Modified = true
	}()

	px.updatesSeq(Args.Seq)
	px.mu.Lock()
	if px.isoriginal[Args.Seq] == false {
		px.originalproposal[Args.Seq] = Args.Proposal
		px.isoriginal[Args.Seq] = true
		(*reply).Pushed = true
		//fmt.Printf("%d is pused by %d\n", px.me, Args.Sender)
	} else {
		(*reply).Pushed = false
		//fmt.Printf("%d is not pused by %d\n", px.me, Args.Sender)
	}

	if px.decided[Args.Seq] {
		(*reply).Decision = px.decision[Args.Seq]
		(*reply).Decided = px.decided[Args.Seq]
	}
	px.mu.Unlock()

	return nil
}

//
// the application wants paxos to start agreement on
// instance seq, with proposed value v.
// Start() returns right away; the application will
// call Status() to find out if/when agreement
// is reached.
//

func (px *Paxos) Start(seq int, v interface{}) {
	// Your code here.
	if px.dead {
		return
	}

	px.updatesSeq(seq)
	go func() {

		/* var hash []bool
		   for i:=0; i < px.num_peers; i++ {
		     hash = append(hash,false)
		   }*/
		for !px.decided[seq] && !px.dead {

			px.mu.Lock()
			curleader := px.leader
			px.mu.Unlock()

			// if !hash[curleader] {
			for i := 0; i < 5; i++ {
				if curleader != px.me {
					//  fmt.Printf("%s starts to call Paxos.PushProposal of %s\n",px.peers[px.me],px.peers[curleader])
					// px.calllock.Lock()

					var reply *ReplyTypePushProposal
					var Args ArgsPushProposal
					Args.Sender = px.me
					Args.Seq = seq
					Args.Proposal = v

					reply = &ReplyTypePushProposal{}
					reply.Modified = false

					acc := px.call(px.peers[curleader], "Paxos.PushProposal", Args, reply)
					// px.calllock.Unlock()
					//  fmt.Printf("%s ends to call Paxos.PushProposal of %s\n",px.peers[px.me],px.peers[curleader])
					if acc {
						//     hash[curleader] = true
						if reply.Decided && reply.Modified {
							var reply2 *ReplyTypeDecide
							var Args2 ArgsDecide
							Args2.Sender = px.me
							Args2.Seq = seq
							Args2.Decision = reply.Decision

							reply2 = &ReplyTypeDecide{}
							reply2.Modified = false
							go px.Decide(Args2, reply2)

							return
						}
						break
					}
				} else {

					var reply *ReplyTypePushProposal
					var Args ArgsPushProposal
					Args.Sender = px.me
					Args.Seq = seq
					Args.Proposal = v

					reply = &ReplyTypePushProposal{}
					reply.Modified = false

					_ = px.PushProposal(Args, reply)
					//  hash[curleader] = true
					if reply.Decided && reply.Modified {
						var reply2 *ReplyTypeDecide
						var Args2 ArgsDecide
						Args2.Sender = px.me
						Args2.Seq = seq
						Args2.Decision = reply.Decision

						reply2 = &ReplyTypeDecide{}
						reply2.Modified = false
						go px.Decide(Args2, reply2)

						return
					}
					break
				}
			}
			// }

			time.Sleep(StartGap)
		}

	}()
}

//
// the application on this machine is done with
// all instances <= seq.
//
// see the comments for Min() for more explanation.
//
func (px *Paxos) Done(seq int) {
	// Your code here.
	if px.dead {
		return
	}

	px.mu.Lock()
	if seq > px.doneseq[px.me] {
		px.doneseq[px.me] = seq
	}
	px.mu.Unlock()
}

//
// the application wants to know the
// highest instance sequence known to
// this peer.
//
func (px *Paxos) Max() int {
	// Your code here.
	px.mu.Lock()
	maxseq := px.highestseq
	px.mu.Unlock()

	return maxseq
}

//
// Min() should return one more than the minimum among z_i,
// where z_i is the highest number ever passed
// to Dotest_primitives.NotError() on peer i. A peers z_i is -1 if it has
// never called Dotest_primitives.NotError().
//
// Paxos is required to have forgotten all information
// about any instances it knows that are < Min().
// The point is to free up memory in long-running
// Paxos-based servers.
//
// Paxos peers need to exchange their highest Dotest_primitives.NotError()
// arguments in order to implement Min(). These
// exchanges can be piggybacked on ordinary Paxos
// agreement protocol messages, so it is OK if one
// peers Min does not reflect another Peers Dotest_primitives.NotError()
// until after the next instance is agreed to.
//
// The fact that Min() is defined as a minimum over
// *all* Paxos peers means that Min() cannot increase until
// all peers have been heard from. So if a peer is dead
// or unreachable, other peers Min()s will not increase
// even if all reachable peers call Done. The reason for
// this is that when the unreachable peer comes back to
// life, it will need to catch up on instances that it
// missed -- the other peers therefor cannot forget these
// instances.
//
func (px *Paxos) Min() int {
	// You code here.
	px.mu.Lock()
	res := px.lowestseq
	px.mu.Unlock()

	return res
}

//
// the application wants to know whether this
// peer thinks an instance has been decided,
// and if so what the agreed value is. Status()
// should just inspect the local peer state;
// it should not contact other Paxos peers.
//
func (px *Paxos) Status(seq int) (bool, interface{}) {
	// Your code here.
	px.updatesSeq(seq)
	r1 := false
	var r2 interface{} = nil

	// fmt.Println("hahahahah!!!")
	px.mu.Lock()
	// fmt.Println("ajshdfkas!!")
	if px.decided[seq] {
		r1 = true
		r2 = px.decision[seq]
	}
	px.mu.Unlock()

	return r1, r2
}

//
// tell the peer to shut itself down.
// for testing.
// please do not change this function.
//
func (px *Paxos) Kill() {
	px.dead = true
	if px.l != nil {
		px.l.Close()
	}
}

func (px *Paxos) Prepare(Args ArgsPrepare, reply *ReplyTypePrepare) error {

	defer func() {
		(*reply).Modified = true
	}()

	px.updatesSeq(Args.Seq)
	px.mu.Lock()
	(*reply).Round = px.acceptrounds[Args.Seq]
	(*reply).AcceptedProposal = px.accepts[Args.Seq]
	if Args.Round > px.preparerounds[Args.Seq] {
		px.preparerounds[Args.Seq] = Args.Round
	}
	px.mu.Unlock()

	return nil
}

func (px *Paxos) AcceptRequire(Args ArgsAcceptRequire, reply *ReplyTypeAcceptRequire) error {

	defer func() {
		(*reply).Modified = true
	}()

	px.updatesSeq(Args.Seq)
	px.mu.Lock()
	if px.preparerounds[Args.Seq] == Args.Round {
		px.acceptrounds[Args.Seq] = Args.Round
		px.accepts[Args.Seq] = Args.Proposal
		(*reply).Accepted = true
	} else {
		(*reply).Accepted = false
	}
	px.mu.Unlock()

	return nil
}

func (px *Paxos) BroadCastDecide(seq int) {

	if px.dead {
		return
	}

	var hash []bool
	for i := 0; i < px.num_peers; i++ {
		hash = append(hash, false)
	}

	hash[px.me] = true
	px.mu.Lock()
	px.num_decided[seq]++
	px.mu.Unlock()

	for true {

		if px.num_decided[seq] == px.num_peers || px.dead {
			//  fmt.Printf("%d quit broadcast\n",px.me)
			return
		}

		for i := 0; i < px.num_peers; i++ {
			if !hash[i] {
				var reply *ReplyTypeDecide
				var Args ArgsDecide
				Args.Sender = px.me
				Args.Seq = seq
				Args.Decision = px.decision[seq]

				reply = &ReplyTypeDecide{}
				reply.Modified = false
				// fmt.Printf("%s attempts to tell %s\n",px.peers[px.me],px.peers[i])
				// px.calllock.Lock()
				acc := px.call(px.peers[i], "Paxos.Decide", Args, reply)
				//  px.calllock.Unlock()
				//  fmt.Printf("%s ends to tell %s\n",px.peers[px.me],px.peers[i])

				if acc {
					if reply.Modified {
						hash[i] = true
						px.num_decided[seq]++
					}
				}
			}
		}

		time.Sleep(BroadCastDecideGap)
	}
}

func (px *Paxos) Decide(Args ArgsDecide, reply *ReplyTypeDecide) error {
	defer func() {
		(*reply).Modified = true
	}()
	//log.Println("Decided", Args.Seq, px.decision[Args.Seq], "ME", px.me, "is decide?", px.decided[Args.Seq])
	px.updatesSeq(Args.Seq)
	px.mu.Lock()
	(*reply).Decided = true
	if px.decided[Args.Seq] == false {
		px.decision[Args.Seq] = Args.Decision
		px.decided[Args.Seq] = true
		go px.BroadCastDecide(Args.Seq)
	}
	px.mu.Unlock()

	return nil
}

func (px *Paxos) ProposeSeq(seq int, myproposal interface{}) {

	defer func() {
		px.proposetoken[seq] = false
	}()

	if px.dead {
		return
	}

	var realproposal interface{}
	var hash []bool

	max_round := -1
	num_prepares := 0
	for i := 0; i < px.num_peers; i++ {
		hash = append(hash, false)
	}

	px.proposeround[seq] = px.proposeround[seq] + 1
	for ; px.proposeround[seq]%px.num_peers != px.me; px.proposeround[seq]++ {
	}

	for true {

		if px.decided[seq] || px.dead {
			return
		} else if num_prepares >= px.num_majority {
			break
		}

		for i := 0; i < px.num_peers; i++ {
			if !hash[i] {

				var reply *ReplyTypePrepare
				var Args ArgsPrepare
				Args.Sender = px.me
				Args.Seq = seq
				px.mu.Lock()
				Args.Round = px.proposeround[seq]
				px.mu.Unlock()
				reply = &ReplyTypePrepare{}
				reply.Modified = false

				if i != px.me {
					//  fmt.Printf("%s attempts to Paxos.Prepare %s\n",px.peers[px.me],px.peers[i])
					//  px.calllock.Lock()
					acc := px.call(px.peers[i], "Paxos.Prepare", Args, reply)
					//  px.calllock.Unlock()
					//  fmt.Printf("%s ends to Paxos.Prepare %s\n",px.peers[px.me],px.peers[i])
					if acc {
						if reply.Modified {
							hash[i] = true
							num_prepares++
						} else {
							continue
						}
					} else {
						continue
					}
				} else {
					_ = px.Prepare(Args, reply)
					hash[i] = true
					num_prepares++
				}

				if reply.Round > px.proposeround[seq] {
					px.proposeround[seq] = reply.Round
					return
				} else if reply.Round > max_round {
					max_round = reply.Round
					realproposal = reply.AcceptedProposal
				}
			}
		}

		time.Sleep(ProposeGap)
	}

	if max_round == -1 {
		realproposal = myproposal
	}
	num_replyes := 0
	num_acceptes := 0
	for i := 0; i < px.num_peers; i++ {
		hash[i] = false
	}

	for true {

		if px.decided[seq] || px.dead {
			return
		} else if num_replyes >= px.num_majority {
			break
		}

		for i := 0; i < px.num_peers; i++ {
			if !hash[i] {

				var reply *ReplyTypeAcceptRequire
				var Args ArgsAcceptRequire
				Args.Sender = px.me
				Args.Seq = seq
				Args.Proposal = realproposal
				px.mu.Lock()
				Args.Round = px.proposeround[seq]
				px.mu.Unlock()
				reply = &ReplyTypeAcceptRequire{}
				reply.Modified = false

				if i != px.me {
					//  fmt.Printf("%s attempts to Paxos.AcceptRequire %s\n",px.peers[px.me],px.peers[i])
					//  px.calllock.Lock()
					acc := px.call(px.peers[i], "Paxos.AcceptRequire", Args, reply)
					//  px.calllock.Unlock()
					//  fmt.Printf("%s ends to Paxos.AcceptRequire %s\n",px.peers[px.me],px.peers[i])
					if acc {
						if reply.Modified {
							hash[i] = true
							num_replyes++
						} else {
							continue
						}
					} else {
						continue
					}
				} else {
					_ = px.AcceptRequire(Args, reply)
					hash[i] = true
					num_replyes++
				}

				if reply.Accepted {
					num_acceptes++
				}
			}
		}

		time.Sleep(ProposeGap)
	}

	if num_acceptes >= px.num_majority {
		var reply *ReplyTypeDecide
		var Args ArgsDecide
		Args.Sender = px.me
		Args.Seq = seq
		Args.Decision = realproposal
		reply = &ReplyTypeDecide{}
		reply.Modified = false

		go px.Decide(Args, reply)
	}
}

func (px *Paxos) Propose() {

	if px.dead {
		return
	}

	for true {

		px.mu.Lock()
		if px.leader != px.me || px.dead {
			px.token = false
			px.mu.Unlock()
			return
		}
		curhighestseq := px.highestseq
		px.mu.Unlock()

		for i := 0; i <= curhighestseq; i++ {

			if px.isoriginal[i] && !px.decided[i] {
				px.mu.Lock()
				if px.proposetoken[i] == false {
					px.proposetoken[i] = true
					//      if i>= len(px.originalproposal) {
					//        fmt.Printf("%d %d %d %d panic!!!!!!!!!!!!!!\n",px.me,i,len(px.originalproposal),px.highestseq)
					//      }
					go px.ProposeSeq(i, px.originalproposal[i])
				}
				px.mu.Unlock()
			}

		}

		time.Sleep(ProposeGap)
	}
}

func (px *Paxos) AreYouAlive(Args ArgsIsAlive, reply *ReplyTypeIsAlive) error {
	defer func() {
		(*reply).Modified = true
	}()

	if px.dead == false {
		(*reply).IsAlive = true
	} else {
		(*reply).IsAlive = false
	}
	return nil
}

func (px *Paxos) LeaderElection() {

	var alive []int = make([]int, px.num_peers)

	for px.dead == false {

		for i := 0; i < px.num_peers; i++ {
			alive[i] = 0
		}

		alive[px.me] = 1

		for i := 0; i <= px.me; i++ {

			if i != px.me {
				for j := 0; j < 5; j++ {
					var reply *ReplyTypeIsAlive
					var Args ArgsIsAlive
					Args.Sender = px.me
					reply = &ReplyTypeIsAlive{}
					reply.Modified = false
					// fmt.Println(px.peers[i])
					//  fmt.Printf("%s attempts to Paxos.AreYouAlive %s\n",px.peers[px.me],px.peers[i])
					//  px.calllock.Lock()
					acc := px.call(px.peers[i], "Paxos.AreYouAlive", Args, reply)
					//  px.calllock.Unlock()
					// fmt.Printf("%s ends to Paxos.AreYouAlive %s\n",px.peers[px.me],px.peers[i])

					if acc {
						if reply.IsAlive && reply.Modified {
							alive[i] = 1
							break
						}
					}
				}
			}

			if alive[i] == 1 {
				px.mu.Lock()
				px.leader = i
				if px.token == false && i == px.me {
					px.token = true
					go px.Propose()
				}
				//     fmt.Printf("%d's leader is %d\n",px.me,i);
				px.mu.Unlock()
				break
			}
		}
		sleepTime := rand.Int63n(int64(LeaderElectionGap) * 10)
		time.Sleep(LeaderElectionGap + time.Duration(sleepTime))
	}
}

func (px *Paxos) UpdateDone(Args ArgsUpdateDone, reply *ReplyTypeUpdateDone) error {

	defer func() {
		(*reply).Modified = true
	}()

	px.mu.Lock()
	if Args.Done > px.doneseq[Args.Sender] {
		px.doneseq[Args.Sender] = Args.Done
	}
	(*reply).Rec = true
	px.mu.Unlock()
	return nil
}

func (px *Paxos) BroadCastDone() {

	for true {

		//   fmt.Printf("%d is in broadcast done\n",px.me)
		if px.dead {
			return
		}

		for i := 0; i < px.num_peers; i++ {
			var Args ArgsUpdateDone
			var reply *ReplyTypeUpdateDone

			Args.Sender = px.me
			px.mu.Lock()
			Args.Done = px.doneseq[px.me]
			px.mu.Unlock()
			reply = &ReplyTypeUpdateDone{}
			reply.Modified = false

			if i != px.me {
				// fmt.Printf("%s attempts to Paxos.UpdateDone %s\n",px.peers[px.me],px.peers[i])
				//  px.calllock.Lock()
				_ = px.call(px.peers[i], "Paxos.UpdateDone", Args, reply)
				//  px.calllock.Unlock()
				// fmt.Printf("%s ends to Paxos.UpdateDone %s\n",px.peers[px.me],px.peers[i])
			}
		}

		myflag := false
		for ; px.lowestseq <= px.highestseq && !px.dead; px.lowestseq++ {
			for i := 0; i < px.num_peers; i++ {
				px.mu.Lock()
				if px.doneseq[i] < px.lowestseq {
					myflag = true
					px.mu.Unlock()
					break
				}
				px.mu.Unlock()
			}

			if myflag {
				break
			}
		}

		if px.dead {
			return
		}

		for i := 0; i < px.lowestseq; i++ {
			if px.num_decided[i] == px.num_peers {
				px.mu.Lock()
				px.decision[i] = nil
				px.accepts[i] = nil
				px.originalproposal[i] = nil
				px.mu.Unlock()
			}
		}

		time.Sleep(BroadCastDoneGap)
	}
}

//
// the application wants to create a paxos peer.
// the ports of all the paxos peers (including this one)
// are in peers[]. this servers port is peers[me].
//
func Make(peers []string, me int) *Paxos {
	px := &Paxos{}
	px.peers = peers
	px.me = me

	// Your initialization code here.
	px.num_peers = len(peers)
	px.num_majority = px.num_peers/2 + 1
	px.leader = 0
	px.highestseq = -1
	px.lowestseq = 0
	px.token = false
	px.unreliable = paxosUnreliable
	px.persistentRPCs = make(map[string]*util.PersistentRPC)
	for i := 0; i < px.num_peers; i++ {
		px.doneseq = append(px.doneseq, -1)
	}
	// fmt.Printf("num_peers %d\n",px.num_peers)

	var rpcs *rpc.Server
	rpcs = rpc.NewServer()
	rpcs.RegisterName("Paxos", px)

	// prepare to receive connections from clients.
	// change "unix" to "tcp" to use over a network.
	//os.Remove(peers[me]) // only needed for "unix"
	strs := strings.Split(peers[me], ":")
	if len(strs) != 2 {
		log.Fatal("Config Error!!!")
	}
	l, e := net.Listen("tcp", ":"+strs[1])
	if e != nil {
		log.Fatal("listen error: ", e)
	}
	px.l = l

	// please do not change any of the following code,
	// or do anything to subvert it.

	// create a thread to accept RPC connections
	go func() {
		for px.dead == false {
			conn, err := px.l.Accept()
			if err == nil && px.dead == false {
				if px.unreliable && (rand.Int63()%1000) < 100 {
					// discard the request.
					conn.Close()
				} else if px.unreliable && (rand.Int63()%1000) < 200 {
					// process the request but force discard of reply.
					c1 := conn.(*net.TCPConn)
					f, _ := c1.File()
					err := syscall.Shutdown(int(f.Fd()), syscall.SHUT_WR)
					if err != nil {
						fmt.Printf("shutdown: %v\n", err)
					}
					px.rpcCount++
					go rpcs.ServeConn(conn)
				} else {
					px.rpcCount++
					go rpcs.ServeConn(conn)
				}
			} else if err == nil {
				conn.Close()
			}
			if err != nil && px.dead == false {
				fmt.Printf("Paxos(%v) accept: %v\n", me, err.Error())
			}
		}
	}()

	go px.LeaderElection()
	go px.BroadCastDone()

	return px
}
