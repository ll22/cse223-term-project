package paxos_test

import (
	"log"
	"paxos"
	"randomaddr"
	"testing"
	"time"
)

func genRandAddr(n int) []string {
	exist := make(map[string]bool)
	res := []string{}
	for i := 0; i < n; i++ {
		addr := randomaddr.RandomLocalAddr()
		for exist[addr] == true {
			addr = randomaddr.RandomLocalAddr()
		}
		exist[addr] = true
		res = append(res, addr)
	}
	return res
}

func runTask(index int, px *paxos.Paxos, task string) {
	var decision interface{}
	var decided bool
	Seq := 0
	for true {
		px.Start(Seq, task)
		to := 10 * time.Millisecond
		for true {
			decided, decision = px.Status(Seq)
			if decided {
				break
			}
			time.Sleep(to)
			if to < 10*time.Second {
				to = to * 2
			}
		}
		v1 := decision.(string)
		if v1 == task {
			return
		}
		Seq++
	}
	if index == 3 {
		px.Done(Seq)
	}
}

func TestWriteAndRead(t *testing.T) {
	n := 5
	addr := genRandAddr(n)
	Allpaxos := []*paxos.Paxos{}
	for i := 0; i < n; i++ {
		px := paxos.Make(addr, i)
		Allpaxos = append(Allpaxos, px)
	}
	go runTask(0, Allpaxos[0], "FirstTask")
	go runTask(1, Allpaxos[1], "SecondTask")
	go runTask(2, Allpaxos[2], "ThirdTask")
	go runTask(3, Allpaxos[3], "FourthTask")
	go runTask(4, Allpaxos[4], "FiveTask")
	time.Sleep(3 * time.Second)
	for i := 0; i < n; i++ {
		log.Printf("%d:", i)
		for j := 0; j < n; j++ {
			_, decision := Allpaxos[i].Status(j)
			log.Printf("%d:%s", j, decision.(string))
		}
		log.Printf("Min:%d;Max:%d", Allpaxos[i].Min(), Allpaxos[i].Max())
	}

}
