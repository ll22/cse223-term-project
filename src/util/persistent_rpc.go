package util

import (
	"net/rpc"
	"sync"
)

// PersistentRPC a basic RPC client for persistent RPC call
type PersistentRPC struct {
	addr string
	m    sync.Mutex
	conn *rpc.Client
}

// NewPersistentRPC create a persistent RPC client
func NewPersistentRPC(addr string) *PersistentRPC {
	return &PersistentRPC{
		addr,
		sync.Mutex{},
		nil,
	}
}

func (c *PersistentRPC) requireConn() error {
	c.m.Lock()
	defer c.m.Unlock()
	if c.conn == nil {
		conn, err := rpc.Dial("tcp", c.addr)
		c.conn = conn
		return err
	}
	return nil
}

// ResetAddr reset the address and close a connection
func (c *PersistentRPC) ResetAddr(addr string) {
	if c.addr == addr {
		return
	}
	c.m.Lock()
	defer c.m.Unlock()
	c.addr = addr
	c.conn = nil
}

// TryCall persistent RPC try its best to call a method. If it failed, it returns error to
// the caller. It is caller's responsibility to have timeout and retry mechanism
func (c *PersistentRPC) TryCall(method string, args interface{}, reply interface{}) error {
	if args == nil || reply == nil {
		panic("trycall nil")
	}
	if err := c.requireConn(); err != nil {
		return err
	}

	if err := c.conn.Call(method, args, reply); err != nil {
		c.conn = nil
		return err
	}

	return nil
}

// FeedReady feed the channel if it's not nil
func FeedReady(ready chan<- bool, v bool) {
	if ready != nil {
		ready <- v
	}
}
