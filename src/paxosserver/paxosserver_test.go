package paxosserver_test

import (
	"clientlib"
	"dns"
	"events"
	"log"
	"paxosserver"
	"randomaddr"
	"servernode"
	"store"
	"sync"
	"testing"
	"testp"
	"time"
	//"io/ioutil"
)

const (
	ClientNum = 1000
)

var CilentFinishNum = 0
var m = sync.Mutex{}

/*
func runTask(index int, px *paxos.Paxos, task string) {
	var decision interface{}
	var decided bool
	Seq := 0
	for true {
		px.Start(Seq, task)
		to := 10 * time.Millisecond
		for true {
			decided, decision = px.Status(Seq)
			if decided {
				break
			}
			time.Sleep(to)
			if to < 10*time.Second {
				to = to * 2
			}
		}
		v1 := decision.(string)
		if v1 == task {
			return
		}
		Seq++
	}
	if index == 3 {
		px.Dotest_primitives.NotError(Seq)
	}
}*/

func TestWriteAndRead(t *testing.T) {
	//log.SetFlags(-1)
	//log.SetOutput(ioutil.Discard)
	log.SetFlags(log.Lshortfile | log.Ltime)
	var rwFiles string = "/ls/foo/wombat/pouch"
	genRandAddr := func(n int) []string {
		exist := make(map[string]bool)
		res := []string{}
		for i := 0; i < n; i++ {
			addr := randomaddr.RandomLocalAddr()
			for exist[addr] == true {
				addr = randomaddr.RandomLocalAddr()
			}
			exist[addr] = true
			res = append(res, addr)
		}
		return res
	}
	var v string
	var err error
	ready := make(chan bool)
	go func() {
		if e := dns.ServeDNSRPC(dns.NewDNS(), &ready); e != nil {
			t.Fatal(e)
		}
	}()
	r := <-ready
	if !r {
		t.Fatal("not ready")
	}
	mydns := dns.NewDnsRPCclient(dns.DNSAddr)
	//finish DNS
	n := 5
	addr := genRandAddr(n * 250)
	allpaxos := []*paxosserver.PaxosServer{}
	for i := 0; i < ClientNum+32; i++ {
		log.Printf("AllServerAddr %d: %s", i, addr[i])
	}

	for i := 0; i < n; i++ {
		st := store.NewStorageId(i)
		ready := make(chan bool)
		go store.ServeStoreRPC(addr[10+i], st, ready)
		<-ready
		close(ready)
		//servernode.NewFS(addr[10+i])
	}
	for i := 0; i < n; i++ {
		pconfig := &paxosserver.PaxosServerConfig{
			Peers:              addr[0:5],
			ServerID:           i,
			PaxosServerRpcaddr: addr[i],
			StoreAddr:          addr[10+i],
			ServerAddr:         addr[15+i],
		}
		px := paxosserver.NewPaxosServer(pconfig)
		allpaxos = append(allpaxos, px)
	}

	time.Sleep(10 * time.Second)
	err = mydns.GetIPwithName(servernode.Primary, &v)
	log.Println("DNS: Master IP is ", v, " error:", err)
	for i := 0; i < n; i++ {
		log.Printf("Master %d: %d", i, allpaxos[i].GetmasterID())

		/*
			for j := 0; j < n; j++ {
				_, decision := allpaxos[i].Status(j)
				log.Printf("%d:%s", j, decision.(string))
			}
			log.Printf("Min:%d;Max:%d", allpaxos[i].Min(), allpaxos[i].Max())
			}*/
	}

	log.Println("Job Start Time:", time.Now())
	startOneCrashClient(addr[30], addr[30:ClientNum+31], true, rwFiles, 20)
	for i := 31; i < ClientNum+30; i++ {
		go startOneCrashClient(addr[i], addr[30:ClientNum+31], false, rwFiles, 20)
	}
	for CilentFinishNum < ClientNum {
		log.Println("Job Finish Percent:", CilentFinishNum, "/", ClientNum)
		time.Sleep(500 * time.Millisecond)
	}
	log.Println("Job Finish Time:", time.Now())

	//Kill one master
	allpaxos[0].Shutdown()
	time.Sleep(15 * time.Second)
	newV := ""
	err = mydns.GetIPwithName(servernode.Primary, &newV)
	log.Println("is same address?", v == newV)
	//testp.As(newV != v)
	log.Println("DNS: Master IP is ", v, " error:", err)
	for i := 0; i < n; i++ {
		log.Printf("Master %d: %d", i, allpaxos[i].GetmasterID())
	}
	log.Println("Start a new client to verify file content")
	e, client := clientlib.NewChubbyClient(addr[ClientNum+30])
	testp.NotError(e)
	var fh clientlib.FileHandleT
	err, fh = client.OpenExisting(rwFiles, servernode.ReadHandle)
	//err, fh = client.Open(
	//	rwFiles,
	//	servernode.ReadHandle,
	//	events.EventFileContentModified,
	//	false,
	//	"",
	//	servernode.FileACLInfo{
	//		Writer:     []string{addr[30]},
	//		Reader:     []string{addr[30]},
	//		ACLChanger: []string{addr[30]},
	//	},
	//	false,
	//)
	testp.NotError(err)
	log.Printf("Second client tries to get Read lock for file %s", rwFiles)
	err = fh.Acquire("R", 1)
	testp.NotError(err)
	log.Println("Second client tries to get Read lock succeed!")

	log.Printf("Second client tries to read file %s", rwFiles)
	e3, content, _ := fh.GetContentsAndStat()
	testp.NotError(e3)
	log.Println("contentresult", content.Content)
	testp.As(content.Content == "dummy_write ")

	fh.Release()
	log.Println("Second client tries to release succeed!")
	client.CloseWith(fh)
	log.Println("Second client tries to closewith succeed!")
	client.ShutDown()
	/*
		for j := 0; j < n; j++ {
			_, decision := allpaxos[i].Status(j)
			log.Printf("%d:%s", j, decision.(string))
		}
		log.Printf("Min:%d;Max:%d", allpaxos[i].Min(), allpaxos[i].Max())
		}*/

}

func startOneCrashClient(myaddr string, clientAddr []string, CreatMode bool, rwFiles string, rwCounter int) {
	var err error
	var fh clientlib.FileHandleT

	//var defaultLogFilePath string = "./gfs/test_log"
	initialContent := "blah blah blah "
	e, client := clientlib.NewChubbyClient(myaddr)
	client.OnDisconnect(func(e error) error {
		return client.Reconnect()
	})
	testp.NotError(e)
	// create and open a file
	err, fh = client.Open(
		rwFiles,
		servernode.WriteHandle,
		events.EventFileContentModified,
		CreatMode,
		initialContent,
		servernode.FileACLInfo{
			Writer:     clientAddr,
			Reader:     clientAddr,
			ACLChanger: clientAddr,
		},
		false,
	)
	testp.NotError(err)

	err = fh.Acquire("W", 400)
	log.Println("client finish acquire")
	testp.NotError(err)

	// apply a contract on that file

	setContents := "dummy_write "

	// reset the initial contents of the file
	err = fh.SetContents(setContents)
	testp.NotError(err)
	e3, content, _ := fh.GetContentsAndStat()
	testp.NotError(e3)
	log.Println("contentresult", content.Content)
	testp.As(content.Content == "dummy_write ")

	fh.Release()
	if e := client.CloseWith(fh); e != nil {
		log.Println(e.Error())
	}
	client.ShutDown()
	log.Println("First client is trying to shut itself down!")
	m.Lock()
	CilentFinishNum++
	m.Unlock()
}
