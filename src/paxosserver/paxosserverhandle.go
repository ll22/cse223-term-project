package paxosserver

import (
	"log"
	"servernode"
)

/*
func (pxs *PaxosServer) serveRpc(ready chan bool, ServerRpcaddr string) error {
	server := rpc.NewServer()
	server.RegisterName("PaxosServer", pxs)
	ln, err := net.Listen("tcp", ServerRpcaddr)
	if err != nil {
		ready <- false
		return err
	}
	ready <- true
	conns := []net.Conn{}

	for {
		conn, err := ln.Accept()
		log.Println("A new One")
		if err != nil {
			pxs.Shutdown()
			for _, c := range conns {
				c.close()
			}
			return err
		}
		conns = append(conns, conn)
		go server.ServeConn(conn)
	}
}
*/

//GetHandle : RPC call
func (pxs *PaxosServer) GetHandle(req servernode.GetHandleRequestInfo, reply *servernode.GetHandleReplyInfo) error {
	//log.Println("Call_GetHandle")
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "GetHandle"
	e, tmp := pxs.newOP(op)
	log.Println("tmp", tmp)
	*reply = *tmp.(*servernode.GetHandleReplyInfo)
	return e
}

//ReturnHandle : RPC call
func (pxs *PaxosServer) ReturnHandle(req servernode.ReturnHandleRequestInfo, reply *servernode.ReturnHandleReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "ReturnHandle"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.ReturnHandleReplyInfo)
	return e
}

//ListAllFiles : RPC call
func (pxs *PaxosServer) ListAllFiles(directory string, reply *servernode.ListReply) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	pxs.oldOP()
	return pxs.myserver.DoListAllFiles(directory, reply)
}

//GetContentsAndStat : RPC call
func (pxs *PaxosServer) GetContentsAndStat(req servernode.GetContentsAndStatRequestInfo, reply *servernode.GetContentsAndStatReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	pxs.oldOP()
	return pxs.myserver.DoGetContentsAndStat(req, reply)
}

//GetStat : RPC call
func (pxs *PaxosServer) GetStat(req servernode.GetStatRequestInfo, reply *servernode.GetStatReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	pxs.oldOP()
	return pxs.myserver.DoGetStat(req, reply)
}

//SetContents : RPC call
func (pxs *PaxosServer) SetContents(req servernode.SetContentsRequestInfo, reply *servernode.SetContentsReplyInfo) error {

	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "SetContents"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.SetContentsReplyInfo)
	return e
}

//SetACL : RPC call
func (pxs *PaxosServer) SetACL(req servernode.SetACLRequestInfo, reply *servernode.SetACLReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "SetACL"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.SetACLReplyInfo)
	return e
}

//Delete : RPC call
func (pxs *PaxosServer) Delete(req servernode.DeleteRequestInfo, reply *servernode.DeleteReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "Delete"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.DeleteReplyInfo)
	return e
}

//Acquire : RPC call
func (pxs *PaxosServer) Acquire(req servernode.AcquireRequestInfo, reply *servernode.AcquireReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	//transfer from AcquireRequestInfo to TryAcquireRequestInfo
	reqq := servernode.TryAcquireRequestInfo{
		FilePath: req.FilePath,
		Acquirer: req.Acquirer,
		Mode:     req.Mode,
	}
	op := pxs.transferOP(reqq, reply)
	op.OPtype = "Acquire"
	e, tmp := pxs.newOP(op)
	log.Println("acquire", tmp)
	if tmp != nil {
		*reply = *tmp.(*servernode.AcquireReplyInfo)
	}
	return e
}

//TryAcquire : RPC call
func (pxs *PaxosServer) TryAcquire(req servernode.TryAcquireRequestInfo, reply *servernode.TryAcquireReplyInfo) error {

	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "TryAcquire"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.TryAcquireReplyInfo)
	return e
}

//Release : RPC call
func (pxs *PaxosServer) Release(req servernode.ReleaseRequestInfo, reply *servernode.ReleaseReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "Release"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.ReleaseReplyInfo)
	return e
}

//GetSequencer : RPC call
func (pxs *PaxosServer) GetSequencer(req servernode.GetSequencerRequestInfo, reply *servernode.GetSequencerReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	pxs.oldOP()
	return pxs.myserver.DoGetSequencer(req, reply)
}

//CheckSequencer : RPC call
func (pxs *PaxosServer) CheckSequencer(req servernode.CheckSequencerRequestInfo, reply *servernode.CheckSequencerReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	pxs.oldOP()
	return pxs.myserver.DoCheckSequencer(req, reply)
}

//KeepLive : RPC call
func (pxs *PaxosServer) KeepLive(req servernode.KeepLiveRequestInfo, reply *servernode.KeepLiveReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}
	op := pxs.transferOP(req, reply)
	op.OPtype = "KeepLive"
	pxs.newOP(op)
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.KeepLiveReplyInfo)
	return e
}

func (pxs *PaxosServer) SendContract(req servernode.SendContractRequestInfo, reply *servernode.SendContractReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}

	op := pxs.transferOP(req, reply)
	op.OPtype = "SendContract"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.SendContractReplyInfo)
	return e
}

func (pxs *PaxosServer) DeleteContract(req servernode.DeleteContractRequestInfo, reply *servernode.DeleteContractReplyInfo) error {
	if err := pxs.CheckLegal(); err != nil {
		return err
	}

	op := pxs.transferOP(req, reply)
	op.OPtype = "DeleteContract"
	e, tmp := pxs.newOP(op)
	*reply = *tmp.(*servernode.DeleteContractReplyInfo)
	return e
}

/*
// define the store RPC type
type paxosserverRPCclient struct {
	addr string
	m    *sync.Mutex
	conn *rpc.Client
}

func NewpaxosserverRPCclient(addr string) *paxosserverRPCclient {
	return &paxosserverRPCclient{
		addr,
		&sync.Mutex{},
		nil,
	}
}

// functions of type paxospaxosserverRPCclient
func (c *paxosserverRPCclient) requireConn() error {
	c.m.Lock()
	defer c.m.Unlock()
	if c.conn == nil {
		conn, err := rpc.Dial("tcp", c.addr)
		c.conn = conn
		return err
	}
	return nil
}

func (c *paxosserverRPCclient) call(method string, args interface{}, reply interface{}) error {
	if err := c.requireConn(); err != nil {
		return err
	}
	//log.Println("LLALALALALAL")
	if err := c.conn.Call(method, args, reply); err != nil {
		c.conn = nil
		return err
	}
	return nil
}

// GetHandle get a file handle for a file
func (c *paxosserverRPCclient) GetHandle(req servernode.GetHandleRequestInfo, res *servernode.GetHandleReplyInfo) error {
	return c.call("PaxosServer.GetHandle", req, res)
}

// ListAllFiles list all the files in a server
func (c *paxosserverRPCclient) ListAllFiles(dummyReq string, reply *servernode.ListReply) error {
	return c.call("PaxosServer.ListAllFiles", dummyReq, reply)
}

func (c *paxosserverRPCclient) Delete(req servernode.DeleteRequestInfo, reply *servernode.DeleteReplyInfo) error {
	return c.call("PaxosServer.Delete", req, reply)
}

func (c *paxosserverRPCclient) Acquire(req servernode.AcquireRequestInfo, reply *servernode.AcquireReplyInfo) error {
	return c.call("PaxosServer.Acquire", req, reply)
}

func (c *paxosserverRPCclient) TryAcquire(req servernode.TryAcquireRequestInfo, reply *servernode.TryAcquireReplyInfo) error {
	return c.call("PaxosServer.TryAcquire", req, reply)
}

func (c *paxosserverRPCclient) Release(req servernode.ReleaseRequestInfo, reply *servernode.ReleaseReplyInfo) error {
	return c.call("PaxosServer.Release", req, reply)
}

func (c *paxosserverRPCclient) GetContentsAndStat(req servernode.GetContentsAndStatRequestInfo, reply *servernode.GetContentsAndStatReplyInfo) error {
	return c.call("PaxosServer.GetContentsAndStat", req, reply)
}

func (c *paxosserverRPCclient) GetStat(req servernode.GetStatRequestInfo, reply *servernode.GetStatReplyInfo) error {
	return c.call("PaxosServer.GetStat", req, reply)
}

func (c *paxosserverRPCclient) SetContents(req servernode.SetContentsRequestInfo, reply *servernode.SetContentsReplyInfo) error {
	return c.call("PaxosServer.SetContents", req, reply)
}

func (c *paxosserverRPCclient) SetACL(req servernode.SetACLRequestInfo, reply *servernode.SetACLReplyInfo) error {
	return c.call("PaxosServer.SetACL", req, reply)
}

func (c *paxosserverRPCclient) CheckSequencer(req servernode.CheckSequencerRequestInfo, reply *servernode.CheckSequencerReplyInfo) error {
	return c.call("PaxosServer.CheckSequencer", req, reply)
}

func (c *paxosserverRPCclient) GetSequencer(req servernode.GetSequencerRequestInfo, reply *servernode.GetSequencerReplyInfo) error {
	return c.call("PaxosServer.GetSequencer", req, reply)
}

func (c *paxosserverRPCclient) ReturnHandle(req servernode.ReturnHandleRequestInfo, reply *servernode.ReturnHandleReplyInfo) error {
	return c.call("PaxosServer.ReturnHandle", req, reply)
}

func (c *paxosserverRPCclient) KeepLive(req servernode.KeepLiveRequestInfo, reply *servernode.KeepLiveReplyInfo) error {
	return c.call("PaxosServer.KeepLive", req, reply)
}

func (c *paxosserverRPCclient) SendContract(req servernode.SendContractRequestInfo, reply *servernode.SendContractReplyInfo) error {
	return c.call("PaxosServer.SendContract", req, reply)
}
*/
