package paxosserver

import (
	"dns"
	"encoding/gob"
	"encoding/json"
	"errors"
	"store"

	"fmt"
	"log"
	"sync"
	"time"

	"paxos"
	"servernode"
)

const (
	// MasterLeaseTime  is 30 * second
	MasterLeaseTime = 30
	// MasterRefreshTime  is 12 * second
	MasterRefreshTime = 10
	//ToMinTime is 1*Millisecond
	ToMinTime = 1
	//ToMaxTime is 20*Millisecond
	ToMaxTime = 20
)

// PaxosServer is the main Chubby Server
// All rpc calls actually call functions in this file
// And then PaxosServer call other sub functions in server
type PaxosServer struct {
	ServerID     int
	Mu           sync.Mutex
	Dead         bool
	Seq          int
	NextOp       int
	OPQ          *OPQueue
	Px           *paxos.Paxos
	myserver     *servernode.ServerNode
	dnsRPCclient dns.DNST
	config       PaxosServerConfig
}
type PaxosServerConfig struct {
	Peers              []string
	ServerID           int
	PaxosServerRpcaddr string
	StoreAddr          string
	ServerAddr         string
}

//ServerID from 0 to n-1
func NewPaxosServer(config *PaxosServerConfig) *PaxosServer {
	gob.Register(Operation{})
	pxs := &PaxosServer{}
	pxs.ServerID = config.ServerID
	pxs.Dead = false
	pxs.NextOp = 0
	pxs.OPQ = NewOPQueue()
	pxs.Px = paxos.Make(config.Peers, config.ServerID)
	pxs.myserver = servernode.NewServerNode(&servernode.ServerNodeConfig{
		StoreAddr:  config.StoreAddr,
		ServerAddr: config.ServerAddr,
	})
	ready := make(chan bool)
	go func() {
		if e := servernode.ServeServerRPC(config.ServerAddr, pxs.myserver, ready); e != nil {
			log.Printf("Starting Chubby server failed")
			panic(e)
		}
	}()
	<-ready
	pxs.dnsRPCclient = dns.NewDnsRPCclient(dns.DNSAddr)
	pxs.config = *config
	pxs.registerAllCall()
	go pxs.Proceeding()
	go pxs.masterMaintain()
	//rpcServeReady := make(chan bool)
	//go pxs.serveRpc(rpcServeReady, config.PaxosServerRpcaddr)
	//<-rpcServeReady

	return pxs
}

func (pxs *PaxosServer) registerAllCall() {
	pxs.myserver.On("GetHandle", func(req, res interface{}) error {
		return pxs.GetHandle(
			req.(servernode.GetHandleRequestInfo),
			res.(*servernode.GetHandleReplyInfo),
		)
	})
	pxs.myserver.On("ReturnHandle", func(req, res interface{}) error {
		return pxs.ReturnHandle(
			req.(servernode.ReturnHandleRequestInfo),
			res.(*servernode.ReturnHandleReplyInfo),
		)
	})
	pxs.myserver.On("ListAllFiles", func(req, res interface{}) error {
		return pxs.ListAllFiles(
			req.(string),
			res.(*servernode.ListReply),
		)
	})
	pxs.myserver.On("GetContentsAndStat", func(req, res interface{}) error {
		return pxs.GetContentsAndStat(
			req.(servernode.GetContentsAndStatRequestInfo),
			res.(*servernode.GetContentsAndStatReplyInfo),
		)
	})
	pxs.myserver.On("GetStat", func(req, res interface{}) error {
		return pxs.GetStat(
			req.(servernode.GetStatRequestInfo),
			res.(*servernode.GetStatReplyInfo),
		)
	})
	pxs.myserver.On("SetContents", func(req, res interface{}) error {
		return pxs.SetContents(
			req.(servernode.SetContentsRequestInfo),
			res.(*servernode.SetContentsReplyInfo),
		)
	})
	pxs.myserver.On("SetACL", func(req, res interface{}) error {
		return pxs.SetACL(
			req.(servernode.SetACLRequestInfo),
			res.(*servernode.SetACLReplyInfo),
		)
	})
	pxs.myserver.On("Delete", func(req, res interface{}) error {
		return pxs.Delete(
			req.(servernode.DeleteRequestInfo),
			res.(*servernode.DeleteReplyInfo),
		)
	})

	pxs.myserver.On("Acquire", func(req, res interface{}) error {
		return pxs.Acquire(
			req.(servernode.AcquireRequestInfo),
			res.(*servernode.AcquireReplyInfo),
		)
	})
	pxs.myserver.On("TryAcquire", func(req, res interface{}) error {
		return pxs.TryAcquire(
			req.(servernode.TryAcquireRequestInfo),
			res.(*servernode.TryAcquireReplyInfo),
		)
	})
	pxs.myserver.On("Release", func(req, res interface{}) error {
		return pxs.Release(
			req.(servernode.ReleaseRequestInfo),
			res.(*servernode.ReleaseReplyInfo),
		)
	})
	pxs.myserver.On("GetSequencer", func(req, res interface{}) error {
		return pxs.GetSequencer(
			req.(servernode.GetSequencerRequestInfo),
			res.(*servernode.GetSequencerReplyInfo),
		)
	})
	pxs.myserver.On("CheckSequencer", func(req, res interface{}) error {
		return pxs.CheckSequencer(
			req.(servernode.CheckSequencerRequestInfo),
			res.(*servernode.CheckSequencerReplyInfo),
		)
	})
	//pxs.myserver.On("KeepLive", func(req, res interface{}) error {
	//	return pxs.KeepLive(
	//		req.(servernode.KeepLiveRequestInfo),
	//		res.(*servernode.KeepLiveReplyInfo),
	//	)
	//})
	pxs.myserver.On("SendContract", func(req, res interface{}) error {
		return pxs.SendContract(
			req.(servernode.SendContractRequestInfo),
			res.(*servernode.SendContractReplyInfo),
		)
	})
}

func (pxs *PaxosServer) masterMaintain() {
	for !pxs.Dead {
		log.Println("masterMaintain id:", pxs.ServerID)
		if !pxs.hasmaster() {
			pxs.ElectMaster()
		} else if pxs.GetmasterID() == pxs.ServerID {
			//go func Renewal master lease
			//log.Println("refresh yes yes yes")
			pxs.ElectMaster()
		}
		time.Sleep(MasterRefreshTime * time.Second)
	}
}

func (pxs *PaxosServer) CheckLegal() error {
	if pxs.Dead == true {
		return errors.New("Paxos server is dead")
	}
	if !pxs.hasmaster() {
		return fmt.Errorf("No Master")
	}
	if pxs.GetmasterID() != pxs.ServerID {
		return fmt.Errorf("I am backup")
	}
	return nil
}

func (pxs *PaxosServer) transferOP(req, reply interface{}) Operation {
	var bytes []byte
	var op Operation
	bytes, _ = json.Marshal(req)
	op.Value = string(bytes)
	bytes, _ = json.Marshal(reply)
	op.Reply = string(bytes)
	op.Operror = nil
	op.Res = nil
	op.Proposer = pxs.ServerID

	return op
}

func (pxs *PaxosServer) newOP(op Operation) (error, interface{}) {
	//need change to all operation
	var tick int
	pxs.Mu.Lock()
	pxs.OPQ.Add(op)
	tick = pxs.OPQ.Len()
	log.Println("newOP:op", op.OPtype, tick)
	pxs.Mu.Unlock()
	pxs.waitforop(tick)
	e, tmp := pxs.OPQ.GetResult(tick)
	pxs.OPQ.Done(tick)
	return e, tmp
}

func (pxs *PaxosServer) oldOP() {
	//need change to all operation
	var tick int
	tick = pxs.OPQ.Len()
	pxs.waitforop(tick)
}

func (pxs *PaxosServer) ElectMaster() {
	log.Printf("ElectMaster %d", pxs.ServerID)
	op := pxs.transferOP(pxs.ServerID, nil) //time.Now())
	op.OPtype = "ElectMaster"
	//	log.Printf("!!!op.Value %s", op.Value)
	pxs.newOP(op)
	log.Printf("ElectMasterFinish %d", pxs.ServerID)
}

func (pxs *PaxosServer) Shutdown() {
	fmt.Println("Shutdown")
	pxs.Px.Kill()
	pxs.Dead = true
}

func (pxs *PaxosServer) waitforop(tick int) {
	outflag := false
	to := ToMinTime * time.Millisecond
	for true {
		pxs.Mu.Lock()
		if pxs.NextOp > tick {
			outflag = true
		}
		pxs.Mu.Unlock()
		if outflag {
			return
		}
		time.Sleep(to)
		if to < ToMaxTime*time.Millisecond {
			to = to * 2
		}
	}
}

func (pxs *PaxosServer) Done() {
	//	log.Println("done", "deletemyProposal", deleteOPQentry, "deleteop", pxs.NextOp)
	pxs.Px.Done(pxs.Seq)
	pxs.Seq++
}

func (pxs *PaxosServer) Proceeding() {
	for !pxs.Dead {
		doflag := false
		pxs.Mu.Lock()
		if pxs.NextOp <= pxs.OPQ.Len() {
			doflag = true
		}
		pxs.Mu.Unlock()
		var decision interface{}
		var decided bool
		//log.Println("loop:", pxs.ServerID, "doflag: ", doflag, pxs.NextOp, pxs.OPQ.Len())
		if doflag {
			pxs.Px.Start(pxs.Seq, pxs.OPQ.GetOP(pxs.NextOp))
			log.Println("serverid", pxs.ServerID, "op0", pxs.NextOp, "len", pxs.OPQ.Len(), "op", pxs.OPQ.GetOP(pxs.NextOp).OPtype, "Value:", pxs.OPQ.GetOP(pxs.NextOp).Value)
			log.Println("serverid", pxs.ServerID, "op1", 1, "op", pxs.OPQ.GetOP(pxs.NextOp+1).OPtype, "Value:", pxs.OPQ.GetOP(pxs.NextOp+1).Value)
			to := ToMinTime * time.Millisecond
			for !pxs.Dead {
				decided, decision = pxs.Px.Status(pxs.Seq)
				if decided {
					break
				}
				time.Sleep(to)
				if to < ToMaxTime*time.Millisecond {
					to = to * 2
				}
			}
			//log.Println("success!!")
			if pxs.Dead {
				return
			}
			v1, _ := decision.(Operation)
			//log.Printf("ServerId:%d ;pxs.Seq:%d ;deal%d:%s op.Value %s", pxs.ServerID, pxs.Seq, v1.Proposer, v1.OPtype, v1.Value)
			//log.Printf("%d:Now opvalue %s", pxs.NextOp, pxs.OPQ.GetOP(pxs.NextOp).Value)
			//	log.Println("enter deal", pxs.NextOp, v1.Proposer, "~~~", v1.OPtype)
			pxs.deal(v1)
			pxs.Done()
		} else {
			decided, decision = pxs.Px.Status(pxs.Seq)
			if decided {
				v1, _ := decision.(Operation)
				pxs.deal(v1)
				pxs.Done()
			} else {
				time.Sleep(10 * time.Millisecond)
			}
		}
	}
}

func (pxs *PaxosServer) hasmaster() bool {
	info, err := pxs.myserver.GetMasterInfo()
	if err != nil {
		panic("error: GetMasterInfo Impossible" + err.Error())
	}
	if info.MasterID == -1 || info.NowLeaseTime.Add(MasterLeaseTime*time.Second).Before(time.Now()) {
		return false
	}
	return true
}

func (pxs *PaxosServer) SetmasterID(masterID int) {
	//need to check whether it is legal !!!!!!!
	//log.Printf("StartsetMaster %d", masterID)
	info, err := pxs.myserver.GetMasterInfo()
	if err != nil {
		panic("error: GetMasterInfo Impossible" + err.Error())
	}
	//log.Println("TrySetmasterID", pxs.ServerID, " :", info.MasterID, "to", masterID, "--time", info.NowLeaseTime)
	if info.MasterID == -1 || info.NowLeaseTime.Add(MasterLeaseTime*time.Second).Before(time.Now()) || info.MasterID == masterID {
		log.Println("SetmasterID", pxs.ServerID, " :", info.MasterID, "to", masterID, "--time", info.NowLeaseTime)
		pxs.myserver.SetMasterInfo(servernode.MasterLease{
			MasterID:     masterID,
			NowLeaseTime: time.Now(),
		})
		//log.Println("~~~DNS~~~", pxs.ServerID == masterID)
		if pxs.ServerID == masterID {
			var tt bool
			log.Println("~~~DNS~~~", pxs.ServerID, pxs.config.ServerAddr)
			err := pxs.dnsRPCclient.Register(&store.KeyValue{
				Key:   servernode.Primary,
				Value: pxs.config.ServerAddr,
			}, &tt)
			if err != nil || !tt {
				log.Println("DNS is not reachable", err, " value is", pxs.config.PaxosServerRpcaddr)
			}
			var v string
			if err := pxs.dnsRPCclient.GetIPwithName(servernode.Primary, &v); err != nil {
				log.Printf("failed to get ip with name %s", servernode.Primary)
			}
			//log.Println("~~DNS: Master IP is ", v, " error:", err)
		}
	}
}

func (pxs *PaxosServer) GetmasterID() int {
	// Do not check whether there is a master
	info, err := pxs.myserver.GetMasterInfo()
	if err != nil {
		panic("error: GetMasterInfo Impossible" + err.Error())
	}
	return info.MasterID
}

func (pxs *PaxosServer) deal(op Operation) {
	log.Println("entered deal deal deal ServerID", pxs.ServerID, "Nextop", pxs.NextOp, "Proposer", op.Proposer, "~~~", op.OPtype)
	if op.OPtype == "ElectMaster" {
		var l int
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error(), "value is", op.Value)
		}
		//log.Println("wawawa", l)
		pxs.SetmasterID(l)
	}
	if op.OPtype == "GetHandle" {
		var l servernode.GetHandleRequestInfo
		r := &servernode.GetHandleReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen,", err.Error())
			}
		}
		e := pxs.myserver.DoGetHandle(l, r)

		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
			//log.Println("serverID", pxs.ServerID)
			//pxs.OPQ.GetResult(pxs.NextOp)
		}
	}
	if op.OPtype == "ReturnHandle" {
		var l servernode.ReturnHandleRequestInfo
		r := &servernode.ReturnHandleReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen,", err.Error())
			}
		}
		e := pxs.myserver.DoReturnHandle(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		} else {
			log.Println("Backup Returnhandle success")
		}
	}
	//	func (self *ServerNode) ListAllFiles(dummyReq string, reply *ListReply) error {
	//	func (self *ServerNode) GetContentsAndStat(req GetContentsAndStatRequestInfo, reply *GetContentAndStatReplyInfo) error{
	//	func (self *ServerNode)GetStat(req GetStatRequestInfo, reply *GetStatReplyInfo) error {

	//	func (self *ServerNode) SetContents(req SetContentsRequestInfo, reply *SetContentsReplyInfo) error {
	if op.OPtype == "SetContents" {
		var l servernode.SetContentsRequestInfo
		r := &servernode.SetContentsReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen,", err.Error())
			}
		}
		//log.Println("server:DoSetContent", pxs.ServerID)
		e := pxs.myserver.DoSetContents(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	//	func (self *ServerNode) SetACL(req SetACLRequestInfo, reply *SetACLReplyInfo) error {
	if op.OPtype == "SetACL" {
		var l servernode.SetACLRequestInfo
		r := &servernode.SetACLReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen, ", err.Error())
			}
		}
		e := pxs.myserver.DoSetACL(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	//	func (self *ServerNode) Delete(req DeleteRequestInfo, reply *DeleteReplyInfo) error {
	if op.OPtype == "Delete" {
		var l servernode.DeleteRequestInfo
		r := &servernode.DeleteReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen, ", err.Error())
			}
		}
		e := pxs.myserver.DoDelete(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	//	func (self *ServerNode) Acquire(req AcquireRequestInfo, reply *AcquireReplyInfo) error {
	if op.OPtype == "Acquire" && op.Proposer != pxs.ServerID {
		//log.Println("paxosserver tryacquire", op.Proposer, pxs.ServerID)
		var l servernode.TryAcquireRequestInfo
		r := &servernode.TryAcquireReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}

		e := pxs.myserver.DoTryAcquire(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	//	func (self *ServerNode) TryAcquire(req TryAcquireRequestInfo, reply *TryAcquireReplyInfo) error {
	if op.OPtype == "TryAcquire" {
		//log.Println("paxosserver tryacquire", op.Proposer, pxs.ServerID)
		var l servernode.TryAcquireRequestInfo
		r := &servernode.TryAcquireReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		e := pxs.myserver.DoTryAcquire(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	//	func (self *ServerNode) Release(req ReleaseRequestInfo, reply *ReleaseReplyInfo) error {
	if op.OPtype == "Release" {
		var l servernode.ReleaseRequestInfo
		r := &servernode.ReleaseReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen, ", err.Error())
			}
		}
		log.Println("start_Dorelease", pxs.ServerID)
		e := pxs.myserver.DoRelease(l, r)
		log.Println("finish_Dorelease", pxs.ServerID)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	//if op.OPtype == "KeepLive" {
	//	var l servernode.KeepLiveRequestInfo
	//	r := &servernode.KeepLiveReplyInfo{}
	//	if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
	//		log.Println("should not happen, ", err.Error())
	//	}
	//	if op.Proposer == pxs.ServerID {
	//		if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
	//			log.Println("should not happen, ", err.Error())
	//		}
	//	}
	//	//log.Println("start_Dorelease", pxs.ServerID)
	//	e := pxs.myserver.DoKeepLive(l, r)
	//	//	log.Println("finish_Dorelease", pxs.ServerID)
	//	if op.Proposer == pxs.ServerID {
	//		pxs.OPQ.SetResult(pxs.NextOp, e, r)
	//	}
	//}
	if op.OPtype == "SendContract" {
		var l servernode.SendContractRequestInfo
		r := &servernode.SendContractReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen,", err.Error())
			}
		}
		e := pxs.myserver.DoSendContract(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}
	if op.OPtype == "DeleteContract" {
		var l servernode.DeleteContractRequestInfo
		r := &servernode.DeleteContractReplyInfo{}
		if err := json.Unmarshal([]byte(op.Value), &l); err != nil {
			log.Println("should not happen, ", err.Error())
		}
		if op.Proposer == pxs.ServerID {
			if err := json.Unmarshal([]byte(op.Reply), r); err != nil {
				log.Println("should not happen,", err.Error())
			}
		}
		e := pxs.myserver.DoDeleteContract(l, r)
		if op.Proposer == pxs.ServerID {
			pxs.OPQ.SetResult(pxs.NextOp, e, r)
		}
	}

	if op.Proposer == pxs.ServerID {
		pxs.Mu.Lock()
		pxs.NextOp++
		pxs.Mu.Unlock()
	}
}
