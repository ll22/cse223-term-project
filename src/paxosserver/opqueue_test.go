package paxosserver_test

import (
	"paxosserver"
	"testing"
	"testp"
)

func TestJustSimpleTest(t *testing.T) {
	c := paxosserver.NewOPQueue()
	var op paxosserver.Operation
	for i := 0; i <= 1000; i++ {
		op.Proposer = i
		c.Add(op)
	}
	testp.As(c.GetOP(500).Proposer == 500)
	c.Done(900)
	testp.As(c.GetOP(500).Proposer == 0)
	testp.As(c.GetOP(1000).Proposer == 1000)
	for i := 1; i <= 1000; i++ {
		op.Proposer = i + 1000
		c.Add(op)
		c.Done(i + 998)
	}
	testp.As(c.GetOP(2000).Proposer == 2000)
	testp.As(c.GetOP(1999).Proposer == 1999)
	testp.As(c.GetOP(1998).Proposer == 0)
}
