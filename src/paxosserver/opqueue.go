package paxosserver

import (
	"sync"
	"log"
)

type Operation struct {
	OPtype   string
	Value    string
	Reply    string
	Res      interface{}
	Operror  error
	Proposer int //serverId
}

type OPQueue struct {
	OpSeq      map[int]Operation
	HighestNum int
	LowestNum  int
	m          *sync.Mutex
}

func NewOPQueue() *OPQueue {
	OPQ := &OPQueue{
		OpSeq:      make(map[int]Operation),
		HighestNum: -1,
		LowestNum:  -1,
		m:          &sync.Mutex{},
	}
	return OPQ
}
func (opq *OPQueue) GetOP(index int) Operation {
	opq.m.Lock()
	defer opq.m.Unlock()
	return opq.OpSeq[index]
}
func (opq *OPQueue) GetResult(index int) (error, interface{}) {
	opq.m.Lock()
	defer opq.m.Unlock()
	log.Println("opq:getresult", index, ":", opq.OpSeq[index].OPtype, opq.OpSeq[index].Operror, opq.OpSeq[index].Res)
	return opq.OpSeq[index].Operror, opq.OpSeq[index].Res
}

func (opq *OPQueue) SetResult(index int, e error, res interface{}) {
	opq.m.Lock()
	defer opq.m.Unlock()
	//op := opq.OpSeq[index]
	op := Operation{}
	op.Operror = e
	op.Res = res
	opq.OpSeq[index] = op
	if res == nil {
		panic("set nil")
	}
	log.Println("opq:setresul", index, opq.OpSeq[index].Operror, opq.OpSeq[index].Res)
}

func (opq *OPQueue) Len() int {
	opq.m.Lock()
	defer opq.m.Unlock()
	return opq.HighestNum
}

func (opq *OPQueue) Add(op Operation) {
	//	log.Println("opq:", op.OPtype, op.Proposer, op.Value, "Len", opq.HighestNum+1)
	opq.m.Lock()
	defer opq.m.Unlock()
	opq.HighestNum++
	opq.OpSeq[opq.HighestNum] = op
}

func (opq *OPQueue) Done(seq int) {
	opq.m.Lock()
	defer opq.m.Unlock()
	//for opq.LowestNum < seq {
	//	opq.LowestNum++
	delete(opq.OpSeq, opq.LowestNum)
	//}
}
