package servernode

import "sync"

type cacheMap struct {
	fnToClients map[string][]string
	m sync.Mutex
}

func NewCacheMap() *cacheMap {

	return &cacheMap{
		fnToClients: make(map[string][]string),
		m: sync.Mutex{},
	}
}

func (cache *cacheMap) ClientHasCache(fn string, clientAddr string) bool {
	cache.m.Lock()
	defer cache.m.Unlock()
	clients, exist := cache.fnToClients[fn]
	if !exist {
		return exist
	}

	for _, client := range clients {
		if client == clientAddr {
			return true
		}
	}
	return false
}

func (cache *cacheMap) addClientToCacheList(fn string, clientAddr string) {
	cache.m.Lock()
	defer cache.m.Unlock()
	clients, exist := cache.fnToClients[fn]
	if !exist {
		clients = make([]string, 0)
		cache.fnToClients[fn] = clients
	}
	clients = append(clients, clientAddr)
	cache.fnToClients[fn] = clients
}

func (cache *cacheMap) removeClientFromCacheList(fn string, clientAddr string) {
	cache.m.Lock()
	defer cache.m.Unlock()
	clients, exist := cache.fnToClients[fn]
	if !exist {
		return
	}
	for i, client := range clients {
		if client == clientAddr {
			clients = append(clients[:i], clients[i+1:]...)
		}
	}
	cache.fnToClients[fn] = clients
}

func (cache *cacheMap) getClientList(fn string) []string {
	cache.m.Lock()
	defer cache.m.Unlock()
	clients, exist := cache.fnToClients[fn]
	if !exist {
		return nil
	}
	return clients
}
