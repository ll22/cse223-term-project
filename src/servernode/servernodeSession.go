package servernode

import (
	"log"
	"time"
)

func contains(list []string, v string) bool {
	for _, l := range list {
		if l == v {
			return true
		}
	}
	return false
}

func (serverNode *ServerNode) waitingKeepLive(
	maxWait int,
	timerStop chan bool,
	sxnClientAddr string,
) {

	log.Printf("Starting waiting for reconnections from client: %s, wait: %d sec", sxnClientAddr, maxWait)
	timeout := time.After(time.Duration(maxWait) * time.Second)

	select {
	case <-timerStop:
		// a read from timerStop has occurred
		log.Printf("server session timer stopped for client: %s", sxnClientAddr)
		return

	case <-timeout:
		// the read from ch has timed out
		log.Printf("server session times out. About to clean up session for client %s:", sxnClientAddr)
		serverNode.cleanUpSession(sxnClientAddr)
		return
	}
}

func (serverNode *ServerNode) cleanUpSession(sxnClientAddr string) {
	log.Printf("trying to clean up session for client %s:", sxnClientAddr)
	//clean up contract
	if err := serverNode.handleContract(sxnClientAddr); err != nil {
		log.Println(err.Error())
		return
	}

	serverNode.sxnMtx.Lock()
	if sxn, exist := serverNode.sxnCh[sxnClientAddr]; !exist {
		serverNode.sxnMtx.Unlock()
		log.Println("trying to cleanup a session that doesn't exist, perhaps you are backup?")
		return
	} else {
		//sxn.timerClosed <- true
		sxn.deprecated <- true
		// sxn.invalidate()
		// close(sxn.events)
		// close(sxn.sessionClosed)
		// close(sxn.timerClosed)
		delete(serverNode.sxnCh, sxnClientAddr)
		log.Printf("\x1b[31;1mDelete session %s\x1b[0m", sxnClientAddr)
		serverNode.sxnMtx.Unlock()
	}

	log.Printf("done cleaning up session for client %s:", sxnClientAddr)

	// remove all handles belong to this session
	handleList, err := serverNode.fs.listHandles(sxnClientAddr)

	if err != nil {
		panic(err)
	}

	log.Println(handleList)
	for _, handle := range handleList {
		if err := serverNode.fs.removeHandle(sxnClientAddr, handle); err != nil {
			panic(err)
		}
		log.Printf("try to release for %s", sxnClientAddr)
		serverNode.fs.Release(handle, sxnClientAddr, "W")
		serverNode.fs.Release(handle, sxnClientAddr, "R")

		log.Printf("Removed handle %s for client %s \n", handle, sxnClientAddr)
	}
	log.Printf("Clean up session for %s finished!", sxnClientAddr)
}

func (serverNode *ServerNode) isSessionAlive(sessionClientAddr string) bool {
	serverNode.sxnMtx.Lock()
	defer serverNode.sxnMtx.Unlock()
	_, exist := serverNode.sxnCh[sessionClientAddr]
	return exist
}

func (serverNode *ServerNode) tryStartSession(addr string) error {
	var exist bool

	serverNode.sxnMtx.Lock()
	if _, exist = serverNode.sxnCh[addr]; !exist {
		session := NewSessionChans()
		serverNode.sxnCh[addr] = session
		go serverNode.waitingKeepLive(ClientTimeout, session.IsTimerStopped(), addr)
	}
	serverNode.sxnMtx.Unlock()

	return nil
}

func (serverNode *ServerNode) registerHandlerForSession(addr, filepath string) error {
	serverNode.sxnMtx.Lock()
	_, exist := serverNode.sxnCh[addr]
	serverNode.sxnMtx.Unlock()
	// update the handle information for this client
	openFiles, err := serverNode.fs.listHandles(addr)
	if err != nil {
		return err
	}

	if !exist && len(openFiles) != 0 {
		log.Println("There should not be any open file for client %s\n", addr)
	}

	if err = serverNode.fs.addHandle(addr, filepath); err != nil {
		return err
	}
	return nil
}

func (serverNode *ServerNode) unregisterHandlerForSession(addr, filepath string) error {
	if err := serverNode.fs.removeHandle(addr, filepath); err != nil {
		return err
	}
	return nil
}

func (serverNode *ServerNode) tryCloseSession(addr string) error {
	// If this is the last handle returned, tell the corresponding KeepLive session to stop and
	// clean up session info using "serverNode.cleanUpSession(req.ClientAddr)"
	serverNode.sxnMtx.Lock()
	defer serverNode.sxnMtx.Unlock()
	if session, exist := serverNode.sxnCh[addr]; exist {
		go session.Close()
	}
	return nil
}
