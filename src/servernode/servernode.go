package servernode

import (
	"errors"
	"events"
	"fmt"
	"log"
	"path/filepath"
	"sync"
	"time"
)

const (
	ReadHandle  = "R"
	WriteHandle = "W"
	ACLHandle   = "ACL"

	Primary = "p"
	Backup1 = "b1"
	Backup2 = "b2"
	Backup3 = "b3"
	Backup4 = "b4"
	Backup5 = "b5"
)

// ServerNodeT servernode interface that is exposed to the RPC
type ServerNodeT interface {
	OpenSession(OpenSessionRequestInfo, *OpenSessionReplyInfo) error
	CloseSession(CloseSessionRequestInfo, *CloseSessionReplyInfo) error
	GetHandle(GetHandleRequestInfo, *GetHandleReplyInfo) error
	ReturnHandle(ReturnHandleRequestInfo, *ReturnHandleReplyInfo) error
	ListAllFiles(string, *ListReply) error
	GetContentsAndStat(GetContentsAndStatRequestInfo, *GetContentsAndStatReplyInfo) error
	GetStat(GetStatRequestInfo, *GetStatReplyInfo) error
	SetContents(SetContentsRequestInfo, *SetContentsReplyInfo) error
	SetACL(SetACLRequestInfo, *SetACLReplyInfo) error
	Delete(DeleteRequestInfo, *DeleteReplyInfo) error
	Acquire(AcquireRequestInfo, *AcquireReplyInfo) error
	TryAcquire(TryAcquireRequestInfo, *TryAcquireReplyInfo) error
	TryMultiAcquire(info TryMultiAcquireRequestInfo, replyInfo *TryMultiAcquireReplyInfo) error
	Release(ReleaseRequestInfo, *ReleaseReplyInfo) error
	TryMultiRelease(info TryMultiReleaseRequestInfo, replyInfo *TryMultiReleaseReplyInfo) error
	GetSequencer(GetSequencerRequestInfo, *GetSequencerReplyInfo) error
	CheckSequencer(CheckSequencerRequestInfo, *CheckSequencerReplyInfo) error
	KeepLive(KeepLiveRequestInfo, *KeepLiveReplyInfo) error
	SendContract(SendContractRequestInfo, *SendContractReplyInfo) error
	DeleteContract(DeleteContractRequestInfo, *DeleteContractReplyInfo) error
}

// ServerNodeProcessorT servernode interface that is exposed to external nodes, like paxosserver
// Only it really do the jobs.
type ServerNodeProcessorT interface {
	On(servEvent string, ln func(request interface{}, reply interface{}) error)
	DoGetHandle(GetHandleRequestInfo, *GetHandleReplyInfo) error
	DoReturnHandle(ReturnHandleRequestInfo, *ReturnHandleReplyInfo) error
	DoListAllFiles(string, *ListReply) error
	DoGetContentsAndStat(GetContentsAndStatRequestInfo, *GetContentsAndStatReplyInfo) error
	DoGetStat(GetStatRequestInfo, *GetStatReplyInfo) error
	DoSetContents(SetContentsRequestInfo, *SetContentsReplyInfo) error
	DoSetACL(SetACLRequestInfo, *SetACLReplyInfo) error
	DoDelete(DeleteRequestInfo, *DeleteReplyInfo) error
	DoAcquire(AcquireRequestInfo, *AcquireReplyInfo) error
	DoTryAcquire(TryAcquireRequestInfo, *TryAcquireReplyInfo) error
	DoTryMultiAcquire(info TryMultiAcquireRequestInfo, replyInfo *TryMultiAcquireReplyInfo) error
	DoTryMultiRelease(info TryMultiReleaseRequestInfo, replyInfo *TryMultiReleaseReplyInfo) error
	DoRelease(ReleaseRequestInfo, *ReleaseReplyInfo) error
	DoGetSequencer(GetSequencerRequestInfo, *GetSequencerReplyInfo) error
	DoCheckSequencer(CheckSequencerRequestInfo, *CheckSequencerReplyInfo) error
	DoKeepLive(KeepLiveRequestInfo, *KeepLiveReplyInfo) error
	DoSendContract(SendContractRequestInfo, *SendContractReplyInfo) error
	DoDeleteContract(DeleteContractRequestInfo, *DeleteContractReplyInfo) error
}

// ServerNode a default wrapper of a store
type ServerNode struct {
	addr           string
	sxnMtx         *sync.Mutex
	sxnCh          map[string]*SessionChans
	fs             FileSystemT
	listeners      map[string]func(req, res interface{}) error
	lnMtx          *sync.Mutex
	clientCacheMap *cacheMap // key: file path -> value: client address
}

// test interface
var (
	_ ServerNodeT          = &ServerNode{}
	_ ServerNodeProcessorT = &ServerNode{}
	_ PaxosMasterInfoT     = &ServerNode{}
)

type ServerNodeConfig struct {
	StoreAddr  string
	ServerAddr string
}

// NewServerNode create a new server node with address addr associated
func NewServerNode(config *ServerNodeConfig) *ServerNode {
	server := &ServerNode{
		addr:           config.ServerAddr,
		sxnCh:          make(map[string]*SessionChans),
		sxnMtx:         &sync.Mutex{},
		fs:             NewFS(config.StoreAddr),
		listeners:      make(map[string]func(req, res interface{}) error),
		lnMtx:          &sync.Mutex{},
		clientCacheMap: NewCacheMap(),
	}
	return server
}

func (serverNode *ServerNode) On(event string, l func(req, res interface{}) error) {
	serverNode.lnMtx.Lock()
	defer serverNode.lnMtx.Unlock()
	serverNode.listeners[event] = l
}

func (serverNode *ServerNode) listenOrElse(
	event string,
	req, res interface{},
	defln func() error, // default listener
) error {
	if req == nil || res == nil {
		return errors.New("should not be nil")
	}
	//log.Println("lalala1")
	serverNode.lnMtx.Lock()
	ln, ok := serverNode.listeners[event]
	serverNode.lnMtx.Unlock()
	if ok {
		//log.Println("lalala1", event)
		return ln(req, res)
	} else {
		return defln()
	}
}

var (
	SessionErr = errors.New("session is dead")
)

func (serverNode *ServerNode) requireSessionAlive(addr string) error {
	if !serverNode.isSessionAlive(addr) {
		return SessionErr
	}
	return nil
}

func (serverNode *ServerNode) OpenSession(req OpenSessionRequestInfo, res *OpenSessionReplyInfo) error {
	// get handle succeed. check if this handle is the first one this Chubby client ever requests
	// if no handle exists
	if err := serverNode.tryStartSession(req.Addr); err != nil {
		log.Println(err.Error())
		return err
	}
	res.Succ = true
	return nil
}

func (serverNode *ServerNode) CloseSession(req CloseSessionRequestInfo, res *CloseSessionReplyInfo) error {
	if err := serverNode.tryCloseSession(req.Addr); err != nil {
		return err
	}
	res.Succ = true
	return nil
}

func (serverNode *ServerNode) GetHandle(req GetHandleRequestInfo, res *GetHandleReplyInfo) error {
	return serverNode.listenOrElse("GetHandle", req, res,
		func() error {
			return serverNode.DoGetHandle(req, res)
		},
	)
}

// GetHandle request to create a handle
func (serverNode *ServerNode) DoGetHandle(req GetHandleRequestInfo, res *GetHandleReplyInfo) error {
	// log.Printf("%+v\n", req)
	// defer log.Printf("%+v\n", res)
	log.Println("DoGetHandle addr: ", serverNode.addr, "filepath", req.FilePath)
	defer log.Println("DoGetHandle success")
	if req.CreateFile && req.HandleType == WriteHandle {
		log.Println(req)
		log.Printf("try to create file %s", req.FilePath)
		succ, err := serverNode.create(req, res)
		if err != nil {
			return err
		}
		if succ {
			if e := serverNode.registerHandlerForSession(req.Addr, req.FilePath); e != nil {
				return e
			}
		}
		res.Succ = succ
		return nil
	}

	if exist, err := serverNode.fs.FileExists(req.FilePath); err != nil {
		return err
	} else if !exist {
		res.ErrType = FileNotExist
		res.Err = fmt.Sprintf("the target file %s does not exist", req.FilePath)
		return nil
	}

	log.Println("check file exist")

	// not create file
	aclOk, err := serverNode.fs.CheckACL(req.FilePath, req.Addr, req.HandleType)
	if err != nil {
		return err
	}
	if !aclOk {
		log.Println("access denied")
		res.Succ = false
		res.Err = fmt.Sprintf("access denied, request %s do not have mode %s in ACL", req.Addr, req.HandleType)
		res.ErrType = AccessDenied
	} else {
		log.Println("before register handle")
		if e := serverNode.registerHandlerForSession(req.Addr, req.FilePath); e != nil {
			return e
		}
		log.Println("register correct")
		res.Succ = true
		res.Err = ""
		res.ErrType = NoError
	}
	return nil
}

func (serverNode *ServerNode) ReturnHandle(req ReturnHandleRequestInfo, res *ReturnHandleReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.ClientAddr); e != nil {
		return e
	}
	return serverNode.listenOrElse("ReturnHandle", req, res,
		func() error {
			return serverNode.DoReturnHandle(req, res)
		},
	)
}

// ReturnHandle close a opened file handle and do gc in case.
func (serverNode *ServerNode) DoReturnHandle(req ReturnHandleRequestInfo, res *ReturnHandleReplyInfo) error {
	log.Printf("Client %s trying to return handle", req.ClientAddr)
	// Update session info from the DB

	if e := serverNode.unregisterHandlerForSession(req.ClientAddr, req.FilePath); e != nil {
		return e
	}
	log.Println("client finish DoReturnHandle")
	return nil
}

// ListReply wrapper of string slice
type ListReply struct {
	L []string
}

func (serverNode *ServerNode) ListAllFiles(directory string, reply *ListReply) error {
	return serverNode.listenOrElse("ListAllFiles", directory, reply,
		func() error {
			return serverNode.DoListAllFiles(directory, reply)
		},
	)
}

// ListAllFiles get an array of all files
func (serverNode *ServerNode) DoListAllFiles(directory string, reply *ListReply) error {
	var err error
	reply.L, err = serverNode.fs.ListFiles(directory)
	if err != nil {
		reply.L = nil
	}
	return err
}

func (serverNode *ServerNode) GetContentsAndStat(
	req GetContentsAndStatRequestInfo,
	reply *GetContentsAndStatReplyInfo,
) error {
	if e := serverNode.requireSessionAlive(req.Requestor); e != nil {
		return e
	}
	return serverNode.listenOrElse("GetContentsAndStat", req, reply,
		func() error {
			return serverNode.DoGetContentsAndStat(req, reply)
		},
	)
}

// GetContentsAndStat get contents and status from FS if access is allowed
func (serverNode *ServerNode) DoGetContentsAndStat(
	req GetContentsAndStatRequestInfo,
	reply *GetContentsAndStatReplyInfo,
) error {

	fileInfo, err := serverNode.fs.Read(req.FilePath)
	if err != nil {
		log.Println("Error in DoGetContentsAndStat()!")
		return err
	}
	reply.Content = fileInfo.Content
	reply.Stat = fileInfo.Meta

	if !serverNode.clientCacheMap.ClientHasCache(req.FilePath, req.Requestor) {
		// add to cache
		serverNode.clientCacheMap.addClientToCacheList(req.FilePath, req.Requestor)
	}

	return nil
}

func (serverNode *ServerNode) GetStat(req GetStatRequestInfo, reply *GetStatReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Requestor); e != nil {
		return e
	}
	return serverNode.listenOrElse("GetStat", req, reply,
		func() error {
			return serverNode.DoGetStat(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoGetStat(req GetStatRequestInfo, reply *GetStatReplyInfo) error {
	if fileInfo, err := serverNode.fs.Read(req.FilePath); err != nil {
		return err
	} else {
		reply.Stat = fileInfo.Meta
		return nil
	}
}

func (serverNode *ServerNode) SetContents(req SetContentsRequestInfo, reply *SetContentsReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Requestor); e != nil {
		return e
	}
	return serverNode.listenOrElse("SetContents", req, reply,
		func() error {
			return serverNode.DoSetContents(req, reply)
		},
	)
}

// SetContents set the file contents
func (serverNode *ServerNode) DoSetContents(req SetContentsRequestInfo, reply *SetContentsReplyInfo) error {
	//log.Println("node:DoSetContent", serverNode.addr)
	// send invalidation to all clients who have cache for this file
	clients := serverNode.clientCacheMap.getClientList(req.FilePath)
	log.Printf("Clients with file %s cached are: %v", req.FilePath, clients)

	for _, client := range clients {

		// don't send invalidation to the requestor
		if client == req.Requestor {
			continue
		}

		if clientChan, clientChanExist := serverNode.sxnCh[client]; clientChanExist {

			//clientChan.events <- events.Event{Type:events.EventHandlerInvalid,Msg:"invalidate"}
			log.Printf("Trying to send invalidation message to %s", client)
			clientChan.Emit(events.Event{Type: events.EventHandlerInvalid, Msg: req.FilePath})
			serverNode.clientCacheMap.removeClientFromCacheList(req.FilePath, client)

		} else {
			//panic("How can client doesn't have a channel?")
		}
	}

	if !serverNode.clientCacheMap.ClientHasCache(req.FilePath, req.Requestor) {
		// add to cache
		serverNode.clientCacheMap.addClientToCacheList(req.FilePath, req.Requestor)
	}

	if succ, err := serverNode.fs.Write(req.FilePath, &FileContentInfo{req.Content}, nil); err != nil {
		reply.Succ = succ
		return err
	} else {
		reply.Succ = succ
		return nil
	}
}

func (serverNode *ServerNode) SetACL(req SetACLRequestInfo, reply *SetACLReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Addr); e != nil {
		return e
	}
	return serverNode.listenOrElse("SetACL", req, reply,
		func() error {
			return serverNode.DoSetACL(req, reply)
		},
	)
}

// SetACL set ACL of a file
func (serverNode *ServerNode) DoSetACL(req SetACLRequestInfo, reply *SetACLReplyInfo) error {
	if succ, err := serverNode.fs.Write(req.FilePath, nil, &req.ACL); err != nil {
		reply.Succ = succ
		return err
	} else {
		reply.Succ = succ
		return nil
	}
}

func (serverNode *ServerNode) Delete(req DeleteRequestInfo, reply *DeleteReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Addr); e != nil {
		return e
	}
	return serverNode.listenOrElse("Delete", req, reply,
		func() error {
			return serverNode.DoDelete(req, reply)
		},
	)
}

// Delete delete a file
func (serverNode *ServerNode) DoDelete(req DeleteRequestInfo, reply *DeleteReplyInfo) error {
	succ, err := serverNode.fs.Delete(req.FilePath, req.IsDir)
	reply.Succ = succ
	return err
}

// Acquire acquires a file lock. This is a blocking method.
func (serverNode *ServerNode) Acquire(req AcquireRequestInfo, reply *AcquireReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Acquirer); e != nil {
		return e
	}
	if e := serverNode.DoAcquire(req, reply); e != nil {
		return e
	}
	nop := func() error {
		//return serverNode.DoAcquire(req, reply)
		log.Println("we don nothing here")
		return nil
	}
	if err := serverNode.listenOrElse("Acquire", req, reply, nop); err != nil {
		relreq := ReleaseRequestInfo{
			Holder:   req.Acquirer,
			FilePath: req.FilePath,
			Mode:     req.Mode,
		}
		relres := ReleaseReplyInfo{}
		if e := serverNode.DoRelease(relreq, &relres); e != nil {
			return e
		}
		reply.Succ = false
		return err
	}
	reply.Succ = true // just ignore the response, it's already succeeded
	return nil
}

// Acquire keep trying to acquire a lock until the number of retries exceed the limit
func (serverNode *ServerNode) DoAcquire(req AcquireRequestInfo, reply *AcquireReplyInfo) error {
	succ, err := serverNode.fs.Acquire(req.FilePath, req.Acquirer, req.Mode, int(req.Retries))
	reply.Succ = succ
	return err
}

func (serverNode *ServerNode) TryAcquire(req TryAcquireRequestInfo, reply *TryAcquireReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Acquirer); e != nil {
		return e
	}
	return serverNode.listenOrElse("TryAcquire", req, reply,
		func() error {
			return serverNode.DoTryAcquire(req, reply)
		},
	)
}

// TryAcquire try to acquire a lock and get the lock iff the acquirer is allowed to.
func (serverNode *ServerNode) DoTryAcquire(req TryAcquireRequestInfo, reply *TryAcquireReplyInfo) error {
	//log.Println("In DoTryAcquire", serverNode.addr)
	if succ, err := serverNode.fs.Acquire(req.FilePath, req.Acquirer, req.Mode, 0); err != nil {
		reply.Succ = succ
		return err
	} else {
		reply.Succ = succ
		return nil
	}
}

func (serverNode *ServerNode) TryMultiAcquire(
	req TryMultiAcquireRequestInfo, reply *TryMultiAcquireReplyInfo,
) error {
	if e := serverNode.requireSessionAlive(req.Acquirer); e != nil {
		return e
	}
	return serverNode.listenOrElse("TryMultiAcquire", req, reply,
		func() error {
			return serverNode.DoTryMultiAcquire(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoTryMultiAcquire(
	req TryMultiAcquireRequestInfo, reply *TryMultiAcquireReplyInfo,
) error {

	handleError := func(releaseReqs []ReleaseRequestInfo) error {
		var err error
		res := ReleaseReplyInfo{Succ: false}
		for _, req := range releaseReqs {
			if e := serverNode.DoRelease(req, &res); e != nil {
				err = e
			}
		}
		return err
	}

	if len(req.Modes) != len(req.FilePaths) {
		return errors.New("invalid request")
	}

	releaseReqs := make([]ReleaseRequestInfo, 0)
	for i, file := range req.FilePaths {
		tx := TryAcquireRequestInfo{
			Acquirer: req.Acquirer,
			Mode:     req.Modes[i],
			FilePath: file,
		}
		rx := TryAcquireReplyInfo{Succ: false}
		releaseReqs = append(releaseReqs, ReleaseRequestInfo{
			FilePath: file,
			Mode:     req.Modes[i],
			Holder:   req.Acquirer,
		})
		if e := serverNode.DoTryAcquire(tx, &rx); e != nil || !rx.Succ {
			reply.Succ = false
			if e := handleError(releaseReqs); e != nil {
				return e
			}
			return e
		}
	}

	reply.Succ = true
	return nil
}

func (serverNode *ServerNode) Release(req ReleaseRequestInfo, reply *ReleaseReplyInfo) error {
	// don't check session
	return serverNode.listenOrElse("Release", req, reply,
		func() error {
			return serverNode.DoRelease(req, reply)
		},
	)
}

// Release release a lock. Always return true if error didn't happen
func (serverNode *ServerNode) DoRelease(req ReleaseRequestInfo, reply *ReleaseReplyInfo) error {
	if succ, err := serverNode.fs.Release(req.FilePath, req.Holder, req.Mode); err != nil {
		reply.Succ = succ
		return err
	} else {
		reply.Succ = succ
		return nil
	}
}

func (serverNode *ServerNode) TryMultiRelease(
	req TryMultiReleaseRequestInfo, reply *TryMultiReleaseReplyInfo,
) error {
	if e := serverNode.requireSessionAlive(req.Holder); e != nil {
		return e
	}
	return serverNode.listenOrElse("TryMultiRelease", req, reply,
		func() error {
			return serverNode.DoTryMultiRelease(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoTryMultiRelease(
	req TryMultiReleaseRequestInfo, reply *TryMultiReleaseReplyInfo,
) error {
	if len(req.FilePaths) != len(req.Modes) {
		return errors.New("mismatch filepaths and mdoes")
	}

	handleError := func(acquireReqs []TryAcquireRequestInfo) error {
		var err error
		res := TryAcquireReplyInfo{}
		for _, req := range acquireReqs {
			if e := serverNode.TryAcquire(req, &res); e != nil {
				err = e
			}
		}
		return err
	}
	acquireReqs := make([]TryAcquireRequestInfo, 0)
	for i, file := range req.FilePaths {
		tx := ReleaseRequestInfo{
			Holder:   req.Holder,
			FilePath: file,
			Mode:     req.Modes[i],
		}
		rx := ReleaseReplyInfo{}
		acquireReqs = append(acquireReqs, TryAcquireRequestInfo{
			Mode:     req.Modes[i],
			FilePath: file,
			Acquirer: req.Holder,
		})
		if e := serverNode.DoRelease(tx, &rx); e != nil || !rx.Succ {
			reply.Succ = false
			if e := handleError(acquireReqs); e != nil {
				return e
			}
			return e
		}
	}

	reply.Succ = true
	return nil
}

func (serverNode *ServerNode) GetSequencer(req GetSequencerRequestInfo, reply *GetSequencerReplyInfo) error {
	return serverNode.listenOrElse("GetSequencer", req, reply,
		func() error {
			return serverNode.DoGetSequencer(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoGetSequencer(req GetSequencerRequestInfo, reply *GetSequencerReplyInfo) error {
	fileInfo, err := serverNode.fs.Read(req.FilePath)
	if err != nil {
		return err
	}
	mode, err := serverNode.fs.GetLockMode(req.FilePath)
	if err != nil {
		return err
	}
	seq := Sequencer{
		LockGeneration: fileInfo.Meta.LockGenerationNum,
		Name:           req.FilePath,
		Mode:           mode,
	}
	reply.Seq = seq
	reply.Succ = true
	return nil
}

func (serverNode *ServerNode) CheckSequencer(req CheckSequencerRequestInfo, reply *CheckSequencerReplyInfo) error {
	return serverNode.listenOrElse("CheckSequencer", req, reply,
		func() error {
			return serverNode.DoCheckSequencer(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoCheckSequencer(req CheckSequencerRequestInfo, reply *CheckSequencerReplyInfo) error {
	fileInfo, err := serverNode.fs.Read(req.FilePath)
	if err != nil {
		return err
	}
	mode, err := serverNode.fs.GetLockMode(req.FilePath)
	if err != nil {
		reply.Succ = false
		return err
	}
	seq := Sequencer{
		LockGeneration: fileInfo.Meta.LockGenerationNum,
		Name:           req.FilePath,
		Mode:           mode,
	}

	reply.Succ = req.Seq.Equals(seq)

	return nil
}

func (serverNode *ServerNode) KeepLive(req KeepLiveRequestInfo, reply *KeepLiveReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.ClientAddr); e != nil {
		return e
	}
	return serverNode.DoKeepLive(req, reply)
	//return serverNode.listenOrElse("KeepLive", req, reply,
	//	func() error {
	//		return nil
	//	},
	//)
}

func (serverNode *ServerNode) DoKeepLive(req KeepLiveRequestInfo, reply *KeepLiveReplyInfo) error {

	// not such a session. Session should be created when a client first calls GetHandle
	serverNode.sxnMtx.Lock()
	sxnHandle, exist := serverNode.sxnCh[req.ClientAddr]
	serverNode.sxnMtx.Unlock()
	//log.Printf("DoKeepLive called from %s", req.ClientAddr)
	if !exist {
		return errors.New("No such a session to keep alive")
	}

	// turn off the waiting-keep-live leaseTimer
	sxnHandle.StopTimer()
	var event events.Event
	log.Printf("DoKeepLive return leaseTimer created for %s", req.ClientAddr)

	select {
	case <-sxnHandle.IsClose():
		log.Printf("%s keepLive session is told to close", req.ClientAddr)
		// close the session
		serverNode.cleanUpSession(req.ClientAddr)
		log.Printf("session is over for client %s", req.ClientAddr)
		reply.Terminate = true
		reply.Event.Type = events.EventNil
		reply.Event.Msg = ""
		return nil

	case <-time.After(time.Second * (LeasePeriod - LeaseSecBeforeExpire)):
		log.Printf("%s keepLive lease time out", req.ClientAddr)
		// reply client
		// tell client about the next lease interval
		reply.Lease = LeasePeriod
		reply.Event.Type = events.EventNil
		reply.Event.Msg = ""
		// start the the waiting-keep-live leaseTimer again
		go serverNode.waitingKeepLive(ClientTimeout, sxnHandle.timerClosed, req.ClientAddr)

	case event = <-sxnHandle.GetEvent():
		log.Printf("Events accepted from the events channel. Send event %s to client %s", event.Msg, req.ClientAddr)
		reply.Event = event
		go serverNode.waitingKeepLive(ClientTimeout, sxnHandle.timerClosed, req.ClientAddr)

	case <-sxnHandle.deprecated:
		reply.Event.Type = events.EventSessionDeprecated
		reply.Event.Msg = "the session is deprecated"
		reply.Terminate = true
		return nil
	}

	log.Printf("keepLive session returns to client %s", req.ClientAddr)
	reply.Terminate = false
	return nil
}

func (serverNode *ServerNode) SendContract(req SendContractRequestInfo, reply *SendContractReplyInfo) error {
	if e := serverNode.requireSessionAlive(req.Addr); e != nil {
		return e
	}
	return serverNode.listenOrElse("SendContract", req, reply,
		func() error {
			return serverNode.DoSendContract(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoSendContract(req SendContractRequestInfo, reply *SendContractReplyInfo) error {
	if succ, err := serverNode.fs.Write("Contract/"+req.Addr+"/"+req.Id, &FileContentInfo{req.FilePath}, nil); err != nil {
		reply.Succ = succ
		return err
	} else {
		reply.Succ = succ
		return nil
	}
}

func (serverNode *ServerNode) DeleteContract(req DeleteContractRequestInfo, reply *DeleteContractReplyInfo) error {
	return serverNode.listenOrElse("DeleteContract", req, reply,
		func() error {
			return serverNode.DoDeleteContract(req, reply)
		},
	)
}

func (serverNode *ServerNode) DoDeleteContract(req DeleteContractRequestInfo, reply *DeleteContractReplyInfo) error {
	if req.Realpath == "" {
		req.Realpath = "Contract/" + req.Addr + "/" + req.Id
	}
	succ, err := serverNode.fs.Delete(req.Realpath, false)
	reply.Succ = succ
	return err
}

func (serverNode *ServerNode) fileExist(fileName string) (bool, error) {
	return serverNode.fs.FileExists(fileName)
}

func (serverNode *ServerNode) create(
	req GetHandleRequestInfo,
	res *GetHandleReplyInfo,
) (bool, error) {
	log.Printf("creating file %s \n", req.FilePath)
	exist, err := serverNode.fileExist(req.FilePath)
	if err != nil {
		return false, err
	}
	path := filepath.Dir(req.FilePath)
	for path != "/" && path != "." {
		log.Println(path)
		exist, err := serverNode.fileExist(path)
		if err != nil {
			return false, err
		}
		if exist {
			res.Err = fmt.Sprintf("try to create %s, but its parent %s is a file", req.FilePath, path)
			log.Println(res.Err)
			res.ErrType = FilePathConflict
			res.Succ = false
			return false, nil
		}
		path = filepath.Dir(path)
	}

	if exist {
		res.Succ = false
		res.Err = "file already exist"
		res.ErrType = FileExist
		return false, nil
	}

	// only initiate content when creating file
	if req.InitialACL.ACLChanger == nil {
		req.InitialACL.ACLChanger = make([]string, 0)
	}
	if req.InitialACL.Reader == nil {
		req.InitialACL.Reader = make([]string, 0)
	}
	if req.InitialACL.Writer == nil {
		req.InitialACL.Writer = make([]string, 0)
	}
	return serverNode.fs.Write(
		req.FilePath,
		&FileContentInfo{req.InitialContent},
		&req.InitialACL,
	)
}
