package servernode

import (
	"store"
	"sync"
)

// FileSystemT A wrapper on store.StorageT for now. The purpose of this abstraction
// is to make the DB swappable
type FileSystemT interface {
	Read(filepath string) (*FileInfo, error)
	Write(filepath string, content *FileContentInfo, acl *FileACLInfo) (bool, error)
	CheckACL(filepath string, who string, mode string) (bool, error)
	ListFiles(directory string) ([]string, error)
	Delete(filepath string, isDir bool) (bool, error)
	Acquire(filepath string, who string, mode string, retries int) (bool, error)
	Release(filepath string, who string, mode string) (bool, error)
	GetLockMode(filepath string) (string, error)
	FileExists(filepath string) (bool, error)
	FileSystemSessionManagerT
}

type FileSystemSessionManagerT interface {
	// get a list of file names some client has opened
	listHandles(clientAddr string) ([]string, error)
	// append a file name to the file name list of some client
	addHandle(clientAddr string, filePath string) error
	// delete a file name from the file name list of some client
	removeHandle(clientAddr string, filePath string) error
}

type fileSystem struct {
	storage store.StorageT
	m       sync.Mutex
	conds   map[string]*sync.Cond
}

var (
	_ FileSystemT               = &fileSystem{}
	_ FileSystemSessionManagerT = &fileSystem{}
)

// NewFS create a FileSystemT wrapper on default DB
func NewFS(storeAddr string) FileSystemT {
	fs := &fileSystem{
		storage: store.NewStoreRPCclient(storeAddr),
		m:       sync.Mutex{},
		conds:   make(map[string]*sync.Cond),
	}
	return fs
}
