package servernode_test

import (
	"log"
	"randomaddr"
	"servernode"
	"store"
	"testing"
	"testp"
	"time"
)

func TestWriteAndRead(t *testing.T) {
	addr := randomaddr.RandomLocalAddr()
	addr2 := randomaddr.RandomLocalAddr()
	for addr == addr2 {
		addr2 = randomaddr.RandomLocalAddr()
	}

	st := store.NewStorage()
	ready := make(chan bool)
	go store.ServeStoreRPC(addr2, st, ready)
	<-ready
	fs := servernode.NewFS(addr2)

	succ, werr := fs.Write(
		"test_write",
		&servernode.FileContentInfo{
			Content: "this is a test",
		},
		&servernode.FileACLInfo{
			ACLChanger: []string{addr},
			Reader:     []string{addr},
			Writer:     []string{addr},
		},
	)
	testp.As(succ)
	testp.NotError(werr)
	info, rerr := fs.Read("test_write")
	testp.NotError(rerr)
	testp.As(info != nil)
	log.Printf("meta: 	 %+v\n", info.Meta)
	log.Printf("content: %+v\n", info.Content)
	testp.As(info.Content.Content == "this is a test")
}

func TestCheckACL(t *testing.T) {
	addr1 := randomaddr.RandomLocalAddr()
	addr2 := randomaddr.RandomLocalAddr()
	for addr1 == addr2 {
		addr2 = randomaddr.RandomLocalAddr()
	}
	addr3 := randomaddr.RandomLocalAddr()
	for addr3 == addr1 || addr3 == addr2 {
		addr3 = randomaddr.RandomLocalAddr()
	}
	st := store.NewStorage()
	ready := make(chan bool)
	go store.ServeStoreRPC(addr3, st, ready)
	<-ready
	fs := servernode.NewFS(addr3)

	succ, err := fs.Write("test_acl",
		&servernode.FileContentInfo{"nothing"},
		&servernode.FileACLInfo{
			Writer:     []string{addr1},
			Reader:     []string{addr1},
			ACLChanger: []string{addr1},
		},
	)
	testp.As(succ)
	testp.NotError(err)

	succ, err = fs.CheckACL("test_acl", addr1, "W")
	testp.As(succ)
	testp.NotError(err)
	succ, err = fs.CheckACL("test_acl", addr1, "R")
	testp.As(succ)
	testp.NotError(err)
	succ, err = fs.CheckACL("test_acl", addr1, "ACL")
	testp.As(succ)
	testp.NotError(err)

	succ, err = fs.CheckACL("test_acl", addr2, "W")
	testp.As(!succ)
	testp.NotError(err)
	succ, err = fs.CheckACL("test_acl", addr2, "R")
	testp.As(!succ)
	testp.NotError(err)
	succ, err = fs.CheckACL("test_acl", addr2, "ACL")
	testp.As(!succ)
	testp.NotError(err)

}

func TestAcquire(t *testing.T) {
	addr1 := randomaddr.RandomLocalAddr()
	addr2 := randomaddr.RandomLocalAddr()
	for addr1 == addr2 {
		addr2 = randomaddr.RandomLocalAddr()
	}
	addr3 := randomaddr.RandomLocalAddr()
	for addr3 == addr1 || addr3 == addr2 {
		addr3 = randomaddr.RandomLocalAddr()
	}
	st := store.NewStorage()
	ready := make(chan bool)
	go store.ServeStoreRPC(addr3, st, ready)
	<-ready
	fs := servernode.NewFS(addr3)
	succ, err := fs.Write("test_lock", nil, &servernode.FileACLInfo{
		Writer:     []string{addr1, addr2},
		Reader:     []string{addr1, addr2},
		ACLChanger: []string{addr1, addr2},
	})
	testp.As(succ)
	testp.NotError(err)

	// succ, err = filesystem.Acquire("test_lock", addr1, "W")
	// test_primitives.As(succ)
	// test_primitives.NotError(err)
	// mode, e := filesystem.GetLockMode("test_lock")
	// test_primitives.As(mode == "W")
	// test_primitives.NotError(e)
	// succ, err = filesystem.Release("test_lock", addr1, "W")
	// test_primitives.As(succ)
	// test_primitives.NotError(err)

	res := 1

	muls := func(times int) {
		for i := 0; i < times; i++ {
			res *= 7
		}
	}

	subs := func(times int) {
		for i := 0; i < times; i++ {
			res -= 1
		}
	}

	done := make(chan bool)

	go func() {
		s, e := fs.Acquire("test_lock", addr1, "W", 10)
		testp.NotError(e)
		testp.As(s)
		muls(3)
		s, e = fs.Release("test_lock", addr1, "W")
		testp.As(s)
		testp.NotError(e)
		done <- true
	}()

	go func() {
		s, e := fs.Acquire("test_lock", addr2, "W", 10)
		testp.NotError(e)
		testp.As(s)
		subs(3)
		s, e = fs.Release("test_lock", addr2, "W")
		testp.As(s)
		testp.NotError(e)
		done <- true
	}()

	for i := 0; i < 2; i++ {
		<-done
	}
	log.Println(res)

	testp.As(res == (-2)*7*7*7 || res == 7*7*7-3)

}

func TestTryAcquire(t *testing.T) {
	addr1 := randomaddr.RandomLocalAddr()
	addr2 := randomaddr.RandomLocalAddr()
	for addr1 == addr2 {
		addr2 = randomaddr.RandomLocalAddr()
	}
	addr3 := randomaddr.RandomLocalAddr()
	for addr3 == addr1 || addr3 == addr2 {
		addr3 = randomaddr.RandomLocalAddr()
	}
	st := store.NewStorage()
	ready := make(chan bool)
	go store.ServeStoreRPC(addr3, st, ready)
	<-ready
	fs := servernode.NewFS(addr3)
	succ, err := fs.Write("test_lock", nil, &servernode.FileACLInfo{
		Writer:     []string{addr1, addr2},
		Reader:     []string{addr1, addr2},
		ACLChanger: []string{addr1, addr2},
	})
	testp.As(succ)
	testp.NotError(err)

	res := 1

	muls := func(times int) {
		for i := 0; i < times; i++ {
			res *= 2
			time.Sleep(200 * time.Millisecond)
		}
	}

	subs := func(times int) {
		for i := 0; i < times; i++ {
			res -= 1
			time.Sleep(200 * time.Millisecond)
		}
	}

	done := make(chan bool)

	go func() {
		log.Printf("%s start", addr1)
		s, e := fs.Acquire("test_try_acquire", addr1, "W", 0) // try acquire
		log.Printf("%s acquire %+v", addr1, s)
		testp.NotError(e)
		if !s {
			done <- true
			return
		}
		muls(10)
		s, e = fs.Release("test_try_acquire", addr1, "W")
		log.Printf("%s release %+v", addr1, s)
		testp.As(s)
		testp.NotError(e)
		done <- true
	}()

	go func() {
		log.Printf("%s start", addr2)
		s, e := fs.Acquire("test_try_acquire", addr2, "W", 0) // try acquire
		log.Printf("%s acquire %+v", addr2, s)
		testp.NotError(e)
		if !s {
			done <- true
			return
		}
		subs(10)
		s, e = fs.Release("test_try_acquire", addr2, "W")
		log.Printf("%s release %+v", addr2, s)
		testp.As(s)
		testp.NotError(e)
		done <- true
	}()

	for i := 0; i < 2; i++ {
		<-done
	}

	log.Printf("res is %d", res)

	testp.As(res == 1024 || res == -9)

}
