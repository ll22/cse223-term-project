package servernode

import (
	"events"
	"sync"
)

// SessionChans a struct storing 2 channels.
// When close() is called, and the last file handle is closed, then it writes to SessionCloseChan
// which causes the corresponding keep alive session to end.
// When KeepLive() is called, it write to the TimerCloseChan which causes the corresponding timer
// session to end. (Timer session is defined to be the interval after a KeepLive returns and before
// the next KeepLive starts. After one KeepLive returns, the Chubby client has to issue another
// KeepLive to the server within some certain amount of time. Otherwise the session would be considered
// to be timed out by the server).

const (
	LeasePeriod          = 12
	LeaseSecBeforeExpire = 1
	ClientTimeout        = 6
)

type SessionChans struct {
	sessionClosed      chan bool
	sessionClosedValid bool
	timerClosed        chan bool
	timerClosedValid   bool
	events             chan events.Event
	eventsValid        bool
	deprecated 		   chan bool
	m                  *sync.Mutex
}

func NewSessionChans() *SessionChans {
	sessionChans := &SessionChans{
		timerClosed:        make(chan bool, 1),
		sessionClosed:      make(chan bool, 1),
		events:             make(chan events.Event, 1),
		timerClosedValid:   true,
		sessionClosedValid: true,
		eventsValid:        true,
		deprecated: 		make(chan bool, 1),
		m:                  &sync.Mutex{},
	}
	return sessionChans
}

func (session *SessionChans) Close() {
	session.m.Lock()
	defer session.m.Unlock()
	if session.sessionClosedValid {
		session.sessionClosed <- true
	}
}

func (session *SessionChans) IsClose() chan bool {
	return session.sessionClosed
}

func (session *SessionChans) StopTimer() {
	session.m.Lock()
	defer session.m.Unlock()
	if session.timerClosedValid {
		session.timerClosed <- true
	}
}

func (session *SessionChans) IsTimerStopped() chan bool {
	return session.timerClosed
}

func (session *SessionChans) Emit(e events.Event) {
	session.m.Lock()
	defer session.m.Unlock()
	if session.eventsValid {
		session.events <- e
	}
}

func (session *SessionChans) GetEvent() chan events.Event {
	return session.events
}

func (session *SessionChans) invalidate() {
	session.m.Lock()
	defer session.m.Unlock()
	session.sessionClosedValid = false
	session.eventsValid = false
	session.timerClosedValid = false
}
