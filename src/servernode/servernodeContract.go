package servernode

import (
	"io/ioutil"
	"log"
	"logentry"
	"os"
)

func (serverNode *ServerNode) handleContract(sxnClientAddr string) error {

	//log.Printf("start handling client contract!")
	gfsTestLogPath := "../clientlib/gfs/test_log/"

	allContracts, err := serverNode.fs.ListFiles("Contract/" + sxnClientAddr)
	if err != nil {
		// TODO: not sure if this should be done?
		log.Printf("finding contracts for %s from DB failed", sxnClientAddr)

		// if can't find contract in its DB, it means client might crash before send the contract here
		// go to GFS and search for logs
		files, _ := ioutil.ReadDir(gfsTestLogPath + sxnClientAddr)
		for _, f := range files {
			allContracts = append(allContracts, gfsTestLogPath+sxnClientAddr+"/"+f.Name())
		}
	}

	//log.Printf("allContracts is %v", allContracts)

	for _, contractpath := range allContracts {

		// try to find contracts information in DB
		// if client has registered contracts, it will find it
		if _, e := serverNode.fs.Read(contractpath); e != nil {
			// TODO: are we gonna do anything here????
			log.Printf("Read all contracts for % failed", sxnClientAddr)
			return e
		} else {
			if e := serverNode.finishContract(contractpath, sxnClientAddr); e != nil {
				log.Printf("failed to finish contract: %s", e.Error())
			} else {
				req := DeleteContractRequestInfo{Addr: "", Id: "", Realpath: contractpath}
				reply := &DeleteContractReplyInfo{}
				if e := serverNode.DeleteContract(req, reply); e != nil {
					log.Printf("failed to delete contract: %s", e.Error())
				}
				log.Printf("finish contract %s", contractpath)
			}
		}
	}
	return nil
}

func (serverNode *ServerNode) finishContract(path string, clientAddr string) error {
	var e error
	var file *os.File
	//Do work
	if e = logentry.CheckFileLegal(path); e != nil {
		return e
	}
	file, e = os.OpenFile(path, os.O_RDWR, os.ModePerm)
	if e != nil {
		return e
	}
	var logitem *logentry.LogEntry
	for {
		logitem, e = logentry.ReadNextLogEntry(file)
		if e == logentry.EOF {
			break
		}
		if logitem.IsDone == true {
			continue
		}
		request := SetContentsRequestInfo{
			Content:   logitem.WriteContent,
			FilePath:  logitem.FileName,
			Requestor: clientAddr,
		}
		reply := SetContentsReplyInfo{
			Succ: false,
		}

		log.Printf("In finishContent(), SetContents sets filepath %s to be %s ------", request.FilePath, request.Content)

		if err := serverNode.SetContents(request, &reply); err != nil {
			return err
		}
		logentry.SetLastDone(file)
	}
	//delete log file
	log.Printf("Remove files from %s", path)
	e = os.Remove(path)
	return e
}
