package servernode

import (
	"log"
	"net"
	"net/rpc"
	"util"
)

// ServerRPCClient the RPC client used to connect to chubby server
type ServerRPCClient struct {
	*util.PersistentRPC
}

// NewServerRPCClient Creates an RPC client that connects to addr.
func NewServerRPCClient(addr string) ServerNodeT {
	c := &ServerRPCClient{util.NewPersistentRPC(addr)}
	return c
}

// ServeServerRPC Serve as a rpc server
func ServeServerRPC(addr string, s ServerNodeT, ready chan bool) error {
	server := rpc.NewServer()
	server.RegisterName("ServerNodeT", s)

	ln, err := net.Listen("tcp", addr)
	if err != nil {
		util.FeedReady(ready, false)
		return err
	}
	defer ln.Close()
	util.FeedReady(ready, true)

	conns := []net.Conn{}

	for {
		conn, err := ln.Accept()
		if err != nil {
			log.Printf("The server node %s is dead", addr)
			for _, c := range conns {
				c.Close()
			}
			return err
		}
		conns = append(conns, conn)
		go server.ServeConn(conn)
	}
}

// RPC client

// GetHandle get a file handle for a file
func (c *ServerRPCClient) GetHandle(req GetHandleRequestInfo, res *GetHandleReplyInfo) error {
	return c.TryCall("ServerNodeT.GetHandle", req, res)
}

// ListAllFiles list all the files in a server
func (c *ServerRPCClient) ListAllFiles(dummyReq string, reply *ListReply) error {
	return c.TryCall("ServerNodeT.ListAllFiles", dummyReq, reply)
}

func (c *ServerRPCClient) Delete(req DeleteRequestInfo, reply *DeleteReplyInfo) error {
	return c.TryCall("ServerNodeT.Delete", req, reply)
}

func (c *ServerRPCClient) Acquire(req AcquireRequestInfo, reply *AcquireReplyInfo) error {
	return c.TryCall("ServerNodeT.Acquire", req, reply)
}

func (c *ServerRPCClient) TryAcquire(req TryAcquireRequestInfo, reply *TryAcquireReplyInfo) error {
	return c.TryCall("ServerNodeT.TryAcquire", req, reply)
}

func (c *ServerRPCClient) Release(req ReleaseRequestInfo, reply *ReleaseReplyInfo) error {
	return c.TryCall("ServerNodeT.Release", req, reply)
}

func (c *ServerRPCClient) GetContentsAndStat(req GetContentsAndStatRequestInfo, reply *GetContentsAndStatReplyInfo) error {
	return c.TryCall("ServerNodeT.GetContentsAndStat", req, reply)
}

func (c *ServerRPCClient) GetStat(req GetStatRequestInfo, reply *GetStatReplyInfo) error {
	return c.TryCall("ServerNodeT.GetStat", req, reply)
}

func (c *ServerRPCClient) SetContents(req SetContentsRequestInfo, reply *SetContentsReplyInfo) error {
	return c.TryCall("ServerNodeT.SetContents", req, reply)
}

func (c *ServerRPCClient) SetACL(req SetACLRequestInfo, reply *SetACLReplyInfo) error {
	return c.TryCall("ServerNodeT.SetACL", req, reply)
}

func (c *ServerRPCClient) CheckSequencer(req CheckSequencerRequestInfo, reply *CheckSequencerReplyInfo) error {
	return c.TryCall("ServerNodeT.CheckSequencer", req, reply)
}

func (c *ServerRPCClient) GetSequencer(req GetSequencerRequestInfo, reply *GetSequencerReplyInfo) error {
	return c.TryCall("ServerNodeT.GetSequencer", req, reply)
}

func (c *ServerRPCClient) ReturnHandle(req ReturnHandleRequestInfo, reply *ReturnHandleReplyInfo) error {
	return c.TryCall("ServerNodeT.ReturnHandle", req, reply)
}

func (c *ServerRPCClient) KeepLive(req KeepLiveRequestInfo, reply *KeepLiveReplyInfo) error {
	return c.TryCall("ServerNodeT.KeepLive", req, reply)
}

func (c *ServerRPCClient) SendContract(req SendContractRequestInfo, reply *SendContractReplyInfo) error {
	return c.TryCall("ServerNodeT.SendContract", req, reply)
}

func (c *ServerRPCClient) DeleteContract(req DeleteContractRequestInfo, reply *DeleteContractReplyInfo) error {
	return c.TryCall("ServerNodeT.DeleteContract", req, reply)
}

func (c *ServerRPCClient) TryMultiAcquire(req TryMultiAcquireRequestInfo, reply *TryMultiAcquireReplyInfo) error {
	return c.TryCall("ServerNodeT.TryMultiAcquire", req, reply)
}

func (c *ServerRPCClient) TryMultiRelease(req TryMultiReleaseRequestInfo, reply *TryMultiReleaseReplyInfo) error {
	return c.TryCall("ServerNodeT.TryMultiRelease", req, reply)
}

func (c *ServerRPCClient) OpenSession(req OpenSessionRequestInfo, res *OpenSessionReplyInfo) error {
	return c.TryCall("ServerNodeT.OpenSession", req, res)
}

func (c *ServerRPCClient) CloseSession(req CloseSessionRequestInfo, res *CloseSessionReplyInfo) error {
	return c.TryCall("ServerNodeT.CloseSession", req, res)
}
