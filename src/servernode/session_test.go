package servernode_test

import (
	"clientlib"
	"events"
	"log"
	"math/rand"
	//"runtime/debug"
	"servernode"
	"strconv"
	"testing"
	"testp"
	"time"
	//"os"
)

func TestCreatAndDestroySession(t *testing.T) {

	// ******************************************************
	// System bootstrap
	// ******************************************************
	var ready chan bool
	var serverAddr string
	var serverStoreAddr string
	existingAddresses := make([]string, 0)

	ready = make(chan bool)
	// start server, server store and DNS server
	log.Printf("Trying to bootstrap the system...\n")
	serverAddr, serverStoreAddr = testp.ServerDnsBootstrap(ready)
	existingAddresses = append(existingAddresses, serverAddr)
	existingAddresses = append(existingAddresses, serverStoreAddr)

	// ******************************************************
	// Test starts
	// ******************************************************

	//oneHandleForEachSession(existingAddresses, ready, 20, 10)

	//oneSxnMulHandles(existingAddresses, 5, 3)

	multipleSessionMultipleHandle(existingAddresses, 5, 250, 5)
}

func multipleSessionMultipleHandle(existingAddresses []string, clientNum int, openFileNum int, maxClientOpenPeriod int) {
	var done chan bool
	var clientAddr string
	done = make(chan bool, 0)

	startClientCount := clientNum
	for startClientCount > 0 {
		startClientCount--
		clientAddr = testp.GenerateClientAddr(existingAddresses)
		existingAddresses = append(existingAddresses, clientAddr)
		go startOneSxnMulHandles(clientAddr, openFileNum, maxClientOpenPeriod, done)
	}

	finishCount := 0
	for clientNum > finishCount {
		finishCount++
		<-done
		log.Printf(strconv.Itoa(finishCount) + " done! \n")
	}
}

func oneSxnMulHandles(existingAddresses []string, openFileNum int, maxClientOpenPeriod int) {
	var clientAddr string

	clientAddr = testp.GenerateClientAddr(existingAddresses)
	// close the opened files one by one
	startOneSxnMulHandles(clientAddr, openFileNum, maxClientOpenPeriod, nil)
}

func startOneSxnMulHandles(clientAddr string, openFileNum int, maxClientOpenPeriod int, done chan bool) {

	var defaultTestFilePath string
	var fileCount int
	var testFilePath string
	var err error
	var clientOpenPeriod int
	var randIndex int
	var initialContent string
	fileCount = 1
	defaultTestFilePath = "/ls/foo/wombat/pouch"
	initialContent = "blah blah blah 1"
	// start a Chubby client
	e, client := clientlib.NewChubbyClient(clientAddr)
	testp.NotError(e)
	log.Printf("Chubby client started at: %s: ", clientAddr)

	// open a bunch of files
	openCount := openFileNum
	openedFiles := make([]string, 0)
	for openCount > 0 {
		openCount--

		testFilePath = testp.NewFileName(defaultTestFilePath, fileCount)
		testFilePath += clientAddr
		fileCount++
		openedFiles = append(openedFiles, testFilePath)

		err, _ = client.Open(
			testFilePath,
			servernode.WriteHandle,
			events.EventFileContentModified,
			true,
			initialContent,
			servernode.FileACLInfo{
				Writer:     []string{clientAddr},
				Reader:     []string{clientAddr},
				ACLChanger: []string{clientAddr},
			},
			false,
		)
		testp.NotError(err)
		log.Printf("Client opens file %s succeed!\n", testFilePath)
	}

	//time.Sleep(time.Duration(10) * time.Second)

	openCount = len(openedFiles)
	for openCount != 0 {
		randIndex = randomInt(openCount)

		log.Printf("Client trying to close file : %s\n", openedFiles[randIndex])
		printDelim()
		err = client.Close(openedFiles[randIndex])
		testp.NotError(err)
		log.Printf("Client closes file %s succeed!\n", openedFiles[randIndex])
		// remove file name from the open-file list
		openedFiles = append(openedFiles[:randIndex], openedFiles[randIndex+1:]...)
		openCount = len(openedFiles)
		// short delay
		clientOpenPeriod = randomInt(maxClientOpenPeriod)
		log.Printf("Client delays %d\n", clientOpenPeriod)
		time.Sleep(time.Duration(clientOpenPeriod) * time.Second)
	}
	log.Printf("All files closed\n")
	if done != nil {
		done <- true
	}
}

// start multiple clients and each of them gets a handle from the server
// and waits arbitrary period of time shorter than maxClientOpenPeriod
// and then returns the handle
func oneHandleForEachSession(existingAddresses []string,
	ready chan bool,
	clientNum int,
	maxClientOpenPeriod int) {

	var err error
	var clientOpenPeriod int
	var defaultTestFilePath string
	var fileCount int
	var testFilePath string
	var initialContent string
	var clientAddr string

	defaultTestFilePath = "/ls/foo/wombat/pouch"
	fileCount = 1
	initialContent = "blah blah blah 1"

	startedTestCount := 0
	for startedTestCount < clientNum {

		// generate an addr for client
		clientAddr = testp.GenerateClientAddr(existingAddresses)
		existingAddresses = append(existingAddresses, clientAddr)
		// start a Chubby client
		e, client := clientlib.NewChubbyClient(clientAddr)
		testp.NotError(e)
		log.Printf("Chubby client started at: %s: ", clientAddr)
		testFilePath = testp.NewFileName(defaultTestFilePath, fileCount)

		go func() {
			printDelim()
			err, _ = client.Open(
				testFilePath,
				servernode.WriteHandle,
				events.EventFileContentModified,
				true,
				initialContent,
				servernode.FileACLInfo{
					Writer:     []string{clientAddr},
					Reader:     []string{clientAddr},
					ACLChanger: []string{clientAddr},
				},
				false,
			)
			testp.NotError(err)
			clientOpenPeriod = randomInt(maxClientOpenPeriod)
			log.Printf("Client opens file %s succeed!\n", testFilePath)

			log.Printf("Client delays %d\n", clientOpenPeriod)
			time.Sleep(time.Duration(clientOpenPeriod) * time.Second)

			log.Printf("Client trying to close file : %s\n", testFilePath)
			printDelim()
			err = client.Close(testFilePath)
			testp.NotError(err)
			ready <- true
		}()

		startedTestCount++
		fileCount++
	}

	finishCount := 0
	for finishCount != clientNum {
		<-ready
		log.Printf(strconv.Itoa(finishCount) + " done! \n")
		finishCount++
	}
}

func randomInt(max int) int {
	rand.Seed(time.Now().Unix())
	return rand.Intn(max)
}

func printDelim() {
	log.Printf("**************\n\n")
}
