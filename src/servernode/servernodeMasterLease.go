package servernode

import (
	"encoding/json"
	"time"
)

type MasterLease struct {
	MasterID     int
	NowLeaseTime time.Time
}

type PaxosableT struct {
	ServerNodeT
	PaxosMasterInfoT
}

var defaultMasterLease = MasterLease{
	MasterID:     -1,
	NowLeaseTime: time.Now(),
}

func (lease MasterLease) String() string {
	bytes, err := json.Marshal(lease)
	if err != nil {
		panic("should not happen, " + err.Error())
	}
	return string(bytes)
}

func MasterLeaseFromString(s string) *MasterLease {
	lease := &MasterLease{}
	if err := json.Unmarshal([]byte(s), lease); err != nil {
		panic("should not happen, " + err.Error())
	}
	return lease
}

type PaxosMasterInfoT interface {
	SetMasterInfo(MasterLease) error
	GetMasterInfo() (*MasterLease, error)
}

func (serverNode *ServerNode) SetMasterInfo(lease MasterLease) error {
	_, err := serverNode.fs.Write(serverNode.addr+":master-lease", &FileContentInfo{
		Content: lease.String(),
	}, nil)
	return err
}

func (serverNode *ServerNode) GetMasterInfo() (*MasterLease, error) {
	fileinfo, err := serverNode.fs.Read(serverNode.addr + ":master-lease")
	if err != nil {
		return nil, err
	}

	if fileinfo == nil {
		return &defaultMasterLease, nil
	}

	fileContent := fileinfo.Content.Content
	if len(fileContent) == 0 {
		return &defaultMasterLease, nil
	}

	return MasterLeaseFromString(fileContent), nil
}
