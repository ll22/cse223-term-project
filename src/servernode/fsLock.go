package servernode

import (
	"errors"
	"fmt"
	"log"
	"store"
	"sync"
)

/*
func isLockInfosConsistent(infos []*FileLockInfo) error {
	if len(infos) <= 1 {
		return nil
	}
	w, r := false, false
	exist := make(map[string]bool)
	for _, i := range infos {
		if exist[i.Who] {
			return fmt.Errorf("infos inconsistent: lock %s already exist", i.Who)
		}
		exist[i.Who] = true
		switch i.Mode {
		case "R":
			if w {
				return errors.New("infos inconsistent: has read lock in write locks")
			}
			r = true
		case "W":
			if r {
				return errors.New("infos inconsistent: has write lock in read locks")
			}
			w = true
		default:
			return errors.New("infos inconsistent: unknown lock mode" + i.Mode)
		}
	}
	return nil
}
*/

func isLockInfosConsistentWith(infos []*FileLockInfo, lockInfo FileLockInfo) error {
	if len(infos) <= 1 {
		return nil
	}
	exist := make(map[string]bool)
	for _, i := range infos {
		if exist[i.Who] {
			return fmt.Errorf("infos inconsistent with %s: duplicate locks", i.Who)
		}
		exist[i.Who] = true
		if i.Mode != lockInfo.Mode {
			return fmt.Errorf("infos inconsistent with %s %s: mode conflict", lockInfo.Who, lockInfo.Mode)
		}
		if i.Who == lockInfo.Who {
			return fmt.Errorf("infos inconsistent with %s: already exist", lockInfo.Who)
		}
	}
	return nil
}

func (fs *fileSystem) dbWLock(
	infos []*FileLockInfo,
	filepath string,
	lockInfo FileLockInfo,
) (bool, error) {
	if lockInfo.Mode != "W" {
		return false, errors.New("don't acquire a write lock with read mode")
	}
	if len(infos) >= 1 {
		return false, nil
	}
	succ := false
	err := fs.storage.ListAppend(store.KV(filepath, lockInfo.String()), &succ)
	return succ, err
}

func (fs *fileSystem) dbRLock(
	infos []*FileLockInfo,
	filepath string,
	lockInfo FileLockInfo,
) (bool, error) {
	if lockInfo.Mode != "R" {
		return false, errors.New("don't acquire a read lock with write mode")
	}
	if err := isLockInfosConsistentWith(infos, lockInfo); err != nil {
		return false, err
	}
	succ := false
	e := fs.storage.ListAppend(store.KV(filepath, lockInfo.String()), &succ)
	return succ, e
}

func (fs *fileSystem) dbWUnlock(
	infos []*FileLockInfo,
	filepath string,
	lockInfo FileLockInfo,
) (bool, error) {
	if lockInfo.Mode != "W" {
		return false, errors.New("don't release a write lock with read mode")
	}
	if len(infos) == 0 {
		return false, errors.New("no write lock to release")
	}
	if len(infos) > 1 {
		return false, errors.New("try to release a write lock within many locks")
	}
	if infos[0].Who != lockInfo.Who {
		return false, fmt.Errorf("%s try to release a write lock belong to %s", lockInfo.Who, infos[0].Who)
	}
	if infos[0].Mode != lockInfo.Mode {
		return false, fmt.Errorf("try to release a write lock, but the lock is %s mode", infos[0].Mode)
	}
	n := 0
	if err := fs.storage.ListRemove(store.KV(filepath, lockInfo.String()), &n); err != nil {
		return false, err
	}
	if n != 1 {
		panic("should not happen: impossible to remove multiple locks")
	}
	return true, nil
}

func (fs *fileSystem) dbRUnlock(
	infos []*FileLockInfo,
	filepath string,
	lockInfo FileLockInfo,
) (bool, error) {
	if lockInfo.Mode != "R" {
		return false, errors.New("don't release a read lock with write mode")
	}
	if len(infos) == 0 {
		return false, errors.New("no read lock to release")
	}

	if err := isLockInfosConsistentWith(infos, lockInfo); err != nil {
		return false, err
	}

	n := 0
	if err := fs.storage.ListRemove(store.KV(filepath, lockInfo.String()), &n); err != nil {
		return false, err
	}

	return true, nil
}

func stringsToLockInfos(ss []string) []*FileLockInfo {
	infos := make([]*FileLockInfo, 0)
	for _, s := range ss {
		infos = append(infos, FileLockInfoFromString(s))
	}
	return infos
}

// Acquire acquire a lock. A lock could be acquired by the same acquirer
// with the same mode successfully.
func (fs *fileSystem) Acquire(
	filepath string,
	who string,
	mode string,
	retries int,
) (bool, error) {
	log.Println("lock acquire", filepath, "who:", who, "mode: ", mode)
	fs.m.Lock()
	if _, ok := fs.conds[filepath]; !ok {
		fs.conds[filepath] = sync.NewCond(&sync.Mutex{})
	}
	fs.m.Unlock()

	var lockfunc func(*store.KeyValue, *bool) error
	if mode == "W" {
		lockfunc = fs.storage.WLock
	} else if mode == "R" {
		lockfunc = fs.storage.RLock
	} else {
		return false, fmt.Errorf("unknow lock mode %s", mode)
	}
	i := 0

	fs.conds[filepath].L.Lock()
	for {
		succ := false
		err := lockfunc(store.KV(filepath, who), &succ)
		log.Printf("Acquire %+v err: %+v", succ, err)
		if err != nil {
			panic(err)
		}
		// not succeed but no error means waiting
		if err == nil && !succ {
			if i < retries {
				fs.conds[filepath].Wait()
			} else {
				break
			}
		} else if err != nil {
			fs.conds[filepath].L.Unlock()
			return false, err

		} else if succ {
			fs.conds[filepath].L.Unlock()
			return succ, nil
		}
		i++
	}
	fs.conds[filepath].L.Unlock()
	log.Println("lock attempts exceed limit")

	return false, nil
}

func (fs *fileSystem) Release(filepath string, who string, mode string) (bool, error) {
	fs.m.Lock()
	if _, ok := fs.conds[filepath]; !ok {
		fs.m.Unlock()
		return false, fmt.Errorf("file %s doesn't have lock", filepath)
	}
	fs.m.Unlock()

	succ := false
	lockby := store.KV(filepath, who)
	var err error

	if mode == "W" {
		err = fs.storage.WUnlock(lockby, &succ)
	} else if mode == "R" {
		err = fs.storage.RUnlock(lockby, &succ)
	} else {
		return false, fmt.Errorf("unknow lock mode %s", mode)
	}

	if err != nil {
		return false, err
	}
	fs.conds[filepath].Broadcast()
	return true, nil

}

func (fs *fileSystem) GetLockMode(filepath string) (string, error) {
	mode := ""
	err := fs.storage.LockMode(filepath, &mode)
	return mode, err
}
