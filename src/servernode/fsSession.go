package servernode

import (
	"errors"
	"log"
	"store"
	"strings"
)

const (
	sessionPrefix string = "session::"
)

func (fs *fileSystem) listHandles(clientAddr string) ([]string, error) {

	var err error
	if err = isValidClientAddr(clientAddr); err != nil {
		return nil, err
	}
	key := sessionPrefix + clientAddr
	var tempList = new(store.List)
	if err = fs.storage.ListGet(key, tempList); err != nil {
		log.Printf("storage.ListGet() failed: %s", err.Error())
		return nil, err
	}
	return tempList.L, nil
}

func (fs *fileSystem) addHandle(clientAddr string, filePath string) error {

	var err error
	if err = isValidClientAddr(clientAddr); err != nil {
		return err
	}
	key := sessionPrefix + clientAddr
	succ := false
	if err = fs.storage.ListAppend(store.KV(key, filePath), &succ); err != nil {
		log.Printf("storage.ListGet() failed: %s", err.Error())
		return err
	}
	return nil
}

// delete a file name from the file name list of some client
func (fs *fileSystem) removeHandle(clientAddr string, filePath string) error {

	var err error
	if err = isValidClientAddr(clientAddr); err != nil {
		return err
	}
	key := sessionPrefix + clientAddr
	var n int
	if err = fs.storage.ListRemove(store.KV(key, filePath), &n); err != nil {
		log.Printf("storage.ListGet() failed: %s", err.Error())
		return err
	}

	if n != 1 {
		return errors.New("Should only have one file name being removed")
	}

	return nil

}

func isValidClientAddr(clientAddr string) error {
	// make sure client address is valid
	if !strings.Contains(clientAddr, ":") {
		return errors.New("Invalid client address!")
	}
	return nil
}
