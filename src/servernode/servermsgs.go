package servernode

import (
	"events"
)

const (
	NoError   = 0 << iota
	FileExist = 1
	FileNotExist
	FilePathConflict
	AccessDenied
)

type OpenSessionRequestInfo struct {
	Addr string
}

type OpenSessionReplyInfo struct {
	Succ bool
}

type CloseSessionRequestInfo struct {
	Addr string
}

type CloseSessionReplyInfo struct {
	Succ bool
}

type GetHandleRequestInfo struct {
	FilePath         string
	HandleType       string
	EventsSubscribed events.EventType
	CreateFile       bool
	InitialContent   string
	InitialACL       FileACLInfo
	IsDir            bool
	Addr             string
}

type GetHandleReplyInfo struct {
	Succ    bool
	Err     string
	ErrType int
}

type ReturnHandleRequestInfo struct {
	FilePath   string
	ClientAddr string
}

type ReturnHandleReplyInfo struct {
	Succ    bool
	Err     string
	ErrType int
}

type GetContentsAndStatRequestInfo struct {
	Requestor  string
	FilePath   string
	Generation uint64
}

type GetContentsAndStatReplyInfo struct {
	Content FileContentInfo
	Stat    FileMetaInfo
}

type GetStatRequestInfo struct {
	Requestor  string
	FilePath   string
	Generation uint64
}

type GetStatReplyInfo struct {
	Stat FileMetaInfo
}

type SetContentsRequestInfo struct {
	Requestor  string
	Content    string
	FilePath   string
	Generation uint64
}

type SetContentsReplyInfo struct {
	Succ bool
}

type SetACLRequestInfo struct {
	Addr string
	FilePath string
	ACL      FileACLInfo
}

type SetACLReplyInfo struct {
	Succ bool
}

type DeleteRequestInfo struct {
	Addr string
	FilePath string
	IsDir    bool
}

type DeleteReplyInfo struct {
	Succ bool
}

type AcquireRequestInfo struct {
	FilePath string
	Acquirer string
	Mode     string
	Retries  uint
}

type AcquireReplyInfo struct {
	Succ bool
}

type TryAcquireRequestInfo struct {
	FilePath string
	Acquirer string
	Mode     string
}

type TryAcquireReplyInfo struct {
	Succ bool
}

type TryMultiAcquireRequestInfo struct {
	FilePaths []string
	Acquirer  string
	Modes     []string
}

type TryMultiAcquireReplyInfo struct {
	Succ bool
}

type ReleaseRequestInfo struct {
	FilePath string
	Holder   string
	Mode     string
}

type ReleaseReplyInfo struct {
	Succ bool
}

type TryMultiReleaseRequestInfo struct {
	FilePaths []string
	Holder    string
	Modes     []string
}

type TryMultiReleaseReplyInfo struct {
	Succ bool
}

type SetSequencerRequestInfo struct {
	FilePath string
	Seq      Sequencer
}

type SetSequencerReplyInfo struct {
	Succ bool
}

type GetSequencerRequestInfo struct {
	FilePath string
}

type GetSequencerReplyInfo struct {
	Seq  Sequencer
	Succ bool
}

type CheckSequencerRequestInfo struct {
	FilePath string
	Seq      Sequencer
}

type CheckSequencerReplyInfo struct {
	Succ bool
}

type KeepLiveRequestInfo struct {
	ClientAddr string
	//EstablishSession bool
}

type KeepLiveReplyInfo struct {
	// if this message means to tell the client to terminate the current session (no)
	Terminate    bool
	Event        events.Event
	Invalidation bool
	// number of seconds the new lease is going to last in mini-second
	Lease int
}

type SendContractRequestInfo struct {
	Addr     string
	Id       string
	FilePath string
}

type SendContractReplyInfo struct {
	Succ bool
}

//Realpath = "Contract/" + Addr + "/" + Id
type DeleteContractRequestInfo struct {
	Addr     string
	Id       string
	Realpath string
}

type DeleteContractReplyInfo struct {
	Succ bool
}
