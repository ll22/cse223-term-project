package servernode

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"store"
	"strings"
)

type FsError struct {
	reason string
}

func (e FsError) Error() string {
	return e.reason
}

var (
	// FsNotDir file system error: the target is not a directory
	FsNotDir = FsError{reason: "the target is not a directory"}
	// FsNotFile file system error: the target is not a file
	FsNotFile = FsError{reason: "the target is not a file"}
)

func (fs *fileSystem) Read(filepath string) (*FileInfo, error) {

	value := ""
	if err := fs.storage.Get(filepath, &value); err != nil {
		log.Println(err)
		return nil, err
	}

	if len(value) == 0 {
		return nil, nil
	}
	fileInfo := FileInfo{}
	e := json.Unmarshal([]byte(value), &fileInfo)
	if e != nil {
		log.Println(e)
	}

	return &fileInfo, e
}

func (fs *fileSystem) Write(filepath string, content *FileContentInfo, acl *FileACLInfo) (bool, error) {
	fs.m.Lock() // make the get and set atomic
	defer fs.m.Unlock()

	//if content != nil {
	//	log.Printf("%#v\n\n", *content)
	//}
	//if acl != nil {
	//	log.Printf("%#v\n\n", *acl)
	//}

	orgFileInfo, err := fs.Read(filepath)
	log.Printf("%#v", orgFileInfo)
	if err != nil {
		log.Println(err.Error())
		return false, err
	}

	if orgFileInfo == nil {
		orgFileInfo = &FileInfo{
			ACL: FileACLInfo{
				ACLChanger: make([]string, 0),
				Reader:     make([]string, 0),
				Writer:     make([]string, 0),
			},
		}
		if content != nil {
			orgFileInfo.Content = *content
		}
		if acl != nil {
			orgFileInfo.ACL = *acl
		}
		log.Printf("make a new file info: %#v", *orgFileInfo)
	} else {
		if content != nil {
			orgFileInfo.Content = *content
			orgFileInfo.Meta.ContentGenerationNum++
		}
		if acl != nil {
			orgFileInfo.ACL = *acl
			orgFileInfo.Meta.ACLGenerationNum++
		}
	}
	bytes, err := json.Marshal(orgFileInfo)
	if err != nil {
		panic("should not happen")
	}

	kv := store.KV(filepath, string(bytes))

	succ := false
	err = fs.storage.Set(kv, &succ)
	return succ, err
}

func (fs *fileSystem) unsafeUpdateMeta(filepath string, meta FileMetaInfo) error {
	orgFileInfo, err := fs.Read(filepath)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	if orgFileInfo == nil {
		return fmt.Errorf("file %s is empty, could not set metadata", filepath)
	}
	if meta.IsNewer(orgFileInfo.Meta) {
		orgFileInfo.Meta = meta
		kv := store.KV(filepath, orgFileInfo.String())
		succ := false
		return fs.storage.Set(kv, &succ)
	}

	return fmt.Errorf("meta data %+v is old", meta)
}

type incre int

const (
	increACL incre = 1 << iota
	increLock
	increContent
	increInstance
)

type unsafeFS struct {
	*fileSystem
}

func (fs *unsafeFS) increMeta(
	filepath string,
	inc incre,
) error {
	orgFileInfo, err := fs.Read(filepath)
	if err != nil {
		log.Println(err.Error())
		return err
	}
	if orgFileInfo == nil {
		return fmt.Errorf("file %s is empty, could not update lock generation", filepath)
	}
	if inc&increACL != 0 {
		orgFileInfo.Meta.ACLGenerationNum++
	}
	if inc&increLock != 0 {
		orgFileInfo.Meta.LockGenerationNum++
	}
	if inc&increContent != 0 {
		orgFileInfo.Meta.ContentGenerationNum++
	}
	if inc&increInstance != 0 {
		orgFileInfo.Meta.InstanceNum++
	}
	kv := store.KV(filepath, orgFileInfo.String())
	succ := false
	return fs.storage.Set(kv, &succ)
}

func checkWithACLInfo(
	requestor string,
	mode string,
	acl FileACLInfo,
) (bool, error) {
	switch mode {
	case "W":
		return contains(acl.Writer, requestor), nil
	case "R":
		return contains(acl.Reader, requestor), nil
	case "ACL":
		return contains(acl.ACLChanger, requestor), nil
	default:
		return false, errors.New("error mode: " + mode)
	}
}

func (fs *fileSystem) CheckACL(
	filepath string,
	who string,
	mode string,
) (bool, error) {
	log.Println(filepath)
	fileInfo, err := fs.Read(filepath)
	log.Printf("%+v", fileInfo)
	if err != nil || fileInfo == nil {
		return false, err
	}
	acl := fileInfo.ACL
	return checkWithACLInfo(who, mode, acl)
}

func (fs *fileSystem) ListFiles(prefix string) ([]string, error) {
	var tempList = new(store.List)
	if err := fs.storage.Keys(&store.Pattern{Prefix: prefix, Suffix: ""}, tempList); err != nil {
		log.Printf("storage.Keys() failed!!")
		return nil, err
	}
	children := []string{}
	for _, f := range tempList.L {
		if (path{f}).isChildrenOf(path{prefix}) {
			children = append(children, f)
		}
	}
	if len(children) == 0 {
		return children, FsNotDir
	}
	return children, nil
}

type path struct {
	s string
}

func (child path) isChildrenOf(parent path) bool {
	childParts := strings.Split(child.s, "/")   // a/b/c
	parentParts := strings.Split(parent.s, "/") // a/b
	if len(childParts) <= len(parentParts) {
		return false
	}
	for i, c := range childParts {
		if c != parentParts[i] {
			return false
		}
	}
	return true
}

// Delete try to delete a file. If failed to delete, succ is assigned false
func (fs *fileSystem) Delete(filename string, isDir bool) (bool, error) {
	succ := true
	if !isDir {
		if err := fs.storage.Remove(filename, &succ); err != nil {
			return succ, err
		}
		if !succ {
			log.Printf("delete %s twice", filename)
		}
	} else {
		files := store.List{L: make([]string, 0)}
		if err := fs.storage.Keys(&store.Pattern{Prefix: filename, Suffix: ""}, &files); err != nil {
			return false, err
		}
		children := make([]string, 0)
		for _, f := range files.L {
			if (path{f}).isChildrenOf(path{filename}) {
				children = append(children, f)
			}
		}
		if len(children) == 0 {
			if err := fs.storage.Remove(filename, &succ); err != nil {
				return succ, err
			}
			if !succ {
				log.Printf("delete dir %s failed", filename)
			}
		} else {
			succ = false
			log.Printf("dir %s is not empty", filename)
		}
	}

	return succ, nil
}

func (fs *fileSystem) FileExists(filepath string) (bool, error) {
	value := ""
	e := fs.storage.Get(filepath, &value)
	return len(value) != 0, e
}
