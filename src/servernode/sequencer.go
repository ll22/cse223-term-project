package servernode

type Sequencer struct {
	Name           string
	Mode           string
	LockGeneration uint64
}

func (seq Sequencer) Equals(seq2 Sequencer) bool {
	return seq.Name == seq2.Name && seq.Mode == seq2.Mode && seq.LockGeneration == seq2.LockGeneration
}
