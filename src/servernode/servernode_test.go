package servernode_test

import (
	"events"
	"log"
	"servernode"
	"store"
	"testing"
	"testp"
	"time"
)

var addrs = []string{}

func TestGetHandle(t *testing.T) {
	log.SetFlags(log.Ldate | log.Llongfile)
	serv := createServ()
	addr := createAddr()
	req := servernode.GetHandleRequestInfo{
		Addr:             addr,
		CreateFile:       true,
		EventsSubscribed: events.EventNil,
		FilePath:         "file1",
		HandleType:       servernode.WriteHandle,
		InitialACL: servernode.FileACLInfo{
			Writer:     []string{addr},
			Reader:     []string{addr},
			ACLChanger: []string{addr},
		},
		InitialContent: "content1",
		IsDir:          false,
	}
	res := servernode.GetHandleReplyInfo{
		Err:     "",
		ErrType: servernode.NoError,
		Succ:    false,
	}
	log.Println("test: try to get handle")
	testp.NotError(serv.GetHandle(req, &res))
	testp.As(res.ErrType == servernode.NoError)
	testp.As(res.Succ)

	log.Println("test: try to conflict handle")
	testp.NotError(serv.GetHandle(req, &res))
	testp.As(res.ErrType == servernode.FileExist)
	log.Println(res.Err)

	log.Println("test: try to open the same handle")
	req.CreateFile = false
	res.ErrType = servernode.NoError
	testp.As(res.ErrType == servernode.NoError)
	testp.NotError(serv.GetHandle(req, &res))
	log.Println(res.ErrType)
	log.Println(res.Err)
	testp.As(res.ErrType == servernode.NoError)

	req.FilePath = "file1/file2"
	req.CreateFile = true
	res.ErrType = servernode.NoError
	res.Err = ""
	testp.NotError(serv.GetHandle(req, &res))
	log.Println(res.Err)
	testp.As(res.ErrType == servernode.FilePathConflict)

}

func TestServerNode_SetContents(t *testing.T) {
	serv := createServ()
	addr := createAddr()
	req := servernode.GetHandleRequestInfo{
		Addr:             addr,
		CreateFile:       true,
		EventsSubscribed: events.EventNil,
		FilePath:         "file1",
		HandleType:       servernode.WriteHandle,
		InitialACL: servernode.FileACLInfo{
			Writer:     []string{addr},
			Reader:     []string{addr},
			ACLChanger: []string{addr},
		},
		InitialContent: "content1",
		IsDir:          false,
	}
	res := servernode.GetHandleReplyInfo{
		Err:     "",
		ErrType: servernode.NoError,
		Succ:    false,
	}
	log.Println("test: try to get handle")
	testp.NotError(serv.GetHandle(req, &res))
	testp.As(res.ErrType == servernode.NoError)
	testp.As(res.Succ)

	getReq := servernode.GetContentsAndStatRequestInfo{
		FilePath:  "file1",
		Requestor: addr,
	}
	getRes := servernode.GetContentsAndStatReplyInfo{}
	testp.NotError(serv.GetContentsAndStat(getReq, &getRes))
	testp.As(getRes.Content.Content == "content1")

	setReq := servernode.SetContentsRequestInfo{
		FilePath: "file1",
		Content:  "content2",
	}
	setRes := servernode.SetContentsReplyInfo{}
	testp.NotError(serv.SetContents(setReq, &setRes))
	testp.As(setRes.Succ)

	testp.NotError(serv.GetContentsAndStat(getReq, &getRes))
	testp.As(getRes.Content.Content == "content2")
}

func TestServerNode_Acquire(t *testing.T) {
	t.Skip()
	serv := createServ()
	addr1 := createAddr()
	addr2 := createAddr()
	req := servernode.GetHandleRequestInfo{
		Addr:             addr1,
		CreateFile:       true,
		EventsSubscribed: events.EventNil,
		FilePath:         "file1",
		HandleType:       servernode.WriteHandle,
		InitialACL: servernode.FileACLInfo{
			Writer:     []string{addr1},
			Reader:     []string{addr1},
			ACLChanger: []string{addr1},
		},
		InitialContent: "content1",
		IsDir:          false,
	}
	res := servernode.GetHandleReplyInfo{
		Err:     "",
		ErrType: servernode.NoError,
		Succ:    false,
	}
	testp.NotError(serv.GetHandle(req, &res))
	testp.As(res.Succ)
	acquireAction := func(addr string, action func()) {
		req := servernode.AcquireRequestInfo{
			FilePath: "file1",
			Acquirer: addr,
			Mode:     "W",
			Retries:  100,
		}
		res := servernode.AcquireReplyInfo{}
		serv.Acquire(req, &res)
		log.Println(res)
		action()
	}
	value := 1
	action1 := func() {
		num := 3
		for num > 0 {
			value += 10
			time.Sleep(time.Second * 2)
			num--
		}
	}

	action2 := func() {
		num := 3
		for num > 0 {
			value *= 10
			time.Sleep(time.Second * 2)
			num--
		}
	}
	done := make(chan bool)
	go func() {
		acquireAction(addr1, action1)
		log.Println("done action1")
		done <- true
	}()
	go func() {
		acquireAction(addr2, action2)
		log.Println("done action2")
		done <- true
	}()
	<-done
	log.Println(value)
	testp.As(value == 31 || value == 1000)
}

func TestServerNode_Release(t *testing.T) {
	t.Skip()
	log.SetFlags(log.Lshortfile | log.Ldate)
	testp.SetT(t)
	serv := createServ()
	addr1 := createAddr()
	addr2 := createAddr()
	req := servernode.GetHandleRequestInfo{
		Addr:             addr1,
		CreateFile:       true,
		EventsSubscribed: events.EventNil,
		FilePath:         "file1",
		HandleType:       servernode.WriteHandle,
		InitialACL: servernode.FileACLInfo{
			Writer:     []string{addr1},
			Reader:     []string{addr1},
			ACLChanger: []string{addr1},
		},
		InitialContent: "content1",
		IsDir:          false,
	}
	res := servernode.GetHandleReplyInfo{
		Err:     "",
		ErrType: servernode.NoError,
		Succ:    false,
	}
	testp.NotError(serv.GetHandle(req, &res))
	testp.As(res.Succ)
	acquireAction := func(addr string, action func()) {
		req := servernode.AcquireRequestInfo{
			FilePath: "file1",
			Acquirer: addr,
			Mode:     "W",
			Retries:  100,
		}
		res := servernode.AcquireReplyInfo{}
		serv.Acquire(req, &res)
		log.Println(res)
		action()
		releaseReq := servernode.ReleaseRequestInfo{
			Mode:     "W",
			FilePath: "file1",
			Holder:   addr,
		}
		releaseRes := servernode.ReleaseReplyInfo{
			Succ: false,
		}
		testp.NotError(serv.Release(releaseReq, &releaseRes))
		testp.As(releaseRes.Succ)
	}
	value := 1
	action1 := func() {
		num := 3
		for num > 0 {
			value += 10
			num--
		}
	}

	action2 := func() {
		num := 3
		for num > 0 {
			value *= 10
			num--
		}
	}

	done := make(chan bool)
	go func() {
		acquireAction(addr1, action1)
		done <- true
	}()
	go func() {
		acquireAction(addr2, action2)
		done <- true
	}()

	<-done
	<-done
	log.Println(value)
	testp.As(value == 31000 || value == 1030)
}

func createServ() servernode.ServerNodeT {
	dbAddr := testp.GenerateClientAddr(addrs)
	addrs = append(addrs)

	servAddr := testp.GenerateClientAddr(addrs)
	addrs = append(addrs)

	db := store.NewStorageId(randomInt(10000))
	ready := make(chan bool)
	go store.ServeStoreRPC(dbAddr, db, ready)
	<-ready
	serv := servernode.NewServerNode(&servernode.ServerNodeConfig{
		ServerAddr: servAddr,
		StoreAddr:  dbAddr,
	})
	return serv
}

func createAddr() string {
	addr := testp.GenerateClientAddr(addrs)
	addrs = append(addrs, addr)
	return addr
}
