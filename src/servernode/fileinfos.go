package servernode

import "encoding/json"

// FileInfo all the information of a file
type FileInfo struct {
	ACL     FileACLInfo
	Content FileContentInfo
	Meta    FileMetaInfo
}

func (fi FileInfo) String() string {
	bytes, err := json.Marshal(fi)
	if err != nil {
		panic("shoud not happen, " + err.Error())
	}
	return string(bytes)
}

func FileInfoFromString(s string) *FileInfo {
	bytes := []byte(s)
	fi := FileInfo{}
	if err := json.Unmarshal(bytes, &fi); err != nil {
		panic("should not happen, " + err.Error())
	}
	return &fi
}

// FileContentInfo the information of a file
type FileContentInfo struct {
	Content string
}

type FileACLInfo struct {
	Writer     []string
	Reader     []string
	ACLChanger []string
}

type FileMetaInfo struct {
	InstanceNum          uint64
	ContentGenerationNum uint64
	LockGenerationNum    uint64
	ACLGenerationNum     uint64
	IsDir                bool
}

func (meta FileMetaInfo) IsNewer(other FileMetaInfo) bool {
	return meta.ACLGenerationNum >= other.ACLGenerationNum &&
		meta.ContentGenerationNum >= other.ContentGenerationNum &&
		meta.InstanceNum >= other.InstanceNum &&
		meta.LockGenerationNum >= other.LockGenerationNum &&
		meta.IsDir == other.IsDir
}

type FileInfoModification struct {
	Modification        FileInfo
	ShouldModifyACL     bool
	ShouldModifyContent bool
	ShouldModifyMeta    bool
}

type FileLockInfo struct {
	Who  string
	Mode string
}

func (fli FileLockInfo) String() string {
	bytes, err := json.Marshal(fli)
	if err != nil {
		panic("shoud not happen, " + err.Error())
	}
	return string(bytes)
}

func FileLockInfoFromString(s string) *FileLockInfo {
	bytes := []byte(s)
	fli := FileLockInfo{}
	if err := json.Unmarshal(bytes, &fli); err != nil {
		panic("should not happen, " + err.Error())
	}
	return &fli
}
