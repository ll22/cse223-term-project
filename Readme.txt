A simple lock service.


File system APIs:

Paxos:


KeepAlive:

"No handle no session" principal. There could be at most one session between a Chubby client and a server.
The session is established when the first Open() is issued from the client to the server. The client initiates
the session establishment. Let the server keep track of the number of file handlers that still remain open.
When the client closes the last file handle, the server returns the KeepLive() RPC call immediately with the
termination message so that the client wouldn't try connect again with KeepLive RPC.

User needs to provide a few routines (event) in case some kinds of timeout happens.
    1. jeopardy(): when client enters the "jeopardy" state
    2. safe(): when client exits from the "jeopardy" state with a response from the server
    3. expire(): when the final timeout happens on the client side (no resposne from the server).

The session terminates under below conditions:
    1. Client explicitly terminates by closing all opened files.
    2. Client times out if server doesn't reply for a while. Run "expire()"